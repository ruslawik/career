@extends('user.layout')

@section('title', $title)

@section('content')

     @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if($already_passed < $user[0]->disc && $available == 1)
    <div class="col-lg-12">
        <form action="{{ $action }}" method="POST">
            {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Берілген сұрақтарға өрнек бойынша жауап беріңіз. Бітіру үшін "Сақтау" түймесін басыңыз.</strong>
                             <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сақтау!</button>
                        </div>
                    <div class="card-body">
                        <div id="pr_block_0" style="display: none;">
                           <b> Сөйлемшелер #1 из 24</b><br><hr>
                                @foreach($questions as $question)
                                    M - <input type="radio" class="radio" id="m{{ $question->id }}" name="most{{$group_ar[$question->id]}}" style="width:25px;" value="{{ $question->id }}" onClick="check_most({{ $question->id }});">
                                    L - <input type="radio" class="radio" id="l{{ $question->id }}" name="least{{$group_ar[$question->id]}}" style="width:25px;" value="{{ $question->id }}" onClick="check_least({{ $question->id }});">
                                        {{ $question->question }}
                                        <hr>
                                    @if($question->id % 4 === 0)
                                        <br>
                                        <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $question->id-8 }}, {{ $question->id-4 }}, 0);">Алдыңғы</a>
                                        
                                        <a class="btn btn-success" style="color:white;" onClick="show_quest({{ $question->id }}, {{ $question->id-4 }}, 0);">Келесі</a>

                                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(0, {{ $question->id-4 }}, 0);">Басына оралу</a>
                                        </div>
                                        <div id="pr_block_{{ $question->id }}" style="display: none;">
                                           <b> Сөйлемшелер #{{ $group_ar[$question->id]+1 }} из 24</b><br><hr>
                                    @endif
                                @endforeach
                    </div>
                    
                </div>
        </form>
    </div>
    Дұрыс жауап берілген сұрақтың өрнегі:<br>
    <img src="/user_res/disc_samp_kaz.png">
    <hr>
    <div class="alert alert-success">
        1. Сұрақтарға жауап беретін ортамды таңдаңыз: жұмыс, үй, басқа қоғамдық орындар және т.б.<br>
2. Төменде келтірілген әр блокта төрт тіркес бар. Әр блоктағы төрт тіркестің әрқайсысын мұқият оқып шығыңыз.  Сізге ең жақын және нақты сипаттайтын сөз тіркесінің жанына «M» тармағын таңдаңыз.<br>
3. Сізге ең алыс және нақты сипаттамайтын тіркестің жанына «L» тармағын таңдаңыз.<br>
4. Әр блокта сізді ең жақын және ең алыс деңгейде сипаттайтын бір жауапты таңдаңыз.<br>
5. Жауаптар 7 минут ішінде немесе көрсетілген уақытқа ең жақын мерзімде берілуі тиіс.<br>
    </div>
    @elseif($already_passed == $user[0]->disc)
        <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Вы уже проходили данный тест</strong>
                        </div>
                    <div class="card-body">
                        Можете <a href="/user/disc/results/">просмотреть свои результаты</a>
                    </div>
                </div>

    @endif
    @if($available != 1)
        <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Нәтиже </strong>
                             
                        </div>
                    <div class="card-body">
                        Бұл тест жабық
                    </div>
        </div>
        </div>
        @endif
    

@endsection

@section('mapp_pass_javascript')
<script>
     function show_quest(next_id, now_id, first_time){
        if(next_id < 96 && next_id >=0){
            var numberOfCheckedRadio = jQuery("#pr_block_"+now_id+' input:radio:checked').length;
            if(numberOfCheckedRadio<2 && first_time!=1){
                alert("Заполните ответы как показано в инструкции! Не пропускайте");
                jQuery("#pr_block_"+now_id).css('display', 'block');
            }else{
                jQuery("#pr_block_"+next_id).toggle('fast');
                jQuery("#pr_block_"+now_id).toggle('fast');
            }
        }
    }
    show_quest(1,0,1);
    function check_most(l_id){
        if(jQuery('#l'+l_id).is(':checked')){ 
            jQuery('#l'+l_id).attr('checked', false);
        }
    }
    function check_least(l_id){
        if(jQuery('#m'+l_id).is(':checked')){ 
            jQuery('#m'+l_id).attr('checked', false);
        }
    }
</script>
@endsection