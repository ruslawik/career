@extends('user.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if($auth == '1')
    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пройденные тесты</strong>
                        </div>
                    <div class="card-body">
                    @foreach ($mapp_results as $key => $result)
                        Завершенное тестирование MAPP - {{ $key }}
                        <!--<a href="/user/download_mapp_results/" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Скачать результаты pdf (англ.)</a>!-->
                        <a target="_blank" href="/user/show_mapp_results_online/ru" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Результаты (расширенная версия)</a>
                        <a target="_blank" href="/user/show_mapp_results_small/ru" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Результаты (сжатая версия)</a>
                        <br><br>
                    @endforeach
                </div>
    </div>
    @endif
     @if($auth=='0')
            <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Тест недоступен </strong>
                             
                    </div>
                    <div class="card-body">
                        Обратитесь к администратору, Ваш аккаунт не связан с аккаунтом в системе MAPP. 
                    </div>
            </div>
        </div>
        @endif

@endsection