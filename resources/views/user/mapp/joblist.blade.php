@extends('user.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="col-lg-12">
        <a href="?lang=ru"><button class="btn btn-success">Русский</button></a>
    <a href="?lang=kz"><button class="btn btn-success">Казахский</button></a>
    <a href="?lang=en"><button class="btn btn-success">Английский</button></a><br><br>
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Список</strong>
                    </div>
                    <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td><b>Профессия</b></td>
                                        <td><b>Совместимость</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jobs as $job)
                                        @if($job->job['not_in_kz']!=1)
                                        <tr>
                                            <td>
                                                    @if($lang == "ru")
                                                        <b>{{$job->job['job_name_rus']}}</b><br>{{$job->job['job_desc_rus']}}
                                                    @elseif($lang == "en")
                                                        <b>{{$job->job['job_name']}}</b><br>{{$job->job['job_desc']}}
                                                    @elseif($lang == "kz")
                                                        <b>{{$job->job['job_name_kaz']}}</b><br>{{$job->job['job_desc_kaz']}}
                                                    @endif
                                            </td>
                                            <td>
                                                <center>{{$job['percent']}}%</center>
                                                <center>
                                                <a href="/user/mapp_job/{{$job->job['sys_id']}}" class="btn-sm btn-success" style="color:white;cursor:pointer;">
                                                    Детали
                                                </a> 
                                                </center>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
    </div>

@endsection