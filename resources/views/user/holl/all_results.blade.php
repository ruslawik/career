@extends('user.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Информация о тесте
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Пройденные тесты</strong>
                </div>
                <div class="card-body">
                    <h6>Тестирование "Социальный интеллект"</h6>
                    <hr>
                     @foreach ($holl_results as $key => $result)
                        Завершенное тестирование Холла - {{ $key }}
                        <a href="/user/holl_indres/{{$key}}/ru" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach

                </div>
    </div>



@endsection