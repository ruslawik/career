@extends('user.layout')

@section('title', $title)

@section('content')

     @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if($passed == '0' && $available == 1)
    <div class="alert alert-warning">
        <b>Инструкция.</b> Если высказывание отражает ваше отношение к себе, людям и событиям — выберите ✔ (слева), если не отражает — ✖ (справа). 
    </div>
    <div class="col-lg-12">
        <form action="{{ $action }}" method="POST">
            {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Выберите нужный вариант. Для завершения теста нажмите "Завершить"</strong>
                             <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Завершить!</button>
                        </div>
                    <div class="card-body" style="padding:0px !important;">
                        <?php
                            $block_id = 1;
                        ?>
                        <div id="pr_block_1" style="display:none; background:{{$col['1']}}; padding:1%;">
                            <h5>Блок 1 из 3</h5>
                            <hr>
                        @foreach($questions as $question)
                        @if($question->id % 10 === 0 && $question->id != 30)
                            <hr>
                            <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">Предыдущий блок</a>
                            
                        <a class="btn btn-success" style="color:white;" onClick="show_quest({{ $block_id+1 }}, {{ $block_id }});">Следующий блок</a>

                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(1, {{ $block_id }});">Вернуться в начало</a>
                            </div>
                            <?php
                                $block_id++;
                            ?>
                            <div id="pr_block_{{$block_id}}" style="display:none; background:{{$col[$block_id]}}; padding:1%;">
                                <h5>Блок {{ $block_id }} из 3</h5><hr>
                        @endif
                            <!-- Вопрос № {{ $question->id }} из 70!-->
                            <table>
                            <tr>
                                <td>
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="1" class="big-checkbox"><span class="checkmark_green"></span></label>
                                </td>
                                <td>
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="0" class="big-checkbox"><span class="checkmark"></span></label>
                                </td>
                                <td>
                                    @if($lang=="rus")
                                        {{ $question->question }}
                                    @endif
                                    @if($lang=="kaz")
                                        {{ $question->q_kaz }}
                                    @endif
                                </td>
                            </tr>
                            </table>
                            <br>
                        @endforeach
                        <hr>
                            <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">Предыдущий блок</a>
                            
                        <a class="btn btn-success" style="color:white;" onClick="show_quest({{ $block_id+1 }}, {{ $block_id }});">Следующий блок</a>

                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(1, {{ $block_id }});">Вернуться в начало</a>
                            </div>


                    </div>
                    
                </div>
        </form>
    </div>
     @elseif($available==1 && $passed != "0")
     <div class="col-lg-12">
        <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Результаты </strong>
                             
                        </div>
                    <div class="card-body">
            Вы уже прошли тестирование. Результаты можно <a href="/user/holl_res/results">посмотреть здесь</a>
            </div>
        </div>
    </div>
        @endif
        @if($available != 1)
        <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Тест недоступен </strong>
                             
                    </div>
                    <div class="card-body">
                        Данный тест недоступен Вам. 
                    </div>
            </div>
        </div>
        @endif
@endsection

@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id){
        if(next_id <= 3 && next_id >=1){
            jQuery("#pr_block_"+next_id).toggle('fast');
            jQuery("#pr_block_"+now_id).toggle('fast');
        }
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    }
    show_quest(1,0);

</script>
@endsection