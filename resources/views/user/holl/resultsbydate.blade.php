@extends('user.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пользователь - {{ $name }} {{ $surname }} || Дата сдачи - {{ $date }}</strong>
                        </div>
                    <div class="card-body">
                    @if($lang=="ru")
                        «Самосознание» - {{ $samosoz }} <br>
                        «Саморегуляция» - {{ $samoreg }} <br>
                        «Эмпатия» - {{ $empathy }} <br>
                        «Навыки взаимодействия» - {{ $vzaimo }} <br>
                        «Самомотивация» - {{ $samomot }}<br>
                         <a href="kz">Қазақша</a>
                         <br><br>Ниже Вы можете просмотреть детализацию ответов<hr>
                        <a class="btn-sm btn-success" style="color:white;cursor:pointer;" onClick="show_all_anses();"> Показать/Скрыть детали</a>
                        <div id="all_answers" style="display: none;">
                            <br>
                            <b>Ответы: 1 - "+", 0 - "-"</b>
                            <hr>
                            @foreach ($results as $result)
                                Вопрос {{ $result->quest_id }} <br>
                                {{ $result->quest_text->question }} <br>
                                Ответ: {{ $result->answer }}
                                <hr>
                            @endforeach
                        </div>
                    @endif
                    @if($lang=="kz")
                        «Өзін-өзі тану» - {{ $samosoz }} <br>
                        «Өзін-өзі басқару» - {{ $samoreg }} <br>
                        «Эмпатия» - {{ $empathy }} <br>
                        «Ынтымақтастық дағдылары» - {{ $vzaimo }} <br>
                        «Өзін-өзі ынталандыру» - {{ $samomot }}<br>
                        <a href="ru">На русском</a>
                        <br><br>Астында сіздің жауаптарыңызды көре аласыз<hr>
                        <a class="btn-sm btn-success" style="color:white;cursor:pointer;" onClick="show_all_anses();"> Көрсету/Жасыру</a>
                        <div id="all_answers" style="display: none;">
                            <br>
                            <b>Жауаптар: 1 - "+", 0 - "-"</b>
                            <hr>
                            @foreach ($results as $result)
                                Сұрақ {{ $result->quest_id }} <br>
                                {{ $result->quest_text->q_kaz }} <br>
                                Жауап: {{ $result->answer }}
                                <hr>
                            @endforeach
                        </div>
                    @endif
                        
                    </div>
    </div>

@endsection

@section('mapp_pass_javascript')
<script>
    function show_all_anses(){
        jQuery("#all_answers").toggle();
    }
</script>
@endsection