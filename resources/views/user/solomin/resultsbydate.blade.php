@extends('user.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пользователь - {{ $name }} {{ $surname }} || Дата сдачи - {{ $date }}</strong>
                        </div>
                    <div class="card-body">
                        <b>Таблица "Я хочу"</b><br>
                        1) человек - человек - {{ $elovek_4elovek_1 }} <br>
                        2) человек - техника - {{ $elovek_texnika_1 }} <br>
                        3) человек - знаковая система - {{ $elovek_znakov_1 }} <br>
                        4) человек - художественный образ - {{ $elovek_hud_1 }}<br>
                        5) человек - природа - {{ $elovek_priroda_1 }}<br>
                        А) исполнительские - {{ $harakter_a_ispolnit_1 }}<br>
                        Б) творческие - {{ $harakter_b_tvor4_1 }}<br>
                        <hr>
                        <b>Таблица "Я могу"</b><br>
                        1) человек - человек - {{ $elovek_4elovek_2 }} <br>
                        2) человек - техника - {{ $elovek_texnika_2 }} <br>
                        3) человек - знаковая система - {{ $elovek_znakov_2 }} <br>
                        4) человек - художественный образ - {{ $elovek_hud_2 }}<br>
                        5) человек - природа - {{ $elovek_priroda_2 }}<br>
                        А) исполнительские - {{ $harakter_a_ispolnit_2 }}<br>
                        Б) творческие - {{ $harakter_b_tvor4_2 }}<br>

                        <br><br>Ниже Вы можете просмотреть детализацию ответов<hr>
                        <a class="btn-sm btn-success" style="color:white;cursor:pointer;" onClick="show_all_anses();"> Показать/Скрыть детали</a>
                        <div id="all_answers" style="display: none;">
                            <br>
                            Таблица 1
                            <hr>
                            @foreach ($results1 as $result)
                                Вопрос {{ $result->quest_id }} <br>
                                {{ $result->quest_text->text }} <br>
                                Ответ: {{ $result->answer }}
                                <hr>
                            @endforeach
                            <hr>
                            Таблица 2
                            <hr>
                            @foreach ($results2 as $result)
                                Вопрос {{ $result->quest_id }} <br>
                                {{ $result->quest_text->text }} <br>
                                Ответ: {{ $result->answer }}
                                <hr>
                            @endforeach
                        </div>
                    </div>
    </div>

@endsection

@section('mapp_pass_javascript')
<script>
    function show_all_anses(){
        jQuery("#all_answers").toggle();
    }
</script>
@endsection