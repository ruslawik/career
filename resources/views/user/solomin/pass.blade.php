@extends('user.layout')

@section('title', $title)

@section('content')

     @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif


    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if($passed == '0' && $available == 1)
     <div class="alert alert-warning">
        <b>Инструкция.</b> Напротив каждого высказывания выберите цифру, соответствующую степени вашего желания заниматься этим видом деятельности:
 0 - вовсе нет;
1 - пожалуй, так; 
2 - верно;
 3 - совершенно верно.

    </div>

    <div class="col-lg-12">
        <form action="{{ $action }}" method="POST">
            {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Выберите нужную цифру. Для завершения теста нажмите "Завершить"</strong>
                             <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Завершить!</button>
                        </div>
                    <div class="card-body" style="padding:0px !important;">
                        <div id="pr_block_1" style="display:none; background:{{$col['1']}}; padding:1%;">
                        <table class="table">
                            <tr>
                                <td style="border:none !important;">
                                <table>
                                    <tr>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio" value="0" class="big-checkbox" disabled checked><span class="checkmark_0"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio"  value="1" class="big-checkbox" disabled checked><span class="checkmark_1"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio"  value="2" class="big-checkbox" disabled checked><span class="checkmark_2"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio" value="3" class="big-checkbox" disabled checked><span class="checkmark_3"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                        </td>
                                    </tr>
                                </table>
                        @foreach($questions1 as $question)
                            <tr>
                                <td>
                                <table>
                                    <tr>
                                        <td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="0" class="big-checkbox"><span class="checkmark_0"></span></label>
                                </td>
                                <td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="1" class="big-checkbox"><span class="checkmark_1"></span></label>
                                </td><td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="2" class="big-checkbox"><span class="checkmark_2"></span></label>
                                </td><td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="check{{$question->id}}" value="3" class="big-checkbox"><span class="checkmark_3"></span></label>
                                </td>
                                <td style="border:none !important;">
                                    @if($lang=="rus")
                                        {{ $question->text }}
                                    @endif
                                    @if($lang=="kaz")
                                        {{ $question->text_kaz }}
                                    @endif
                                </td>
                                </tr>
                                </table>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        @endforeach
                        <tr><td>
                            <a class="btn btn-success" onClick="show_quest(1,2);" style="float:right;"><i class="fa fa-next"></i>&nbsp; Следующий блок</a>
                        </td></tr>
                        </table>

                        </div>

                        <div id="pr_block_2" style="display:none; background:{{$col['2']}}; padding:1%;">
                        <table class="table">
                            <tr>
                                <td style="border:none !important;">
                                <table>
                                    <tr>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio" value="0" class="big-checkbox" disabled checked><span class="checkmark_0"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio"  value="1" class="big-checkbox" disabled checked><span class="checkmark_1"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio"  value="2" class="big-checkbox" disabled checked><span class="checkmark_2"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                            <label class="check_container"><input type="radio" value="3" class="big-checkbox" disabled checked><span class="checkmark_3"></span></label>
                                        </td>
                                        <td style="border:none !important;">
                                        </td>
                                    </tr>
                                </table>
                        @foreach($questions2 as $question)
                            <tr>
                                <td>
                                <table>
                                    <tr>
                                        <td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="two_check{{$question->id}}" value="0" class="big-checkbox"><span class="checkmark_0"></span></label>
                                </td>
                                <td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="two_check{{$question->id}}" value="1" class="big-checkbox"><span class="checkmark_1"></span></label>
                                </td><td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="two_check{{$question->id}}" value="2" class="big-checkbox"><span class="checkmark_2"></span></label>
                                </td><td style="border:none !important;">
                                    <label class="check_container"><input type="radio" name="two_check{{$question->id}}" value="3" class="big-checkbox"><span class="checkmark_3"></span></label>
                                </td>
                                <td style="border:none !important;">
                                    @if($lang=="rus")
                                        {{ $question->text }}
                                    @endif
                                    @if($lang=="kaz")
                                        {{ $question->text_kaz }}
                                    @endif
                                </td>
                                </tr>
                                </table>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td> <a class="btn btn-success" onClick="show_quest(1,2);" style="float:right;"><i class="fa fa-next"></i>&nbsp; Предыдущий блок</a></td>
                        </tr>
                        </table>
                        </div>

                    </div>
                </div>
        </form>
    </div>
    @elseif($available==1 && $passed != "0")
     <div class="col-lg-12">
        <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Результаты </strong>
                             
                        </div>
                    <div class="card-body">
            Вы уже прошли тестирование. Результаты можно <a href="/user/solomin_res/results">посмотреть здесь</a>
            </div>
        </div>
    </div>
        @endif
        @if($available != 1)
        <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Тест недоступен </strong>
                    </div>
                    <div class="card-body">
                        Данный тест недоступен Вам. 
                    </div>
            </div>
        </div>
        @endif

@endsection


@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id){
        if(next_id <= 2 && next_id >=1){
            jQuery("#pr_block_"+next_id).toggle('fast');
            jQuery("#pr_block_"+now_id).toggle('fast');
        }
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    }
    show_quest(1,0);

</script>
@endsection