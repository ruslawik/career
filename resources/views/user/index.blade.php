@extends('user.layout')

@section('title', $title)

@section('content')
    
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Пройти тесты</div>
                                Для прохождения доступных Вам тестов, выберите соответствующий пункт меню
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection