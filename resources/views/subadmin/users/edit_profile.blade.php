@extends('subadmin.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Ред.</span> <b>Ваш профиль</b> - 
                  Вы можете редактировать основную информацию о себе, изменять пароль
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{$user['0']->id}}">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать профиль</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Логин</div>
                                            <input type="text" class="form-control col-sm-12" name="login" disabled value="{{$user['0']->login}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Имя</div>
                                            <input type="text" class="form-control col-sm-12" name="name" value="{{$user['0']->name}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Фамилия</div>
                                            <input type="text" class="form-control col-sm-12" name="surname" value="{{$user['0']->surname}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Новый пароль (если требуется)</div>
                                            <input type="text" class="form-control col-sm-12" name="password">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success" style="float:left;"><i class="fa fa-magic"></i>&nbsp; Изменить</button>
                        </div>
                    </div>
            
    </div>


@endsection