@extends('subadmin.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Нажмите на логин пользователя для просмотра результатов тестов или просмотра информации о всех пользователях subadmin аккаунта
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <input type="hidden" id="asd" value="1">
    <div class="col-lg-12">
             <div class="input-group">
                            <div class="input-group-addon" style="background:green;color:white;" id="to_change_background">Показывать</div>
                            <select class="form-control col-lg-4" id="is_active">
                                <option value="1">Активные</option>
                                <option value="0">Неактивные</option>
                            </select>
            </div><hr>
            <table id="all_users" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Логин</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Роль</th>
                        <th><center>Ред.</center></th>
                      </tr>
                    </thead>

            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#is_active").change(function(){
                var is_active = jQuery("#is_active").val();
                if(is_active == 0){
                    jQuery("#to_change_background").css("background", "red");
                }else{
                    jQuery("#to_change_background").css("background", "green");
                }
                jQuery('#all_users').DataTable().ajax.url('/subadmin/ajax_all_users/'+is_active).load();
            });
        });
        jQuery(document).ready(function(){
        jQuery('#all_users').DataTable({
            'processing': false,
            'serverSide': false,
            'serverMethod': 'get',
            'ajax': {
                'url':'/subadmin/ajax_all_users/1',
            }, 
            'columns': [
                { data: 'login',
                  "render": function(data,type,row,meta) {
                        if(row.user_type==2){
                            var a = '<a href="/subadmin/user/'+row.id+'">'+data+'</a>';
                        }
                        if(row.user_type==3){
                            var a = '<a href="/subadmin/subadmin_info/'+row.id+'">'+data+'</a>';
                        }
                        return a;
                    }
                },
                { data: 'name' },
                { data: 'surname' },
                { data: 'user_type',
                    "render": function(data,type,row,meta) {
                        if(data==2){
                            var a = 'Клиент';
                        }
                        if(data==1){
                            var a = 'Администратор';
                        }
                        if(data==3){
                            var a = 'Sub-admin';
                        }
                        return a;
                    }
                },
                { data: "id", // can be null or undefined
                  "render": function(data,type,row,meta) {
                        if(row.user_type == 2){
                            var a = '<center><a href="/subadmin/edit_user/'+data+'"><i class="fa fa-edit" style="font-size:24px;"></i></a></center>';
                        }
                        if(row.user_type == 3){
                            var a = '<center><a href="/subadmin/edit_subadmin/'+data+'"><i class="fa fa-edit" style="font-size:24px;"></i></a></center>';
                        }
                        if(row.user_type == 1){
                            var a = '';
                        }
                        return a;
                    }
                }
                ]
            });
        });
    </script>

@endsection