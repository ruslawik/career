@extends('subadmin.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Здесь Вы можете просмотреть количество тестов, выданных Вам вышестоящим subadmin-аккаунтом или администратором, а также просмотреть количество использованных тестов
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>


<div class="col-lg-12" id="user_additionals">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Выданные Вам доступы к тестам</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Название теста</td>
                                        <td>Количество</td>
                                        <td>Цена за тест</td>
                                        <td>Дата выдачи</td>
                                        <td>Кем выдано</td>
                                        <td>Осталось</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($all_given_tests as $test)
                                    <tr>
                                        <td>{{$test->setting}}</td>
                                        <td>{{$test->value}}</td>
                                        <td>{{$test->test_price->price}}</td>
                                        <td>{{$test->created_at}}</td>
                                        <td>{{$test->user->name}} {{$test->user->surname}}</td>
                                        <td>{{$test->value - $test->used_value}}</td>
                                     </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
</div>
  

@endsection