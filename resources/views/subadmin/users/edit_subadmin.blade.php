@extends('subadmin.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Изменение</span> <b>Subadmin</b> - 
                  Вы можете редактировать настройки данного пользователя, добавлять ему новые тесты в пределах количества, выданного Вам администратором системы, а также активироать/деактивировать пользователя
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-6">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{$user['0']->id}}">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать пользователя</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif

                           <div class="input-group">
                                            <div class="input-group-addon" style="<?php if($user['0']->is_active == 1){print("background:green;");}else{print("background:red;");}?> color:white;">Пользователь</div>
                            <select class="form-control" name="is_active">
                                <option value="1" <?php if($user['0']->is_active == 1)print ("selected");?>>Активен</option>
                                <option value="0" <?php if($user['0']->is_active == 0)print ("selected");?>>Неактивен</option>
                            </select>
                        </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Логин</div>
                                            <input type="text" class="form-control col-sm-12" name="login" disabled value="{{$user['0']->login}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Имя</div>
                                            <input type="text" class="form-control col-sm-12" name="name" value="{{$user['0']->name}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Фамилия</div>
                                            <input type="text" class="form-control col-sm-12" name="surname" value="{{$user['0']->surname}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Новый пароль (если требуется)</div>
                                            <input type="text" class="form-control col-sm-12" name="password">
                            </div>
                            <hr>
                        </div>
                    </div>
            
    </div>

     <div class="col-lg-6" id="user_additionals">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить доступы к тестам </strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Тест</td>
                                        <td>Количество</td>
                                        <td>Цена за тест</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MAPP</td>
                                        <td>
                                            <input type="text" name="mapp_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="mapp_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DISC</td>
                                        <td>
                                            <input type="text" name="disc_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="disc_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Кейрси</td>
                                        <td>
                                            <input type="text" name="keirsi_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="keirsi_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Холл</td>
                                        <td>
                                            <input type="text" name="holl_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="holl_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Томас</td>
                                        <td>
                                            <input type="text" name="tomas_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="tomas_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Соломин</td>
                                        <td>
                                            <input type="text" name="solomin_test" value="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="solomin_test_price" value=0>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                </div>
    </form>
</div>

<div class="col-lg-12" id="user_additionals">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Выданные Вами доступы к тестам</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Название теста</td>
                                        <td>Количество</td>
                                        <td>Цена за тест</td>
                                        <td>Дата выдачи</td>
                                        <td>Осталось</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($all_given_tests as $test)
                                    <tr>
                                        <td>{{$test->setting}}</td>
                                        <td>{{$test->value}}</td>
                                        <td>{{$test->test_price->price}}</td>
                                        <td>{{$test->created_at}}</td>
                                        <td>{{$test->value - $test->used_value}}</td>
                                     </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
</div>
  

@endsection