@extends('subadmin.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Изменение</span> <b>Subadmin</b> - 
                  Вы можете редактировать настройки данного пользователя, назначать ему новые тестирования и запрещать их, а также активироать/деактивировать пользователя
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-6">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{$user['0']->id}}">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать пользователя</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif

                           <div class="input-group">
                                            <div class="input-group-addon" style="<?php if($user['0']->is_active == 1){print("background:green;");}else{print("background:red;");}?> color:white;">Пользователь</div>
                            <select class="form-control" name="is_active">
                                <option value="1" <?php if($user['0']->is_active == 1)print ("selected");?>>Активен</option>
                                <option value="0" <?php if($user['0']->is_active == 0)print ("selected");?>>Неактивен</option>
                            </select>
                        </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Логин</div>
                                            <input type="text" class="form-control col-sm-12" name="login" disabled value="{{$user['0']->login}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Имя</div>
                                            <input type="text" class="form-control col-sm-12" name="name" value="{{$user['0']->name}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Фамилия</div>
                                            <input type="text" class="form-control col-sm-12" name="surname" value="{{$user['0']->surname}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Новый пароль (если требуется)</div>
                                            <input type="text" class="form-control col-sm-12" name="password">
                            </div>
                            <br>
                            <hr>
                                Введите логин и пароль тестируемого от <a href="https://recruit.assessment.com/CareerCenter/" target="_blank">https://recruit.assessment.com/CareerCenter/</a> (если требуется)
                                <div class="input-group">
                                            <div class="input-group-addon">Логин mapp</div>
                                            <input type="text" class="form-control col-sm-12" name="mapp_login" value="{{$user['0']->mapp_login}}">
                                </div>
                                <br>
                                <div class="input-group">
                                            <div class="input-group-addon">Пароль mapp</div>
                                            <input type="text" class="form-control col-sm-12" name="mapp_pass" value="{{$user['0']->mapp_pass}}">
                                </div>
                            <hr><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Выберите роль</div>
                                            <select name="role" class="form-control col-sm-5">
                                                <option value="2" <?php if($user['0']->user_type == 2)print ("selected");?>>Клиент</option>
                                                <option value="1" <?php if($user['0']->user_type == 1)print ("selected");?>>Администратор</option>
                                                <option value="3" <?php if($user['0']->user_type == 3)print ("selected");?>>Sub-admin</option>
                                            </select>
                            </div>
                        </div>
                    </div>
            
    </div>

    <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Тесты к прохождению</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Тест</td>
                                        <td>Выбрать</td>
                                        <td>Цена за тест</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MAPP</td>
                                        <td>
                                            @if($mapp_passed == 0)
                                                <input type="checkbox" name="mapp_test" value="1" <?php $user['0']->mapp == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="mapp_test" value="1" style="display:none;" checked>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="mapp_test_price" value={{$user['0']->mapp_price}}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DISC</td>
                                        <td>
                                            @if($disc_passed == 0)
                                                <input type="checkbox" name="disc_test" value="1" <?php $user['0']->disc == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="disc_test" value="1" checked style="display:none;">
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="disc_test_price" value={{$user['0']->disc_price}}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Кейрси</td>
                                        <td>
                                            @if($keirsi_passed == 0)
                                                <input type="checkbox" name="keirsi_test" value="1" <?php $user['0']->keirsi == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="keirsi_test" value="1" checked style="display:none;">
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="keirsi_test_price" value={{$user['0']->keirsi_price}}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Холл</td>
                                        <td>
                                            @if($holl_passed == 0)
                                                <input type="checkbox" name="holl_test" value="1" <?php $user['0']->holl == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="holl_test" value="1" checked style="display:none;">
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="holl_test_price" value={{$user['0']->holl_price}}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Томас</td>
                                        <td>
                                            @if($tomas_passed == 0)
                                                <input type="checkbox" name="tomas_test" value="1" <?php $user['0']->tomas == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="tomas_test" value="1" checked style="display:none;">
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="tomas_test_price" value={{$user['0']->tomas_price}}>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Соломин</td>
                                        <td>
                                            @if($solomin_passed == 0)
                                                <input type="checkbox" name="solomin_test" value="1" <?php $user['0']->solomin == 1 ? print("checked"):print("");?> class="form-control">
                                            @else
                                                <a style="color:green;">Пройден</a>
                                                <input type="checkbox" name="solomin_test" value="1" checked style="display:none;">
                                            @endif
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="solomin_test_price" value={{$user['0']->solomin_price}}>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; СОХРАНИТЬ!</button>
                    </div>
                    </form>
    </div>



@endsection