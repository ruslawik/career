@extends('subadmin.layout')

@section('title', $title)

@section('content')

    <div class="col-lg-12">
        <a href="javascript:history.back()" class="btn btn-success">Назад</a> <br><br>
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Статистика прохождения теста {{$test_name}} с {{$from_date}} до {{$to_date}}</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            Клиент
                                        </th>
                                        <th>
                                            Сумма
                                        </th>
                                        <th>
                                            Дата прохождения
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sum = 0;
                                ?>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{$test_name}}
                                        </td>
                                        <td>
                                            {{$user->name." ".$user->surname}}
                                        </td>
                                        <td>
                                            <?php
                                                echo $user->$test_name_price;
                                                $sum += $user->$test_name_price;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                echo $user->$test_name_finished_date;
                                            ?>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                            <b>Итог: {{$sum}} тенге</b>
                        </div>
        </div>
    </div>


@endsection