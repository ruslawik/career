@extends('subadmin.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Результаты пройденного теста MAPP пользователя {{ $username }} за {{ $date }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title"></strong>
                </div>
                <div class="card-body">
                    @foreach ($groups as $key => $group)
                                <b>Предпочтение #{{ $key }} </b><br>
                                @foreach ($group as $gr)
                                    @if ($gr->answer($user_id, $date, $gr->id)['answer'] == 1)
                                        <font color="green"><b>Most</b></font>
                                    @endif
                                    @if ($gr->answer($user_id, $date, $gr->id)['answer'] == 0)
                                        <font color="red"><b>Least</b></font>
                                    @endif
                                    @if ($gr->answer($user_id, $date, $gr->id)['answer'] == 3)
                                        Blank
                                    @endif
                                        {{ $gr->text }}<br>
                                @endforeach
                                <br>
                    @endforeach
                </div>
    </div>



@endsection