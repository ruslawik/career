@extends('subadmin.layout')

@section('title', $title)

@section('content')

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пользователь - {{ $name }} {{ $surname }} || Дата сдачи - {{ $date }}</strong>
                        </div>
                    <div class="card-body">
                        <!-- {{ $E }} - {{ $I }} - {{ $S }} - {{ $N }} - {{ $T }} - {{ $F }} - {{ $J }} - {{ $P }} !-->
                        <h4>Формула психотипа: <font color="green">{{ $formula }} </font></h4>
                        <br>Вы можете просмотреть детализацию ответов<hr>
                        <a class="btn-sm btn-success" style="color:white;cursor:pointer;" onClick="show_all_anses();"> Показать/Скрыть детали</a>
                        <div id="all_answers" style="display: none;">
                            <hr>
                            @foreach ($results as $result)
                                Вопрос {{ $result->quest_id }} <br>
                                {{ $result->quest_text->text }} <br>
                                <i>Ваш ответ</i> <b>{{ $result->answer }} </b> - {{ $result->answer_text($result->answer, $result->quest_id)['text'] }}
                                <hr>
                            @endforeach
                        </div>
                    </div>
    </div>

@endsection

@section('mapp_pass_javascript')
<script>
    function show_all_anses(){
        jQuery("#all_answers").toggle();
    }
</script>
@endsection