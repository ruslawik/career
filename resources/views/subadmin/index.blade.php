@extends('subadmin.layout')

@section('title', $title)

@section('content')
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="col-lg-12">
                 <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Период</strong>
                        </div>
                        <div class="card-body">
                            <form action="{{$action}}" method="POST">
                                {{csrf_field()}}
                            <div class="row">
                                <div class="col">Начиная с
                                    <input type="date" class="form-control" name="start_date" value="{{$firstDay}}" />
                                </div>
                                <div class="col">
                                    До
                                    <input type="date" class="form-control" name="end_date" value="{{$lastDay}}" />
                                </div>
                                <div class="col"><br>
                                    <input type="submit" value="Применить фильтр" class="btn btn-success">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

            </div>

    <div class="col-lg-6">

        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Ваша статистика</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            Пройдено кол.
                                        </th>
                                        <th>
                                            Общая сумма
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            MAPP
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/mapp/{{$firstDay}}/{{$lastDay}}">{{$mapp_tests}}</a>
                                        </td>
                                        <td>
                                            {{$mapp_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            DISC
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/disc/{{$firstDay}}/{{$lastDay}}">{{$disc_tests}}</a>
                                        </td>
                                        <td>
                                            {{$disc_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Кейрси
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/keirsi/{{$firstDay}}/{{$lastDay}}">{{$keirsi_tests}}</a>
                                        </td>
                                        <td>
                                            {{$keirsi_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Холл
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/holl/{{$firstDay}}/{{$lastDay}}">{{$holl_tests}}</a>
                                        </td>
                                        <td>
                                            {{$holl_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Томас
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/tomas/{{$firstDay}}/{{$lastDay}}">{{$tomas_tests}}</a>
                                        </td>
                                        <td>
                                            {{$tomas_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Соломин
                                        </td>
                                        <td>
                                            <a href="/subadmin/show_stat/solomin/{{$firstDay}}/{{$lastDay}}">{{$solomin_tests}}</a>
                                        </td>
                                        <td>
                                            {{$solomin_total_sum}}
                                        </td>
                                    </tr>
                            </tbody>
                            </table>
                            <b>Итог:</b>
                            {{($mapp_total_sum)+($disc_total_sum)+($keirsi_total_sum)+($holl_total_sum)+($tomas_total_sum)+($solomin_total_sum)}} тенге
                        </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Дочерние аккаунты (все)</strong>
                        </div>
                        <div class="card-body">

                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Данные об аккаунте
                                        </th>
                                        <th>
                                            Дата выдачи
                                        </th>
                                        <th>
                                            Ред.
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subadmins as $subadmin)
                                    <tr>
                                        <td><a href="/subadmin/subadmin_info/{{$subadmin->id}}">{{$subadmin->name}} {{$subadmin->surname}} | {{$subadmin->login}}<a/></td>
                                        <td>{{$subadmin->created_at}}</td>
                                        <td><a href="/subadmin/edit_subadmin/{{$subadmin->id}}"><i class="fa fa-edit" style="font-size:24px;"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
    </div>

@endsection