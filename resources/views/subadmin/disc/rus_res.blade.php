
@section('res')
                        <hr>
                        <h3>Расшифровка</h3>
                        <hr>
                        Определена формула (Маска): 
                        <b>{{ $most_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_mask[0]))
                        <h4>{{ $description_mask[0]['name'] }} (Маска)</h4>
                        <hr>
                        {!! $description_mask[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_mask[0]['id']}}">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Определена формула (Сердце): 
                        <b>{{ $least_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_least[0]))
                        <h4>{{ $description_least[0]['name'] }} (Сердце)</h4>
                        <hr>
                        {!! $description_least[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_least[0]['id']}}">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Определена формула (Зеркало): 
                        <b>{{ $change_formula }}</b>
                        <?php
                            //print_r($top_change_letters);
                        ?>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description[0]))
                        <h4>{{ $description[0]['name'] }} (Зеркало)</h4>
                        <hr>
                        {!! $description[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description[0]['id']}}">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif

                        <hr>
                        <h3>Топ профессий</h3>
                        <hr>
                        <h4>Формула для подбора профессий: {{$top_prof}}</h4><br>
                        <a href="/topd.docx"><button class="btn btn-success">Скачать профессии типа D</button></a>
                        <a href="/topi.docx"><button class="btn btn-success">Скачать профессии типа I</button></a>
                        <a href="/tops.docx"><button class="btn btn-success">Скачать профессии типа S</button></a>
                        <a href="/topc.docx"><button class="btn btn-success">Скачать профессии типа C</button></a>
                </div>
    </div>
@endsection