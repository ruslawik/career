<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Онлайн форма подачи заявки</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="/manage_res/assets/css/lib/chosen/chosen.min.css">
    @include('manage.__include.css')

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>


        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">CareerVision</a>
                <a class="navbar-brand hidden" href="/">CV</a>
            </div>
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    
                        <h4>{{ $title }}</h4>
                    
                </div>

                <div class="col-sm-5">
                    

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->
        @if (session('status'))
            <div class="content mt-3">
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
            </div>
            @else
        <form action="/application_save" method="POST">
            {{ csrf_field() }}
        <div class="content mt-3">

          <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <strong>Личная информация</strong> <small></small>
                            </div>
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label class="form-control-label">Имя</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-male"></i></div>
                                        <input class="form-control" name="name">
                                    </div>
                                    <small class="form-text text-muted">пример: Ринат</small>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Фамилия</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                        <input class="form-control" name="surname">
                                    </div>
                                    <small class="form-text text-muted">пример: Ванкеев</small>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Пол</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-mars"></i></div>
                                        <select class="form-control" name="sex">
                                            <option value="male">Мужской</option>
                                            <option value="female">Женский</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Дата рождения</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input class="form-control" name="born">
                                    </div>
                                    <small class="form-text text-muted">Пример: 20 марта 1996</small>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Страна, город, адрес проживания</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                        <input class="form-control" name="address">
                                    </div>
                                    <small class="form-text text-muted">Пример: Казахстан, Астана, ул. Республика, 45</small>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Школа</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                                        <input class="form-control" name="school">
                                    </div>
                                    <small class="form-text text-muted">Пример: средняя школа № 1</small>
                                </div>

                                <div class="row">
                                          <div class="col-6">
                                              <div class="form-group">
                                                  <label for="cc-exp" class="control-label mb-1">В каком классе вы учитесь?</label>
                                                  <select class="form-control" name="sinif">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-6">
                                              <label for="x_card_code" class="control-label mb-1">Ожидаемый год выпуска</label>
                                              <div class="input-group">
                                                  <input class="form-control" name="outofdate">
                                                  <div class="input-group-addon">
                                                      <span class="fa fa-question-circle fa-lg"></span>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-6">
                                              <label class=" form-control-label">Ваш номер телефона</label>
                                                <div class="input-group">
                                                  <div class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                                                  <input class="form-control" name="tel">
                                              </div>
                                              <small class="form-text text-muted">Пример: +7 (775) 123 45 56</small>
                                          </div>
                                      </div>
                                    <hr>
                                    <h4>Ваши Будущие планы</h4>
                                    <hr>
                                    Укажите три специальности Вашей мечты, которыми Вы хотели бы заняться
                                    <hr>
                                    <div class="input-group">1&nbsp;<select class="form-control" name="prof1">
                                        <option value="Доктор">Доктор</option>
                                        <option value="Преподаватель">Преподаватель</option>
                                        <option value="Инженер">Инженер</option>
                                        <option value="IT">Специалист по информационным технологиям</option>
                                        <option value="Дизайнер">Дизайнер</option>
                                        <option value="Полицейский">Полицейский</option>
                                        <option value="Юрист">Юрист</option>
                                        <option value="Музыкант">Музыкант</option>
                                        <option value="Экономист">Экономист</option>
                                    </select></div>
                                    <div class="input-group">2&nbsp;<select class="form-control" name="prof2">
                                        <option value="Преподаватель">Преподаватель</option>
                                        <option value="Доктор">Доктор</option>
                                        <option value="Инженер">Инженер</option>
                                        <option value="IT">Специалист по информационным технологиям</option>
                                        <option value="Дизайнер">Дизайнер</option>
                                        <option value="Полицейский">Полицейский</option>
                                        <option value="Юрист">Юрист</option>
                                        <option value="Музыкант">Музыкант</option>
                                        <option value="Экономист">Экономист</option>
                                    </select></div>
                                    <div class="input-group">3&nbsp;<select class="form-control" name="prof3">
                                        <option value="Инженер">Инженер</option>
                                        <option value="Преподаватель">Преподаватель</option>
                                        <option value="Доктор">Доктор</option>
                                        <option value="IT">Специалист по информационным технологиям</option>
                                        <option value="Дизайнер">Дизайнер</option>
                                        <option value="Полицейский">Полицейский</option>
                                        <option value="Юрист">Юрист</option>
                                        <option value="Музыкант">Музыкант</option>
                                        <option value="Экономист">Экономист</option>
                                    </select></div>
                                    <br>
                                    Хотели бы Вы открыть свой бизнес?<br>
                                    Если да, то каким бизнесом хотели бы Вы заняться?
                                    <textarea class="form-control" name="your_business"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <strong>Ваши интересы</strong> <small></small>
                            </div>
                            <div class="card-body card-block">
                                Пожалуйста, укажите уровень Вашей заинтересованности в следующих предметах<hr>
                                <div class="row">
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Математика
                                                  <select class="form-control" name="math">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Биология
                                                  <select class="form-control" name="bio">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Химия
                                                  <select class="form-control" name="chem">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                 <div class="row">
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Физика
                                                  <select class="form-control" name="phys">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Информатика
                                                  <select class="form-control" name="it_interes">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                Пожалуйста, укажите уровень Вашей заинтересованности в следующих внеклассных занятиях
                                <hr>
                                <div class="row">
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Танцы
                                                  <select class="form-control" name="dance">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Пение
                                                  <select class="form-control" name="sing">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-5">
                                              <div class="form-group">
                                                  Игра на музыкальных инструментах
                                                  <select class="form-control" name="music_inses">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                 <div class="row">
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Публичные выступления
                                                  <select class="form-control" name="public_speak">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Писательское творчество
                                                  <select class="form-control" name="write_creat">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                <hr>
                                <h4>Уровень знания языков</h4>
                                <hr>
                                <b>Казахский</b>
                                <div class="row">
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Аудирование
                                                  <select class="form-control" name="kazakh_language_aud">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Письмо
                                                  <select class="form-control" name="kazakh_language_write">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Чтение
                                                  <select class="form-control" name="kazakh_language_read">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Речь
                                                  <select class="form-control" name="kazakh_language_speak">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                <hr>
                                <b>Русский</b>
                                <div class="row">
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Аудирование
                                                  <select class="form-control" name="russian_language_aud">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Письмо
                                                  <select class="form-control" name="russian_language_write">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Чтение
                                                  <select class="form-control" name="russian_language_read">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Речь
                                                  <select class="form-control" name="russian_language_speak">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                <hr>
                                <b>Английский</b>
                                <div class="row">
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Аудирование
                                                  <select class="form-control" name="english_language_aud">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Письмо
                                                  <select class="form-control" name='english_language_write'>
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Чтение
                                                  <select class="form-control" name="english_language_read">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Речь
                                                  <select class="form-control" name="english_language_speak">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                                <hr>
                                <b>Другое</b>
                                <input class="form-control col-sm-5" name="other_lang">
                                <div class="row">
                                          <div class="col-4">
                                              <div class="form-group">
                                                  Аудирование
                                                  <select class="form-control" name="other_language_aud">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-3">
                                              <div class="form-group">
                                                  Письмо
                                                  <select class="form-control" name="other_language_write">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Чтение
                                                  <select class="form-control" name="other_language_read">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                          <div class="col-2">
                                              <div class="form-group">
                                                  Речь
                                                  <select class="form-control" name="other_language_speak">
                                                        <option value='1'>1</option>
                                                        <option value='2'>2</option>
                                                        <option value='3'>3</option>
                                                        <option value='4'>4</option>
                                                        <option value='5'>5</option>
                                                    </select>
                                              </div>
                                          </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Укажите Ваши ожидания от нашей программы</strong> <small></small>
                            </div>
                            <div class="card-body card-block">
                                <b>Определение признаков личности</b>
                                <select data-placeholder="Можете выбрать несколько вариантов" multiple class="standardSelect" name="expect1[]">
                                    <option value=""></option>
                                    <option value="1">Тест на тип личности</option>
                                    <option value="2">Консультация</option>
                                    <option value="3">Консультация с родителями</option>
                                    <option value="4">Консультация с учителями</option>
                                    <option value="5">Тренинги</option>
                                </select>
                                <b>Профессиональная диагностика</b>
                                <select data-placeholder="Можете выбрать несколько вариантов" multiple class="standardSelect" name="expect2[]">
                                    <option value=""></option>
                                    <option value="1">Диагностический тест MAPP (30+ страниц личного отчета)</option>
                                    <option value="2">Диагностический тест Keyer (5+ страниц личного отчета)</option>
                                    <option value="3">Генетик-тест (5+ страниц личного отчета)</option>
                                    <option value="4">Тренинги и семинары с психологом</option>
                                    <option value="5">Консультации по всем тестам</option>
                                    <option value="6">Лекции от представителей разных профессий</option>
                                </select>
                                <b>Практический опыт</b>
                                <select data-placeholder="Можете выбрать несколько вариантов" multiple class="standardSelect" name="expect3[]">
                                    <option value=""></option>
                                    <option value="1">Личные консультации с представителями разных профессий</option>
                                    <option value="2">Поездки в организации</option>
                                    <option value="3">Стажировки (наблюдения, консультации)</option>
                                    <option value="4">Бизнес-кейсы / практические занятия</option>
                                    <option value="5">Ориентационная программа</option>
                                </select>
                                <hr>
                                <h4>По какой программе вы хотели бы работать?</h4>
                                <hr>
                                <table class="table">
                                    <thead>
                                        <td><b>Описание</b></td>
                                        <td><b>Период</b></td>
                                        <td><b></b></td>
                                        <td><b>Выбрать</b></td>
                                    </thead>
                                    <tr>
                                        <td width="50%">Полный пакет (консультации с родителями и преподавателями, диагностика с использованием 4 тестов, тренинги, бизнес-кейсы, ориентационные программы, стажировки)</td>
                                        <td>1 месяц</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work1" value="w1"></td>
                                    </tr>
                                    <tr>
                                        <td>Консультация с родителями</td>
                                        <td>2-3 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work2" value="w2"></td>
                                    </tr>
                                    <tr>
                                        <td>Консультация с учителями</td>
                                        <td>1-2 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work3" value="w3"></td>
                                    </tr>
                                    <tr>
                                        <td>Диагностика с комплексным тестирование MAPP + консультация (более 30 страниц личного отчета) </td>
                                        <td>3 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work4" value="w4"></td>
                                    </tr>
                                    <tr>
                                        <td>Диагностика с помощью Генетик-тест</td>
                                        <td>2 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work5" value="w5"></td>
                                    </tr>
                                    <tr>
                                        <td>Диагностика с помощью теста Keyer + консультация</td>
                                        <td>2 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work6" value="w6"></td>
                                    </tr>
                                    <tr>
                                        <td>Диагностика с помощью теста Личности + консультация</td>
                                        <td>2 часа</td>
                                        <td></td>
                                        <td><input type="checkbox" name="work7" value="w7"></td>
                                    </tr>
                                </table>
                                <input type="submit" style="float:right;" class="btn btn-success" value="Отправить заявку">
                            </div>
                        </div>
                    </div>
                </form>
                @endif
        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="/manage_res/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="/manage_res/assets/js/plugins.js"></script>
    <script src="/manage_res/assets/js/main.js"></script>
    <script src="/manage_res/assets/js/lib/chosen/chosen.jquery.min.js"></script>

    <script>
        jQuery(document).ready(function() {
            jQuery(".standardSelect").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
        });
    </script>

</body>
</html>
