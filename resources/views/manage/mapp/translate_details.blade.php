@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете редактировать текст переводов результатов теста MAPP
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
        <a href="/manage/mapp_tercume"><button class='btn btn-secondary'>Переводы названий профессий</button></a>
        <a href="/manage/mapp_tercume_jobdesc"><button class='btn btn-secondary'>Переводы описаний профессий</button></a>
        <a href="/manage/mapp_tercume_details"><button class='btn btn-warning'>Переводы детализации профессий</button></a>
        <a href="/manage/mapp_tercume_all"><button class='btn btn-secondary'>Общие переводы</button></a>
            <br><br>
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Переводы</strong>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                            <br><br>
                            {{ $jobs->links() }}
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>
                                            <b>Английский</b>
                                        </td>
                                        <td>
                                            <b>Русский</b>
                                        </td>
                                        <td>
                                            <b>Казахский</b>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $k = 0;
                                ?>
                                @foreach ($jobs as $job)
                                    <?php $k++; ?>
                                    <tr>
                                        <td>
                                             <input type="hidden" name="trans_{{$k}}" value="{{$job->id}}">
                                            <textarea class="form-control" name="en_row_{{$k}}">{{$job->detail_name}}</textarea>
                                        </td>
                                        <td>
                                            <textarea class="form-control" name="ru_row_{{$k}}">{{$job->detail_name_rus}}</textarea>
                                        </td>
                                        <td>
                                            <textarea class="form-control" name="kz_row_{{$k}}">{{$job->detail_name_kaz}}</textarea>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" name="total_val" value="{{$k}}">
                            <br>
                            <button type="submit" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                            <br><br>
                            {{ $jobs->links() }}
                            <br>
                        </div>
                    </div>

             </form>
            
    </div>



@endsection