@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете изменить 35 вопросов
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Вопросы теста</strong>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                        </div>
                        <div class="card-body">
                            @foreach ($questions as $question)
                                Вопрос {{ $question->id }} - группа {{ $question->group_id }}
                                <input type="text" class="form-control" value="{{ $question->text }}" name="q{{ $question->id }}">
                                <br>
                            @endforeach
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                        </div>
                    </div>
            </form>
    </div>



@endsection