<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Центр управления тестами</title>
    <meta name="description" content="CareerVision">
    <meta name="viewport" content="width=device-width, initial-scale=1">

      @include('manage.__include.css')
      <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
</head>
<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="/">
                        <h3 style="color:white;">CareerVision</h3>
                        <h6 style="color:white;">РЕГИСТРАЦИЯ</h6>
                    </a>
                </div>
                <div class="login-form">
                    @if (session('error'))
                        <div class="alert alert-success">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="checkbox">
                            <label class="pull-right">
                                <a href="/login">Перейти к странице входа</a>
                            </label>

                        </div>
                    <form method="POST" id="reg_form" action="{{ $action }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Придумайте логин</label>
                            <input type="text" class="form-control" onChange="is_exists();" id="login" name="login" placeholder="Введите логин">
                            <p id="is_exists"></p>
                        </div>
                        <div class="form-group">
                            <label>Введите фамилию</label>
                            <input type="text" class="form-control" id="surname" name="surname" placeholder="Введите фамилию">
                        </div>
                        <div class="form-group">
                            <label>Введите имя</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя">
                        </div>
                        <div class="form-group">
                            <label>Придумайте пароль</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль">
                        </div>
                        <div class="form-group">
                            <label>Повторите пароль</label>
                            <input type="password" class="form-control" id="password2" name="password2" placeholder="Введите пароль">
                        </div>
                        <div class="form-group">
                            <label>Введите email (будут высланы Ваши данные)</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Введите email">
                        </div>
                        <div class="form-group" id="tests_to_pass">
                             <label>Выберите тест для прохождения</label><br>
                            <!--<input type="checkbox" name="MAPP" value="25000"> MAPP тест - 25 000 тенге <br>!-->
                            <input type="checkbox" name="DISC" value="10000"> DISC тест - 10 000 тенге<br>
                            <!--
                            <input type="checkbox" name="holl"> Тест Холла <br>
                            <input type="checkbox" name="keirsi"> Кейрси тест <br>
                            <input type="checkbox" name="tomas"> Тест Томаса <br>
                            <input type="checkbox" name="tomas"> Тест Соломина <br>!-->
                        </div>
                        <div class="form-group">
                            <label>Есть промо-код?</label>
                            <input type="text" class="form-control" name="promo" placeholder="Введите промо-код">
                        </div>
                        <input type="hidden" id="all_sum" value="0">
                        <input type="hidden" id="pay_description" value="">
                        <input type="hidden" id="er_login" value="0">
                        <button type="button" id="pay" onClick="get_tests_sum();" class="btn btn-success btn-flat m-b-30 m-t-30">ПЕРЕЙТИ К ОПЛАТЕ ></button>
                        <center><a target="_blank" href="/cloudpayments">Информация об онлайн платежах</a></center>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="/manage_res/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="/manage_res/assets/js/plugins.js"></script>
    <script src="/manage_res/assets/js/main.js"></script>

    <script>

    function is_exists (){
        var login = jQuery("#login").val();
        jQuery.ajax({
                type: "GET",
                url: "/is_exists",
                beforeSend: function(){

                },
                data: {login: login},
                success: function(answer) {
                    if(answer == 1){
                        jQuery("#er_login").val(1);
                        jQuery("#is_exists").html("Такой логин уже существует");
                    }else{
                        jQuery("#er_login").val(0);
                        jQuery("#is_exists").html("Этот логин свободен");
                    }
                }
            });
    }

    this.pay = function () {
    var widget = new cp.CloudPayments();
    widget.charge({ // options
            publicId: 'pk_fb30e07a2d7e7748f7a6a5052c38d',  //id из личного кабинета
            description: 'Оплата тестов: '+jQuery("#pay_description").val(), //назначение
            amount: parseInt(jQuery("#all_sum").val()), //сумма
            currency: 'KZT', //валюта
            //invoiceId: '1234567', //номер заказа  (необязательно)
            accountId: jQuery("#email").val(), //идентификатор плательщика (необязательно)
            skin: "classic", //дизайн виджета
            data: {
                myProp: 'myProp value' //произвольный набор параметров
            }
        },
        function (options) { // success
            jQuery("#reg_form").submit();
        },
        function (reason, options) { // fail
            //alert("FAIL");
        });
    };    

    function get_tests_sum(){
        var ok = true;
        var all_sum = 0;
        var description = "";
        jQuery('#tests_to_pass input:checked').each(function() {
                all_sum += parseInt(jQuery(this).attr('value'));
                description += jQuery(this).attr('name')+" - "+jQuery(this).attr('value')+" тг. ";
        });
        //description += " = "+all_sum+" тг.";
        jQuery("#all_sum").val(all_sum);
        jQuery("#pay_description").val(description);

        if(jQuery("#er_login").val() == 1){
            alertModal("Такой логин уже существует", "Придумайте другой логин для Вашей идентификации");
            ok = false;
        }

        if(jQuery("#login").val().length < 1) {
            alertModal("Поле логин обязательно для заполнения!", "Заполните данные");
            ok = false;
        }

        if(all_sum == 0){
            alertModal("Выберите тест!", "Необходимо выбрать тесты для прохождения");
            ok = false;
        }
        
        if (jQuery("#name").val().length < 1) {
            alertModal("Поле имя обязательно для заполнения!", "Заполните данные");
            ok = false;
        }
        if (jQuery("#surname").val().length < 1) {
            alertModal("Поле фамилия обязательно для заполнения!", "Заполните данные");
            ok = false;    
        }
        if (jQuery("#password").val().length < 1) {
            alertModal("Поле пароль обязательно для заполнения!", "Заполните данные");
            ok = false;          
        }
        if (jQuery("#password2").val().length < 1) {
            alertModal("Поле повторите пароль обязательно для заполнения!", "Заполните данные");
            ok = false;
        }
        if (jQuery("#password").val() != jQuery("#password2").val()) {
            alertModal("Введенные пароли не совпадают!", "Проверьте пароль");
            ok = false;
        }
        if (jQuery("#email").val().length < 1) {
            alertModal("Поле эл. почта обязательно для заполнения!", "Заполните данные");    
            ok = false;                    
        }
        if(ok){
            pay();
        }
    }


    function alertModal(title, body) {
        // Display error message to the user in a modal
        jQuery('#alert-modal-title').html(title);
        jQuery('#alert-modal-body').html(body);
        jQuery('#alert-modal').modal('show');
    }
    </script>

    <div id="alert-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="alert-modal-title" class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div id="alert-modal-body" class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
