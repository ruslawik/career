<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Центр управления тестами</title>
    <meta name="description" content="CareerVision">
    <meta name="viewport" content="width=device-width, initial-scale=1">

      @include('manage.__include.css')
</head>
<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="/">
                        <h3 style="color:white;">CareerVision</h3>
                        <h6 style="color:white;">Центр управления тестами</h6>
                    </a>
                </div>
                <div class="login-form">
                    @if (session('error'))
                        <div class="alert alert-success">
                            {{ session('error') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ $action }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Ваш логин</label>
                            <input type="text" class="form-control" name="login" placeholder="Введите логин">
                        </div>
                        <div class="form-group">
                            <label>Пароль</label>
                            <input type="password" class="form-control" name="password" placeholder="Введите пароль">
                        </div>
                        <div class="checkbox">
                            <label class="pull-right">
                                <a href="/regpage">Регистрация</a>
                            </label>

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Войти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="/res/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="/res/assets/js/popper.min.js"></script>
    <script src="/res/assets/js/plugins.js"></script>
    <script src="/res/assets/js/main.js"></script>


</body>
</html>
