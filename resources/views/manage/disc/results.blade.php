@extends('manage.layout')

@section('title', $title)

@section('content')
    <style>
        p{
            color:black !important;
        }
    </style>
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Результаты пройденного теста DISC пользователя {{ $username }} за {{ $date }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title"></strong>
                </div>
                <div class="card-body">
                    <table class="table">
                            <thead>
                                <td></td>
                                <td>D</td>
                                <td>I</td>
                                <td>S</td>
                                <td>C</td>
                                <td>*</td>
                                <td>Total score</td>
                            </thead>
                            <tr>
                                <td>MOST</td>
                                <td>{{ $most['D'] }}</td>
                                <td>{{ $most['I'] }}</td>
                                <td>{{ $most['S'] }}</td>
                                <td>{{ $most['C'] }}</td>
                                <td>{{ $most['*'] }}</td>
                                <td>{{ $most['total'] }}</td>
                            </tr>
                            <tr>
                                <td>LEAST</td>
                                <td>{{ $least['D'] }}</td>
                                <td>{{ $least['I'] }}</td>
                                <td>{{ $least['S'] }}</td>
                                <td>{{ $least['C'] }}</td>
                                <td>{{ $least['*'] }}</td>
                                <td>{{ $least['total'] }}</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td><center><b>Graph 1 MOST</b></center><img src="{{ $img_most_graph }}" style="margin-right:20px;"></td>
                                <td><center><b>Graph 2 LEAST</b></center><img src="{{ $img_least_graph }}"  style="margin-right:20px;"></td>
                                <td><center><b>Graph 3 CHANGE</b></center><img src="{{ $img_change_graph }}"  style="margin-right:20px;"></td>
                            </tr>
                        </table>
                        @if($lang=="ru")
                        <hr>
                        <h3>Расшифровка</h3> (<a href="kz">Казахский</a>)
                        <hr>
                        Определена формула (Маска): 
                        <b>{{ $most_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_mask[0]))
                        <h4>{{ $description_mask[0]['name'] }} (Маска)</h4>
                        <hr>
                        {!! $description_mask[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_mask[0]['id']}}/ru">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Определена формула (Сердце): 
                        <b>{{ $least_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_least[0]))
                        <h4>{{ $description_least[0]['name'] }} (Сердце)</h4>
                        <hr>
                        {!! $description_least[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_least[0]['id']}}/ru">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Определена формула (Зеркало): 
                        <b>{{ $change_formula }}</b>
                        <?php
                            //print_r($top_change_letters);
                        ?>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description[0]))
                        <h4>{{ $description[0]['name'] }} (Зеркало)</h4>
                        <hr>
                        {!! $description[0]['desciption'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description[0]['id']}}/ru">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif

                        <hr>
                        <h3>Топ профессий</h3>
                        <hr>
                        <h4>Формула для подбора профессий: {{$top_prof}}</h4><br>
                        <a href="/topd.docx"><button class="btn btn-success">Скачать профессии типа D</button></a>
                        <a href="/topi.docx"><button class="btn btn-success">Скачать профессии типа I</button></a>
                        <a href="/tops.docx"><button class="btn btn-success">Скачать профессии типа S</button></a>
                        <a href="/topc.docx"><button class="btn btn-success">Скачать профессии типа C</button></a>
                        @endif
                        @if($lang=="kz")
                           <hr>
                        <h3>Шифрын анықтау</h3>(<a href="ru">Орысша</a>)
                        <hr>
                        Бетперде формуласы: 
                        <b>{{ $most_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_mask[0]))
                        <h4>{{ $description_mask[0]['kaz_name'] }} (Бетперде)</h4>
                        <hr>
                        {!! $description_mask[0]['kaz_description'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_mask[0]['id']}}/kz">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Негізі, Ішкі мен формуласы: 
                        <b>{{ $least_formula }}</b>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description_least[0]))
                        <h4>{{ $description_least[0]['kaz_name'] }} (Ішкі мен)</h4>
                        <hr>
                        {!! $description_least[0]['kaz_description'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description_least[0]['id']}}/kz">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif
                        <hr>
                        Айна формуласы: 
                        <b>{{ $change_formula }}</b>
                        <?php
                            //print_r($top_change_letters);
                        ?>
                        <!--<hr>Определена модель (Вы можете редактировать соответствие формул моделям <a href="/manage/disc_formulas">здесь</a>):<br><br>!-->
                        @if(isset($description[0]))
                        <h4>{{ $description[0]['kaz_name'] }} (Айна, Саналы мен)</h4>
                        <hr>
                        {!! $description[0]['kaz_description'] !!}
                        <a target="_blank" href="/manage/disc_text_edit/{{$description[0]['id']}}/kz">(Редактировать внешний вид текста)</a>
                        @else
                            <br>
                            <font color="red">Такая формула не имеет ни одной связи с определенной моделью</font>
                            <a href="/manage/disc_formulas">СВЯЗАТЬ</a>
                        @endif

                        <hr>
                        <h3>Мамандықтар</h3>
                        <hr>
                        <h4>Формула: {{$top_prof}}</h4><br>
                        <a href="/topd_kaz.docx"><button class="btn btn-success">D мамандықтары</button></a>
                        <a href="/topi_kaz.docx"><button class="btn btn-success">I мамандықтары</button></a>
                        <a href="/tops_kaz.docx"><button class="btn btn-success">S мамандықтары</button></a>
                        <a href="/topc_kaz.docx"><button class="btn btn-success">C мамандықтары</button></a> 
                        @endif
                </div>
    </div>



@endsection