@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Для удобства вставки текста нужного формата в отчет 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Редактировать текст</strong>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ $action }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$id}}">
                         <input type="hidden" name="lang" value="{{$lang}}">
                         <textarea class="form-control" name="disc_text" id="summary-ckeditor">{{$text}}</textarea>
                         <hr>
                         <input type="submit" value="Сохранить" class="btn btn-success">
                    </form>
                </div>
    </div>



@endsection

@section('datatable_js')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection