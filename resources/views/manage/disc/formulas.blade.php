@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Добавляйте и редактируйте (удаление невозможно)
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <form method="POST" action="{{ $action_add }}">
                <div class="card-header">
                    <strong class="card-title">Соответствие формул типам моделей</strong>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить</button>
                        <br><br>
                </div>
            </form>
                <div class="card-body">
                     <form method="POST" action="{{ $action }}">
                        {{ csrf_field() }}
                    <table class="table table-striped">
                        <thead><td>Формула</td><td>Модель</td></thead>
                   @foreach($all_formulas as $formula)
                        <tr>
                            <td>
                            <input type="text" name="f{{$formula['id']}}" value="{{ $formula['formula'] }}" class="form-control">
                            </td>
                            <td>
                            <select class="form-control" name="t{{$formula['id']}}">
                            @foreach($all_types as $type)
                                @if($type['id']==$formula['type_id'])
                                    <option value="{{$type['id']}}">{{$type['name']}}</option>
                                @endif
                            @endforeach
                            @foreach($all_types as $type)
                                @if($type['id']!=$formula['type_id'])
                                    <option value="{{$type['id']}}">{{$type['name']}}</option>
                                @endif
                            @endforeach
                            </select>
                            </td>
                        </tr>
                   @endforeach
                    <tr>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-save"></i>&nbsp; Сохранить</button>
                        </td>
                    </tr>
                    </table>
                </form>
                </div>
                </form>
    </div>



@endsection