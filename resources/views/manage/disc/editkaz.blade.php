@extends('manage.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Назар аударыңыз!</span> Введите вопросы и соответствие букв D, I, S, C, * в нужные поля</a>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            Редактор
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сақтау</button>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <td>
                                @foreach($questions as $question)
                                    M - <input type="text" name="most{{$question->id}}" style="width:25px;" value="{{ $question->most }}">
                                    L - <input type="text" name="least{{$question->id}}" style="width:25px;" value="{{ $question->least }}">
                                        <input type="text" name="question{{$question->id}}" value="{{ $question->question }}"><br>
                                    @if($question->id % 12 === 0 || $question->id === 0)
                                        </td></tr><tr><td>
                                    @endif
                                    @if($question->id % 4 === 0 && $question->id % 12 !== 0)</td><td>
                                    @endif
                                @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
            </form>
    </div>



@endsection