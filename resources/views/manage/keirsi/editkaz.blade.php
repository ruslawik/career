@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Редактор казахского варианта теста КЕЙРСИ
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Вопросы теста</strong>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                        </div>
                        <div class="card-body">
                            @foreach ($questions as $question)
                                Вопрос {{ $question->id }} 
                                <a class="btn-sm btn-success" style="color:white;cursor:pointer;" onClick="show_anses_div({{ $question->id }});"> Ответы</a>
                                <input type="text" class="form-control" value="{{ $question->text }}" name="q{{ $question->id }}">
                                <div id="anses{{ $question->id }}" style="display:none;">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Ответ а:</div>
                                            <input type="text" class="form-control col-sm-5" name="q{{ $question->id }}a" value="{{ $question->answer_a['text'] }}">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon">Ответ б:</div>
                                            <input type="text" class="form-control col-sm-5" name="q{{ $question->id }}b" value="{{ $question->answer_b['text'] }}">
                                        </div>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                        </div>
                    </div>
            </form>
    </div>



@endsection