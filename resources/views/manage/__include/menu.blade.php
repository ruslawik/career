@if(\Auth::user()->id == 1)
<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/manage/admin"> <i class="menu-icon fa fa-dashboard"></i>Главная </a>
                    </li>
                    <h3 class="menu-title">Редактор тестов</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Кейрси</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/keirsi_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/keirsi_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>MAPP</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/mapp_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/mapp_edit_kaz">Редактировать КАЗ</a></li>
                            <li><i class="fa fa-language"></i><a href="/manage/mapp_tercume">ПЕРЕВОДЫ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>DISC</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_formulas">Формулы</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit_kaz">Редактировать КАЗ</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit_en">Редактировать ENG</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Холл</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/holl_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/holl_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Томас</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/tomas_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/tomas_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Соломин</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin_edit">Редактировать 1</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin_editkaz">Редактировать 1 КАЗ</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin2_edit">Редактировать 2</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin2_editkaz">Редактировать 2 КАЗ</a></li>
                        </ul>
                    </li>

                    <h3 class="menu-title">Пользователи</h3><!-- /.menu-title -->
                        <!--<li><a href="/manage/applications"><i class="menu-icon fa fa-list"></i>Заявки</a></li>!-->
                        <li><a href="/manage/all_users"><i class="menu-icon fa fa-list"></i>Все пользователи</a></li>
                        <li><a href="/manage/add_user"><i class="menu-icon fa fa-puzzle-piece"></i>Добавить</a></li>
                </ul>
@else
    <h3 class="menu-title"><center>Статистика продаж</center></h3>
@endif