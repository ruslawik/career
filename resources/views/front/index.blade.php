<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CareerVision.kz</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- css -->
	<link href="/front_res/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/front_res/plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
	<link href="/front_res/css/cubeportfolio.min.css" rel="stylesheet" />
	<link href="/front_res/css/style.css" rel="stylesheet" />


	<!-- Theme skin -->
	<link id="t-colors" href="/front_res/skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="/front_res/bodybg/bg1.css" rel="stylesheet" type="text/css" />

	<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
	======================================================= -->

</head>

<body>

	<div id="wrapper">
		<!-- start header -->
		<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li><i class="fa fa-phone"></i> +7 775 000 57 94</li>
							</ul>
						</div>
						<div class="col-md-6">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search" title="Click to start searching"></span>
								</form>
							</div>


						</div>
					</div>
				</div>
			</div>

			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
						<a class="navbar-brand" href="/"><img src="/front_res/img/logo.jpg" alt="" width="199" height="52" /></a>
					</div>
					<div class="navbar-collapse collapse ">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Главная <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">О нас</a></li>

								</ul>

							</li>
							<li><a href="#">Наши клиенты</a></li>
							<li><a href="#">Контакты</a></li>
							<li><a href="/application">Подать заявку!</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<ul class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							<li class="active">Главная</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section id="content">
			<div class="container">
				<div class="row">

					<div class="col-lg-4">
						<aside class="left-sidebar">
							<div class="widget">
								<form role="form">
									<div class="form-group">
										<input type="text" class="form-control" id="s" placeholder="Поиск..">
									</div>
								</form>
							</div>
							<div class="widget">
								<h5 class="widgetheading">Типы тестов</h5>
								<ul class="cat">
									<li><i class="fa fa-angle-right"></i><b>MAPP</b> - определяет ТОП-20 подходящих профессий,                                                                 отчет на 30 страниц, определяет  потенциальную сферу деятельности, личные качества, математический склад, и способности к изучению языков. </li>
									<li><i class="fa fa-angle-right"></i><b>Кейрси тест</b> - личностный опросник, позволяющий более подробно узнать себя как изнутри, так и в обществе. Определяет выбор будущей карьеры.</li>
									<li><i class="fa fa-angle-right"></i><b>DISC тест</b> - четырехсекторная поведенческая модель для исследования людей в окружающей их среде или в определенной ситуации. Предназначается для оценки: стиля поведения; коммуникативных и личностных навыков; мотиваторов; потенциала компетенций; роли в команде; эмоционального интеллекта.</li>
									<li><i class="fa fa-angle-right"></i><b>Генетический тест</b> - помогает понять себя, раскрыть свой потенциал, обнаружить талант и расставить приоритеты в развитии, выбрать профессию, вид спорта или хобби. </li>
								</ul>
							</div>
						</aside>
					</div>

					<div class="col-lg-8">
						<article>
							<div class="post-image">
								<div class="post-heading">
									<h3><a href="#">Консалтинговый центр “Career Vision Kazakhstan”</a></h3>
								</div>
								<img src="/front_res/img/clients.png" alt="" class="img-responsive" />
							</div>
							<p>
								
							</p>
						</article>

						<article>
							<div class="post-slider">
								<div class="post-heading">
									<h3><a href="#">Пакеты услуг</a></h3>
								</div>
								<!-- start flexslider -->
								<div id="post-slider" class="postslider flexslider">


								<ul class="nav nav-tabs">
									<li class="active"><a href="#one" data-toggle="tab"><i class="icon-briefcase"></i> Пакет услуг для учеников</a></li>
									<li><a href="#two" data-toggle="tab">Пакет Услуг для профориентаторов и школ</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="one">
										<ul><li>Диагностика на основе 4х тестов </li>
<li>Консультация со специалистом по карьере</li>
<li>Менторство с профессионалами</li>
<li>Составление индивидуального плана развития</li>
<li>Экскурсии в организациях</li>
<li>2,3-х недельные стажировки в компаниях</li>
<li>Решение бизнес кейсов</li>
<li>Индивидуальная характеристика для родителей</li>
<li>Подготовка к поступлению в ВУЗы.</li>
<li>Командные/корпоративные тренинги, воркшопы, тимбилдинги по распределению ролей, обязанностей в команде</li>
</ul>
									</div>
									<div class="tab-pane" id="two">
										<ul>
<li>Обучение диагностике на основе 4-х тестов </li>
<li>Тренинг по проведению консультаций</li>
<li>Шаблон составления индивидуального плана действий</li>
<li>Решение бизнес кейсов</li>
<li>Тренинг по содействию в выборе и поступлении в ВУЗы (рекомендательные письма, CV, мотивационные письма и т.д.), подборка подходящих курсов.</li>
<li>Тематические Семинары</li>
</ul>
									</div>
								</div>
   <br>
<img src="/front_res/img/students.jpg" width="100%">

								</div>								<!-- end flexslider -->
							</div>
						</article>
						<article>
							<div class="post-quote">
								<div class="post-heading">
									<h3><a href="#">Семинары + Программа онлайн менторства от профессионалов</a></h3>
								</div>
								

								<ul class="nav nav-tabs">
									<li class="active"><a href="#one1" data-toggle="tab"><i class="icon-briefcase"></i>Семинары</a></li>
									<li><a href="#two1" data-toggle="tab">Программа онлайн менторства от профессионалов</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="one1">
										<ul><li>
										Секреты Успешного Time Management”</li>
<li>“Как поступить в университет, и для чего?”</li>
<li>“Как можно найти работу без диплома (дуальное образование)?”</li>
<li>“Stress-relief: Как снизить уровень тревожности у абитуриента?”</li>
<li>“WORK hard to PLAY hard - Продуктивность и эффективность в подготовке к экзаменам”</li>
<li>“Профессии будущего и что уже не актуально”</li></ul>
									</div>
									<div class="tab-pane" id="two1">
										<ul>
<li>Индивидуальные поэтапные онлайн встречи </li>
<li>Получение домашнего задания</li>
<li>Разработка плана карьерного роста</li>
<li>Детальная оценка креативности, способностей, соответствие профессиональным интересам</li>
<li>Dual evaluation</li>
</ul>
									</div>
								</div>
								<img src="/front_res/img/graph.png" width="100%">
								<iframe src="https://drive.google.com/file/d/1zI8CpW1c7eHjw_YaQtAtxzKaWgCgiHdc/preview" width="100%" height="480"></iframe>
								<iframe src="https://drive.google.com/file/d/1IdIeIQVm1SCr-gOWWxkI4YrdEQZNURtj/preview" width="100%" height="480"></iframe>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Связаться с нами</h4>
							<address>
					<strong>Career Vision Kazakhstan</strong><br>
					 Программа Профессиональной профориентации<br>
					 <b>+7 775 000 57 94</b></address>
						</div>
					</div>
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Подписка</h4>
							<p>Введите Ваш E-mail для обратной связи или <a href="/application">оставьте заявку</a></p>
							<div class="form-group multiple-form-group input-group">
								<input type="email" name="email" class="form-control">
								<span class="input-group-btn">
                            <button type="button" class="btn btn-theme btn-add">Связаться</button>
                        </span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sub-footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="copyright">
								<p>&copy; Sailor Theme - All Right Reserved</p>
								<div class="credits">
									<!--
                    All the links in the footer should remain intact. 
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Sailor
                  -->
									Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<ul class="social-network">
								<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/front_res/js/jquery.min.js"></script>
	<script src="/front_res/js/modernizr.custom.js"></script>
	<script src="/front_res/js/jquery.easing.1.3.js"></script>
	<script src="/front_res/js/bootstrap.min.js"></script>
	<script src="/front_res/plugins/flexslider/jquery.flexslider-min.js"></script>
	<script src="/front_res/plugins/flexslider/flexslider.config.js"></script>
	<script src="/front_res/js/jquery.appear.js"></script>
	<script src="/front_res/js/stellar.js"></script>
	<script src="/front_res/js/classie.js"></script>
	<script src="/front_res/js/uisearch.js"></script>
	<script src="/front_res/js/jquery.cubeportfolio.min.js"></script>
	<script src="/front_res/js/google-code-prettify/prettify.js"></script>
	<script src="/front_res/js/animate.js"></script>
	<script src="/front_res/js/custom.js"></script>

</body>

</html>
