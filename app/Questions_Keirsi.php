<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answers_Keirsi;

class Questions_Keirsi extends Model
{
    protected $table = 'questions_keirsi';

    public function answer_a()
    {
        return $this->hasOne('App\Answers_Keirsi', 'question_id')->where('type', 'a');
    }

    public function answer_b()
    {
        return $this->hasOne('App\Answers_Keirsi', 'question_id')->where('type', 'b');
    }
}