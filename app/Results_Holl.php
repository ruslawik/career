<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results_Holl extends Model
{
    protected $table = "results_holl";

    function quest_text (){
    	return $this->hasOne("App\Questions_Holl", 'id', 'quest_id');
    }
}
