<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\MappJob;
use App\MappJobDetails;
use App\MappResPercent;

class MappAuto extends Controller
{

	public function mapp_send (Request $r){

		if(Auth::check()){

			$ar = $r->input('array_to_send');
			$l = count($ar);
			$group_quest_amount = $r->input('group_quest_amount');

			//если не все вопросы отвечены (в каждом вопросе два варианта должно быть отмечено)
			if($l < $group_quest_amount*2){
				//выдаем ошибку что нужно ответить на все вопросы
				die("err_fill_all_fields");
			}
			auth_made:
			//если мы авторизовались и существует файл с куками от сервера assestment
			if(file_exists( public_path().'/mapp_cookies/'.Auth::user()->id)){
				//главный массив на отсылку в assestment.com
				
				$least_quest = 10000000000;
				
				$most_quest = -1000000000;
				$MAIN_DATA = array();
				for ($i = 0; $i < $l; $i++){

					$selected_answer = $ar[$i];
					$selected_answer_data = explode("_", $selected_answer);

					//вопрос до которого мы будем совершать отсылку на сервера assestment
					if($selected_answer_data[1]>$most_quest){
						$most_quest = $selected_answer_data[1];
					}
					//вопрос с которого мы будем начинать отсылку на сервера assestment
					if($selected_answer_data[1]<$least_quest){
						$least_quest = $selected_answer_data[1];
					}
					//тип ответа - 1 и 0 у нас хранятся как least и most
					switch ($selected_answer_data[3]) {
						//если тип most то сохраняем в массив MAIN_DATA на позицию вопроса его данные ответа most
						case '1':
								$MAIN_DATA[$selected_answer_data[1]]['M'] = $selected_answer_data[2];
							break;
						//если тип least то сохраняем в массив MAIN_DATA на позицию вопроса его данные ответа least
						case '0':
								$MAIN_DATA[$selected_answer_data[1]]['L'] = $selected_answer_data[2];
							break;
					}
				}
				//самая важная часть - идем по массиву с ответами циклом и ОТПРАВЛЯЕМ НА СЕРВЕРА
				//assestment.com!!!!! с помощью метода send_question с текущими куками нужные ответы на нужные вопросы
				// $i - номер вопроса
				for($i = $least_quest; $i<=$most_quest; $i++){
					$this->send_question (Auth::user()->id, $i, $MAIN_DATA[$i]['M'], $MAIN_DATA[$i]['L']);
				}
			}else{
				//если не авторизовались то авторизуемся, сохраняем куки и идем на метку auth_made выше по коду
				$this->mapp_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id);
				goto auth_made;
			}
		}
		//print_r($ar);
	}

	public function send_question ($cookies_file_id, $q_number, $most, $least){
     	//запрашиваем текущую страницу вопроса чтобы вырвать оттуда request_token 
     	$req_token = $this->getRequestToken($cookies_file_id, "http://recruit.assessment.com/CareerCenter/TakeMAPP/TakeTheMAPP/".$q_number."?selectedLanguage=ENGLISH");
     	
     	//ОТПРАВЛЯЕМ ЗАПРОС НА ДОБАВЛЕНИЕ НОВОГО ОТВЕТА В ТЕСТЕ
     	$url = 'http://recruit.assessment.com/CareerCenter/TakeMAPP/TakeTheMAPP/'.($q_number+1).'?selectedLanguage=ENGLISH';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 1);
     	curl_setopt($ch, CURLOPT_POSTFIELDS,
    					 "MostDesired=".$most."&LeastDesired=".$least."&LanguageSelected=ENGLISH&AnswerId=".$q_number."&__RequestVerificationToken=".$req_token);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$cookies_file_id);
     	//curl_setopt($ch, CURLOPT_COOKIEJAR, "cookies.txt");

     	$response = curl_exec($ch);
     	$err = curl_error($ch);  //if you need
     	curl_close($ch);
     	if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $response, $matches ) ) {
      		$status = (int)$matches[1];
      		if($status != 200){
      			print("error");
      		}
    	}
    }
	public function mapp_auth ($login, $pass, $cookies_file_id){

		//ПОЛУЧАЕМ СТРАНИЦУ ВХОДА, БЕРЕМ ОТТУДА crsf_field token и куки, чтобы отправить
    	//пост запрос на авторизацию в системе
  		$url = 'http://recruit.assessment.com/CareerCenter/Account/LogIn';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 0);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path()."/mapp_cookies/".$cookies_file_id);

     	$response = curl_exec($ch);
     	$err = curl_error($ch); 
     	curl_close($ch);
     	//print(htmlspecialchars($response));
     	preg_match('<input name="__RequestVerificationToken" type="hidden" value="(.*?)" /> ', $response, $out);
     	$request_token = $out[1];
     	print($err);
     	//ОТПРАВЛЯЕМ ПОСТ ЗАПРОС НА АВТОРИЗАЦИЮ В СИСТЕМЕ
     	$url = 'http://recruit.assessment.com/CareerCenter/Account/Login?ReturnUrl=%2FCareerCenter';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 1);
     	curl_setopt($ch, CURLOPT_POSTFIELDS,
    					 "EmailAddress=".$login."&Password=".$pass."&__RequestVerificationToken=".$request_token);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path()."/mapp_cookies/".$cookies_file_id);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path()."/mapp_cookies/".$cookies_file_id);

     	$response = curl_exec($ch);
     	$err = curl_error($ch); 
     	curl_close($ch);
     	if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $response, $matches ) ) {
      		$status = (int)$matches[1];
      		if($status != 200){
      			print("error");
      		}
    	}
	}

    public function check_auth ($login, $pass, $c_id){

        $this->mapp_auth($login, $pass, $c_id);
        

        $url = 'http://recruit.assessment.com/CareerCenter';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path()."/mapp_cookies/".$c_id);
        curl_setopt($ch, CURLOPT_COOKIEJAR, public_path()."/mapp_cookies/".$c_id);

        $response = curl_exec($ch);
        $err = curl_error($ch); 
        curl_close($ch);

        if(preg_match('/\/content\/img\/MCtr_YourMappAssessment_01\.jpg/', $response)){
            return true;
        }else{
            return false;
        }

    }

    public function getRequestToken ($cookie_file_name, $url){

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 0);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$cookie_file_name);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/mapp_cookies/'.$cookie_file_name);

     	$response = curl_exec($ch);
     	$err = curl_error($ch);  //if you need
     	curl_close($ch);
     	preg_match('<input name="__RequestVerificationToken" type="hidden" value="(.*?)" /> ', $response, $out);
     	$request_token = $out[1];
     	return $request_token;
    }

    public function getResults (){
    	if(file_exists( public_path().'/mapp_cookies/'.Auth::user()->id)){
    	
    		$url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults/GetResultsInPDF';
    		$fp = fopen(public_path()."/mapp_results/result".Auth::user()->id.".pdf", "w+");
     		$ch = curl_init();
     		curl_setopt($ch, CURLOPT_URL, $url);
     		curl_setopt($ch, CURLOPT_POST, 0);
     		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     		curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.Auth::user()->id);
     		curl_setopt($ch, CURLOPT_FILE, $fp); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

     		$response = curl_exec($ch);
     		$err = curl_error($ch); 
	     	curl_close($ch);

			return response()->download(public_path()."/mapp_results/result".Auth::user()->id.".pdf", Auth::user()->name."_".Auth::user()->surname."_MAPP_results.pdf", ['location' => '/user/mapp/results']);
    	}else{
    		$this->mapp_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id);
    	}

    }

    public function showJob ($sys_id, Request $r){

        $result_in_base = MappResPercent::where("user_id", Auth::user()->id)
                                        ->where("type", "d")
                                        ->where("job_sys_id", $sys_id)
                                        ->get();
        $job = MappJob::where("sys_id", $sys_id)->get();

        if($result_in_base->count() > 0){

            $array['job_details'] = $result_in_base;
            $array['title'] = "Детализация профессии";
            $array['job'] = $job;

            if($r->get('lang') != NULL){
                $lang = $r->get('lang');
            }else{
                $lang = "en";
            }
            $array['lang'] = $lang;

            return view("user.mapp.show_job", $array);

        }else{
            $this->getJob($sys_id);
            return redirect()->to('/user/mapp_job/'.$sys_id); 
        }
    }

    public function getJobList (Request $r){
        get_job_auth:
        $result_in_base = MappResPercent::where("user_id", Auth::user()->id)
                                        ->where("type", "j")
                                        ->get();
        if($result_in_base->count() > 0){
            $array["jobs"] = $result_in_base;
            $array['title'] = "Список профессий";
            if($r->get('lang') != NULL){
                $lang = $r->get('lang');
            }else{
                $lang = "en";
            }
            $array['lang'] = $lang;
            return view("user.mapp.joblist", $array);
        }
        else if(file_exists( public_path().'/mapp_cookies/'.Auth::user()->id)){
        
            $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/SortedMAPPMatchToONET';
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.Auth::user()->id);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)<\/a><\/h3>[\t\n\r\s]+<p>(.*?)<\/p>/', $response, $out);
            preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)%<\/a><\/h3>/', $response, $out2);

            $to_ins = array();

            for($i=0; $i<20; $i++){
                //$to_ins[] = array("sys_id" => $out[1][$i], "job_name" => $out[2][$i], "job_desc" => $out[3][$i]);
                $sys_id = $out[1][$i];

                $job_data = MappJob::where("sys_id", $sys_id)->get();

                $job_percent = $out2[2][$i];
                $job_rel_id = $job_data[0]->id;

                $to_ins[] = array(
                        "type" => "j",
                        "rel_id" => $job_rel_id,
                        "job_sys_id" => $sys_id,
                        "user_id" => Auth::user()->id,
                        "percent" => $job_percent
                );
                $this->getJob($sys_id, Auth::user()->id);
            }
            MappResPercent::insert($to_ins);
            return redirect()->to('/user/mapp/joblist'); 
        }else{
            $this->mapp_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id);
            goto get_job_auth;
        }
    }

     public function getJob ($sys_id, $user_id){
        //$sys_id = "29-1063.00";
        get_job_auth:
        if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
        
            $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/MAPPMatchToONET/'.$sys_id;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            preg_match_all('/<td>(.*?)_(.*?) - (.*?)<\/td>/', $response, $out);
            preg_match_all('/<td>(.*?)%<\/td>/', $response, $out2);
            preg_match_all("/<\/span>[\t\n\r\s]+(.*?) - .*?[\t\n\r\s]+<\/a>[\t\n\r\s]+<\/td>[\t\n\r\s]+<td>.*?<b>(\d+)%<\/b><\/td>/", $response, $out3);

            $to_ins = array();

            //IN - INTEREST IN JOB CONTENT 
            $IN = $out3[2][0];
            //TE - TEMPERAMENT FOR THE JOB 
            $TE = $out3[2][1];
            //AP - APTITUDE FOR THE JOB
            $AP = $out3[2][2];
            //PE - PEOPLE 
            $PE = $out3[2][3];
            //TH - THINGS 
            $TH = $out3[2][4];
            //DA - DATA
            $DA = $out3[2][5];
            //RE - REASONING 
            $RE = $out3[2][6];
            //MA - MATHEMATICAL CAPACITY 
            $MA = $out3[2][7];
            //LA - LANGUAGE CAPACITY
            $LA = $out3[2][8];

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("IN"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $IN
                );
            
            for($i=0; $i<10; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("TE"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $TE
                );

            for($i=10; $i<22; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("AP"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $AP
                );

            for($i=22; $i<33; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("PE"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $PE
                );

            for($i=33; $i<41; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("TH"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $TH
                );

            for($i=41; $i<49; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("DA"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $DA
                );

             for($i=49; $i<56; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }   

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("RE"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $RE
                );

            for($i=56; $i<62; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }  

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("MA"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $MA
                );


            for($i=62; $i<68; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat("LA"),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $LA
                );

            for($i=68; $i<72; $i++){
                $subcategory = $out[1][$i]."_".$out[2][$i];
                $percent = $out2[1][$i];

                $to_ins[] = array(
                        "type" => "d",
                        "rel_id" => $this->get_id_by_cat($subcategory),
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $percent
                );
            }

            /*
            print("<pre>");
            print_r($to_ins);
            */

            MappResPercent::insert($to_ins);

            //return $to_ins;

        }else{
            $this->mapp_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id);
            goto get_job_auth;
        }
    }

    function get_id_by_cat ($cat){
        $data = MappJobDetails::where("subcategory", $cat)->get()->toArray();
        return $data[0]['id'];
    }

    function getJobUnivers ($id){

        $job_q = MappJob::where("id", $id)->get();
        $job = $job_q[0]['job_name_rus'];

        $ar['title'] = "Список университетов Казахстана по профессии '".$job."'";
        $univers = $job_q[0]['univers'];
        $un = explode("\n", $univers);
        $ar['all_univers'] = $un;

        return view("user.mapp.job_univers_open", $ar);
    }

}
