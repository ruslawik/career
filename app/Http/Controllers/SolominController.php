<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solomin1;
use App\Solomin2;
use App\Results_Solomin1;
use App\Results_Solomin2;
use Carbon\Carbon;
use DateTimeZone;
use Auth;
use App\User;
use Validator;

class SolominController extends Controller
{
    public function getSolominEdit (){

		$ar['title'] = "Ред. тест «Ориентация» И.Л. Соломин";
		$ar['action'] = action("SolominController@Save");
		$ar['questions'] = Solomin1::all();

		return view('manage.solomin.edit', $ar);
	}

    public function getSolominKazEdit (){

        $ar['title'] = "«Ориентация» И.Л. Соломин KAZ";
        $ar['action'] = action("SolominController@SaveKaz");
        $ar['questions'] = Solomin1::all();

        return view('manage.solomin.kazedit', $ar);
    }

	public function getSolomin2Edit (){

		$ar['title'] = "Ред. тест «Ориентация» И.Л. Соломин - 2 таблица";
		$ar['action'] = action("SolominController@Save2");
		$ar['questions'] = Solomin2::all();

		return view('manage.solomin.edit', $ar);
	}

    public function getSolomin2KazEdit (){

        $ar['title'] = "«Ориентация» И.Л. Соломин - 2 таблица KAZ";
        $ar['action'] = action("SolominController@Save2Kaz");
        $ar['questions'] = Solomin2::all();

        return view('manage.solomin.kazedit', $ar);
    }

	public function Save (Request $r){
        for($i = 1; $i <= 35; $i++){
            Solomin1::where("id", "=", $i)->update(['text'=>$r->input('q'.$i)]);
   		}
        return back();
    }
    public function SaveKaz (Request $r){
        for($i = 1; $i <= 35; $i++){
            Solomin1::where("id", "=", $i)->update(['text_kaz'=>$r->input('q'.$i)]);
        }
        return back();
    }

    public function Save2 (Request $r){
        for($i = 1; $i <= 35; $i++){
            Solomin2::where("id", "=", $i)->update(['text'=>$r->input('q'.$i)]);
   		}
        return back();
    }
    public function Save2Kaz (Request $r){
        for($i = 1; $i <= 35; $i++){
            Solomin2::where("id", "=", $i)->update(['text_kaz'=>$r->input('q'.$i)]);
        }
        return back();
    }

    public function getSolomin ($lang){
        $ar['lang'] = $lang;
    	$ar['title'] = "Пройти тест «Ориентация» И.Л. Соломин";
		$ar['action'] = action("SolominController@Send");
		$ar['questions1'] = Solomin1::all();
		$ar['questions2'] = Solomin2::all();
		$col = array( "1" => "#7BD38E", "2" => "#74B7C4");
        $ar['col'] = $col;

        $solomin_results = Results_Solomin1::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if($solomin_results != NULL){
            $ar['passed'] = '1';
        }else{
            $ar['passed'] = '0';
        }

        if(Auth::user()->solomin == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

    	return view('user.solomin.pass', $ar);
    }

    public function Send (Request $r){

        $to_ins1 = array();
        $to_ins2 = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));

        $messages = [
            'required' => 'Необходимо заполнить все вопросы',
        ];

        for($i=1; $i<=35; $i++){

            $validator = Validator::make($r->all(), [
                'check'.$i => 'required',
                'two_check'.$i => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

             $to_ins1[] = array("quest_id"=>$i, "answer" => $r->input('check'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
             $to_ins2[] = array("quest_id"=>$i, "answer" => $r->input('two_check'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
        }

        Results_Solomin1::insert($to_ins1);
        Results_Solomin2::insert($to_ins2);
        User::where('id', Auth::user()->id)->update(["solomin_finished_date"=>$current]);
        return redirect('user/solomin/finish')->with('status', 'Вы успешно завершили тестирование «Ориентация»! Наши сотрудники свяжутся с Вами в ближайшее время.');

    }

    public function finish (){

        $ar['title'] = "«Ориентация»";

        return view("user.solomin.finish", $ar);
    }

    public function getSolominResultsByDate ($id, $date){
        $ar['title'] = "Детализация результатов теста Соломина";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date1 = Results_Solomin1::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $results_by_date2 = Results_Solomin2::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();

        $ar['results1'] = $results_by_date1;
        $ar['results2'] = $results_by_date2;

        $a = array();
        foreach($results_by_date1 as $result){
            $a[$result->quest_id] = $result->answer;
        }

        $b = array();
        foreach($results_by_date2 as $result){
            $b[$result->quest_id] = $result->answer;
        }

        $ar['elovek_4elovek_1'] = $a[1]+$a[2]+$a[3]+$a[4]+$a[5];
        $ar['elovek_texnika_1'] = $a[6]+$a[7]+$a[8]+$a[9]+$a[10];
        $ar['elovek_znakov_1'] = $a[11]+$a[12]+$a[13]+$a[14]+$a[15];
        $ar['elovek_hud_1'] = $a[16]+$a[17]+$a[18]+$a[19]+$a[20];
        $ar['elovek_priroda_1'] = $a[21]+$a[22]+$a[23]+$a[24]+$a[25];
        $ar['harakter_a_ispolnit_1'] = $a[26]+$a[27]+$a[28]+$a[29]+$a[30];
        $ar['harakter_b_tvor4_1'] = $a[31]+$a[32]+$a[33]+$a[34]+$a[35];

        $ar['elovek_4elovek_2'] = $b[1]+$b[2]+$b[3]+$b[4]+$b[5];
        $ar['elovek_texnika_2'] = $b[6]+$b[7]+$b[8]+$b[9]+$b[10];
        $ar['elovek_znakov_2'] = $b[11]+$b[12]+$b[13]+$b[14]+$b[15];
        $ar['elovek_hud_2'] = $b[16]+$b[17]+$b[18]+$b[19]+$b[20];
        $ar['elovek_priroda_2'] = $b[21]+$b[22]+$b[23]+$b[24]+$b[25];
        $ar['harakter_a_ispolnit_2'] = $b[26]+$b[27]+$b[28]+$b[29]+$b[30];
        $ar['harakter_b_tvor4_2'] = $b[31]+$b[32]+$b[33]+$b[34]+$b[35];


        return view("manage.solomin.resultsbydate", $ar);
    }

      public function getSolominResultsByDateForSubadmin ($id, $date){
        $ar['title'] = "Детализация результатов теста Соломина";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date1 = Results_Solomin1::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $results_by_date2 = Results_Solomin2::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();

        $ar['results1'] = $results_by_date1;
        $ar['results2'] = $results_by_date2;

        $a = array();
        foreach($results_by_date1 as $result){
            $a[$result->quest_id] = $result->answer;
        }

        $b = array();
        foreach($results_by_date2 as $result){
            $b[$result->quest_id] = $result->answer;
        }

        $ar['elovek_4elovek_1'] = $a[1]+$a[2]+$a[3]+$a[4]+$a[5];
        $ar['elovek_texnika_1'] = $a[6]+$a[7]+$a[8]+$a[9]+$a[10];
        $ar['elovek_znakov_1'] = $a[11]+$a[12]+$a[13]+$a[14]+$a[15];
        $ar['elovek_hud_1'] = $a[16]+$a[17]+$a[18]+$a[19]+$a[20];
        $ar['elovek_priroda_1'] = $a[21]+$a[22]+$a[23]+$a[24]+$a[25];
        $ar['harakter_a_ispolnit_1'] = $a[26]+$a[27]+$a[28]+$a[29]+$a[30];
        $ar['harakter_b_tvor4_1'] = $a[31]+$a[32]+$a[33]+$a[34]+$a[35];

        $ar['elovek_4elovek_2'] = $b[1]+$b[2]+$b[3]+$b[4]+$b[5];
        $ar['elovek_texnika_2'] = $b[6]+$b[7]+$b[8]+$b[9]+$b[10];
        $ar['elovek_znakov_2'] = $b[11]+$b[12]+$b[13]+$b[14]+$b[15];
        $ar['elovek_hud_2'] = $b[16]+$b[17]+$b[18]+$b[19]+$b[20];
        $ar['elovek_priroda_2'] = $b[21]+$b[22]+$b[23]+$b[24]+$b[25];
        $ar['harakter_a_ispolnit_2'] = $b[26]+$b[27]+$b[28]+$b[29]+$b[30];
        $ar['harakter_b_tvor4_2'] = $b[31]+$b[32]+$b[33]+$b[34]+$b[35];


        return view("subadmin.solomin.resultsbydate", $ar);
    }

    public function getSolominResultsByDateForUser ($date, $lang){
        $ar['title'] = "Детализация результатов теста Соломина";
        $ar['name'] = User::find(Auth::id())->name;
        $ar['surname'] = User::find(Auth::id())->surname;
        $ar['date'] = $date;
        $ar['lang'] = $lang;

        $results_by_date1 = Results_Solomin1::where("user_id", Auth::id())
                                            ->where("created_at", $date)
                                            ->get();
        $results_by_date2 = Results_Solomin2::where("user_id", Auth::id())
                                            ->where("created_at", $date)
                                            ->get();

        $ar['results1'] = $results_by_date1;
        $ar['results2'] = $results_by_date2;

        $a = array();
        foreach($results_by_date1 as $result){
            $a[$result->quest_id] = $result->answer;
        }

        $b = array();
        foreach($results_by_date2 as $result){
            $b[$result->quest_id] = $result->answer;
        }

        $ar['elovek_4elovek_1'] = $a[1]+$a[2]+$a[3]+$a[4]+$a[5];
        $ar['elovek_texnika_1'] = $a[6]+$a[7]+$a[8]+$a[9]+$a[10];
        $ar['elovek_znakov_1'] = $a[11]+$a[12]+$a[13]+$a[14]+$a[15];
        $ar['elovek_hud_1'] = $a[16]+$a[17]+$a[18]+$a[19]+$a[20];
        $ar['elovek_priroda_1'] = $a[21]+$a[22]+$a[23]+$a[24]+$a[25];
        $ar['harakter_a_ispolnit_1'] = $a[26]+$a[27]+$a[28]+$a[29]+$a[30];
        $ar['harakter_b_tvor4_1'] = $a[31]+$a[32]+$a[33]+$a[34]+$a[35];

        $ar['elovek_4elovek_2'] = $b[1]+$b[2]+$b[3]+$b[4]+$b[5];
        $ar['elovek_texnika_2'] = $b[6]+$b[7]+$b[8]+$b[9]+$b[10];
        $ar['elovek_znakov_2'] = $b[11]+$b[12]+$b[13]+$b[14]+$b[15];
        $ar['elovek_hud_2'] = $b[16]+$b[17]+$b[18]+$b[19]+$b[20];
        $ar['elovek_priroda_2'] = $b[21]+$b[22]+$b[23]+$b[24]+$b[25];
        $ar['harakter_a_ispolnit_2'] = $b[26]+$b[27]+$b[28]+$b[29]+$b[30];
        $ar['harakter_b_tvor4_2'] = $b[31]+$b[32]+$b[33]+$b[34]+$b[35];


        return view("user.solomin.resultsbydate", $ar);
    }


    public function results (){

        $solomin_results = Results_Solomin1::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $ar['solomin_results'] = $solomin_results;
        $ar['title'] = "Результаты";

        return view("user.solomin.all_results", $ar);
    }

}
