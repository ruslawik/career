<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FrontController extends Controller{
    function getIndex (){
        $ar = array();
        $ar['title'] = 'Career Vision';

        return view("front.index", $ar);
    }

}
