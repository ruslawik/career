<?php
namespace App\Http\Controllers\Subadmin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\ImageManager;
use Image;
use DateTimeZone;
use App\Http\Controllers\Manage\Graphs_Disc_DrawController;
use App\Graphs_Disc;
use App\Application;
use App\Questions_Keirsi;
use App\Questions_Keirsi_Kaz;
use App\Answers_Keirsi;
use App\Answers_Keirsi_Kaz;
use App\Questions_Mapp;
use App\Questions_Mapp_Kaz;
use App\User;
use App\Results_Mapp;
use App\Results_Keirsi;
use App\Results_Disc;
use App\Questions_Disc;
use App\Questions_Disc_Kaz;
use App\Results_Holl;
use App\Results_Tomas;
use App\Results_Solomin1;
use App\Results_Solomin2;
use App\Disk_Formula;
use App\Disc_Types;
use Auth;
use App\SubadminSetting;
use App\SubadminTestPrice;
use App\Http\Controllers\MappAuto;

class SubadminController extends Controller{

    public $IS_PARENT = "ER";

    function getIndex (Request $r){
        $ar = array();
        $ar['title'] = 'Центр управления тестами';
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        if($r->has('start_date') && $r->has('end_date')){
            $ar['firstDay'] = $r->input('start_date')." 00:00:00";
            $ar['lastDay'] = $r->input('end_date')." 23:59:59";
        }else{
            $ok = 1;
            $ar['firstDay'] = Carbon::now()->startOfMonth();
            $ar['lastDay'] = Carbon::now()->endOfMonth();
        }

        $ar['action'] = action("Subadmin\SubadminController@getIndex");

        $ar['mapp_tests'] = $this->tests_math("mapp", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['mapp_total_sum'] = $this->tests_math("mapp", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['disc_tests'] = $this->tests_math("disc", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['disc_total_sum'] = $this->tests_math("disc", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['keirsi_tests'] = $this->tests_math("keirsi", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['keirsi_total_sum'] = $this->tests_math("keirsi", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['holl_tests'] = $this->tests_math("holl", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['holl_total_sum'] = $this->tests_math("holl", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['tomas_tests'] = $this->tests_math("tomas", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['tomas_total_sum'] = $this->tests_math("tomas", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['solomin_tests'] = $this->tests_math("solomin", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['solomin_total_sum'] = $this->tests_math("solomin", $ar['firstDay'], $ar['lastDay'], "sum");
        if(isset($ok)){
            $ar['firstDay'] = $ar['firstDay']->toDateString();
            $ar['lastDay'] = $ar['lastDay']->toDateString();
        }else{
            $ar['firstDay'] = Carbon::createFromFormat('Y-m-d H:i:s', $ar['firstDay'])->format('Y-m-d');
            $ar['lastDay'] = Carbon::createFromFormat('Y-m-d H:i:s', $ar['lastDay'])->format('Y-m-d');
        }
            
        $ar['subadmins'] = User::where("user_type", 3)
                            ->where('parent_id', Auth::user()->id)
                            ->get();

        return view("subadmin.index", $ar);
    }

    function tests_math ($test_name, $start_date, $end_date, $action){

        if($action == "sum"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", Auth::user()->id)
                            ->sum($test_name."_price");
            return $data;
        }

        if($action == "count"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", Auth::user()->id)
                            ->count();
            return $data;
        }

        if($action == "get"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", Auth::user()->id)
                            ->get();
            return $data;
        }

    }

    function getStatByTest ($test_name, $from_date, $to_date){

        $ar['title'] = "Статистика по тесту ".$test_name;
        $ar['from_date'] = $from_date;
        $ar['to_date'] = $to_date;
        $ar['test_name'] = $test_name;

        $from_date = $from_date." 00:00:00";
        $to_date = $to_date." 23:59:59";

        $ar['users'] = $this->tests_math($test_name, $from_date, $to_date, "get");
        $ar['test_name_price'] = $test_name."_price";
        $ar['test_name_finished_date'] = $test_name."_finished_date";
        return view("subadmin.stat_by_test", $ar);
    }

    function getUserInfo (Request $r, $id){
        $ar = array();
        $ar['title'] = 'Информация о пользователе';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['user_id'] = $id;

        $this->rec_check_user_parent($id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих клиентов!']);
        }

        if($user_info->user_type==3){
            return redirect("/subadmin/subadmin_info/".$id);
        }

        $mapp_results = Results_Mapp::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($mapp_results!=NULL){
            $mapp_auto = new MappAuto();
            if($mapp_auto->check_auth($user_info['mapp_login'], $user_info['mapp_pass'], $user_info['id'])){
                $ar['auth'] = "1";
            }else{
                $ar['auth'] = '0';
            }
        }
        $keirsi_results = Results_Keirsi::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $disc_results = Results_Disc::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $holl_results = Results_Holl::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $tomas_results = Results_Tomas::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $solomin_results = Results_Solomin1::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $ar['disc_results'] = $disc_results;
        $ar['keirsi_results'] = $keirsi_results;
        $ar['mapp_results'] = $mapp_results;
        $ar['holl_results'] = $holl_results;
        $ar['tomas_results'] = $tomas_results;
        $ar['solomin_results'] = $solomin_results;

        return view("subadmin.users.info", $ar);
    }

    function getKeirsiResultsByDate ($id, $date){
        $ar['title'] = "Детализация результатов КЕЙРСИ";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $this->rec_check_user_parent($id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих клиентов!']);
        }

        $results_by_date = Results_Keirsi::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        $b = array();
        $k = 1;
        foreach($results_by_date as $key=>$value){
            if($value->answer == "a") $a[$k] = 1; else $a[$k] = 0;
            if($value->answer == "b") $b[$k] = 1; else $b[$k] = 0;
            $k++;
        }

        $sum_E = 0; $sum_I = 0;
        for($i=1; $i<=70; $i+=7){ $sum_E += $a[$i]; $sum_I += $b[$i]; }

        $sum_S = 0; $sum_N = 0;
        for($i=2; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }
        for($i=3; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }

        $sum_T = 0; $sum_F = 0;
        for($i=4; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }
        for($i=5; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }

        $sum_J = 0; $sum_P = 0;
        for($i=6; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }
        for($i=7; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }

        $ar['E'] = $sum_E; $ar['I'] = $sum_I; $ar['S'] = $sum_S; $ar['N'] = $sum_N;
        $ar['T'] = $sum_T; $ar['F'] = $sum_F; $ar['J'] = $sum_J; $ar['P'] = $sum_P;

        if($sum_E < $sum_I) $first = "I"; else $first = "E";
        if($sum_S < $sum_N) $second = "N"; else $second = "S";
        if($sum_T < $sum_F) $third = "F"; else $third = "T";
        if($sum_J < $sum_P) $fourth = "P"; else $fourth = "J";

        $ar["formula"] = $first.$second.$third.$fourth;

        return view("subadmin.keirsi.resultsbydate", $ar);
    }
    //END OF KEIRSI Controller Methods
    //The MAPP Test Controller Methods

    function getMappResults ($id, $date, Request $r){
        $ar = array();
        $ar['title'] = 'Страница результатов';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['date'] = $date;
        $ar['user_id'] = $id;

        $this->rec_check_user_parent($id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих клиентов!']);
        }

        $ar['group_number'] = Questions_Mapp::all()
                                            ->groupBy('group_id')
                                            ->count();
        $ar['groups'] = Questions_Mapp::all()->groupBy('group_id');

        return view("subadmin.mapp.results", $ar);
    }

    public function download_mapp_results ($user_id){

        $this->rec_check_user_parent($user_id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих клиентов!']);
        }

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        download:
        if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
        
            $url = 'https://recruit.assessment.com/CareerCenter/YourMAPPResults/GetResultsInPDF';
            $fp = fopen(public_path()."/mapp_results/result".$user_id.".pdf", "w+");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
            curl_setopt($ch, CURLOPT_FILE, $fp); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $response = curl_exec($ch);
            $err = curl_error($ch); 
            curl_close($ch);

            return response()->download(public_path()."/mapp_results/result".$user_id.".pdf", $name."_".$surname."_MAPP_results.pdf", ['location' => '/manage/user/'.$user_id]);
        }else{
            $mapp = new MappAuto();
            $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
            goto download;
        }

    }

    //END OF MAPP Controller Methods

    //START OF DISC Controller Methods

    function getDiscResults ($id, $date, Request $r, $lang){
        $ar = array();
        $ar['title'] = 'Страница результатов';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['date'] = $date;
        $ar['user_id'] = $id;

        $this->rec_check_user_parent($id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих клиентов!']);
        }

        $results_by_date = Results_Disc::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $most = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $least = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $change = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        foreach ($results_by_date as $key => $value) {
            switch($value->get_most->most){
                case "D":$most["D"]++;break;
                case "I":$most["I"]++;break;
                case "S":$most["S"]++;break;
                case "C":$most["C"]++;break;
                case "*":$most["*"]++;break;
            }
            switch($value->get_least->least) {
                case "D":$least["D"]++;break;
                case "I":$least["I"]++;break;
                case "S":$least["S"]++;break;
                case "C":$least["C"]++;break;
                case "*":$least["*"]++;break;
            }
        }
        $most['total'] = $most['D']+$most['I']+$most['S']+$most['C']+$most['*'];
        $least['total'] = $least['D']+$least['I']+$least['S']+$least['C']+$least['*'];
        $change['D'] = $most['D']-$least['D'];
        $change['I'] = $most['I']-$least['I'];
        $change['S'] = $most['S']-$least['S'];
        $change['C'] = $most['C']-$least['C'];
        $ar['most'] = $most;
        $ar['least'] = $least;


        $D_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "D")->get()->toArray();
        $I_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "I")->get()->toArray();
        $S_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "S")->get()->toArray();
        $C_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "C")->get()->toArray();

        $D_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "D")->get()->toArray();
        $I_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "I")->get()->toArray();
        $S_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "S")->get()->toArray();
        $C_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "C")->get()->toArray();

        $D_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "D")->get()->toArray();
        $I_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "I")->get()->toArray();
        $S_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "S")->get()->toArray();
        $C_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "C")->get()->toArray();

        $img_most_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_most, $I_numbers_most, $S_numbers_most, $C_numbers_most, $most["D"], $most["I"], $most["S"], $most["C"]);
        $img_least_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_least, $I_numbers_least, $S_numbers_least, $C_numbers_least, $least["D"], $least["I"], $least["S"], $least["C"]);
        $img_change_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_change, $I_numbers_change, $S_numbers_change, $C_numbers_change, $change["D"], $change["I"], $change["S"], $change["C"]);

        $ar['img_most_graph'] = $img_most_graph['image']->encode('data-url');
        $ar['img_least_graph'] = $img_least_graph['image']->encode('data-url');
        $ar['img_change_graph'] = $img_change_graph['image']->encode('data-url');

        $ar['top_most_letters'] = $img_most_graph['top_letters'];
        $ar['top_least_letters'] = $img_least_graph['top_letters'];
        $ar['top_change_letters'] = $img_change_graph['top_letters'];

        //Зеркало формула и описание
        $formula = "";
        $pred = 1000;
        foreach($img_change_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula .= "=";
            }
            if($value > $pred){
                $formula .= ".";
            }
            $formula .= $letter;
            $pred = $value;
        }
        $ar['change_formula'] = $formula;
        $type_id_q = Disk_Formula::where("formula", $formula)->first();
        $type_id = $type_id_q['type_id'];
        $ar['description'] = Disc_Types::where('id',$type_id)->get();

        $top_prof = $formula[0];
        if(isset($formula[2])){
            $top_prof .= $formula[2];
        }
        $ar['top_prof'] = $top_prof;

        //Маска - формула и описание

        $formula_mask = "";
        $pred = 1000;
        foreach($img_most_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_mask .= "=";
            }
            if($value > $pred){
                $formula_mask .= ".";
            }
            $formula_mask .= $letter;
            $pred = $value;
        }
        $ar['most_formula'] = $formula_mask;
        $type_id_q_most = Disk_Formula::where("formula", $formula_mask)->first();
        $type_id_mask = $type_id_q_most['type_id'];
        $ar['description_mask'] = Disc_Types::where('id',$type_id_mask)->get();

        //Сердце

        $formula_heart = "";
        $pred = 1000;
        foreach($img_least_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_heart .= "=";
            }
            if($value > $pred){
                $formula_heart .= ".";
            }
            $formula_heart .= $letter;
            $pred = $value;
        }
        $ar['least_formula'] = $formula_heart;
        $type_id_q_least = Disk_Formula::where("formula", $formula_heart)->first();
        $type_id_least = $type_id_q_least['type_id'];
        $ar['description_least'] = Disc_Types::where('id',$type_id_least)->get();

        $ar['lang'] = $lang;
        return view("subadmin.disc.results", $ar);
    }
    //END of DISC

    //Users Controller Methods
    function getAddUser (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('Subadmin\SubadminController@savePostNewUser');

        return view("subadmin.users.add_user", $ar);

    }

    function getAllUsers (){
        $ar = array();
        $ar['title'] = 'Все пользователи';

        return view("subadmin.users.all_users", $ar);
    }

    function savePostNewUser (Request $request){
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->login = $request->input('login');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->mapp_login = $request->input('mapp_login');
        $new_user->mapp_pass = $request->input('mapp_pass');
        $new_user->parent_id = Auth::user()->id;

        $text_to_mess = "";

        if($request->has('mapp_test')){
            $mapp_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'mapp_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($mapp_test_amount['value']-($mapp_test_amount['used_value']+1) >= 0){
                $new_user->mapp = 1;
                SubadminSetting::where('id', $mapp_test_amount['id'])
                                ->update(["used_value" => $mapp_test_amount['used_value']+1]);
                $text_to_mess .= "Тест MAPP успешно назначен!<br>";
            }else{
                $new_user->mapp = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения MAPP</b></font><br>";
            }
        }
        $new_user->mapp_price = $request->input('mapp_test_price');
        if($request->has('disc_test')){
            $disc_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'disc_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($disc_test_amount['value']-($disc_test_amount['used_value']+1) >= 0){
                $new_user->disc = 1;
                SubadminSetting::where('id', $disc_test_amount['id'])
                                ->update(["used_value" => $disc_test_amount['used_value']+1]);
                $text_to_mess .= "Тест DISC успешно назначен!<br>";
            }else{
                $new_user->disc = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения DISC</b></font><br>";
            }
        }
        $new_user->disc_price = $request->input('disc_test_price');
        if($request->has('keirsi_test')){
            $keirsi_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'keirsi_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($keirsi_test_amount['value']-($keirsi_test_amount['used_value']+1) >= 0){
                $new_user->keirsi = 1;
                SubadminSetting::where('id', $keirsi_test_amount['id'])
                                ->update(["used_value" => $keirsi_test_amount['used_value']+1]);
                $text_to_mess .= "Тест КЕЙРСИ успешно назначен!<br>";
            }else{
                $new_user->keirsi = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения КЕЙРСИ</b></font><br>";
            }
        }
        $new_user->keirsi_price = $request->input('keirsi_test_price');
        if($request->has('holl_test')){
             $holl_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'holl_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($holl_test_amount['value']-($holl_test_amount['used_value']+1) >= 0){
                $new_user->holl = 1;
                SubadminSetting::where('id', $holl_test_amount['id'])
                                ->update(["used_value" => $holl_test_amount['used_value']+1]);
                $text_to_mess .= "Тест ХОЛЛА успешно назначен!<br>";
            }else{
                $new_user->holl = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста ХОЛЛА</b></font><br>";
            }
        }
        $new_user->holl_price = $request->input('holl_test_price');
        if($request->has('tomas_test')){
             $tomas_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'tomas_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($tomas_test_amount['value']-($tomas_test_amount['used_value']+1) >= 0){
                $new_user->tomas = 1;
                SubadminSetting::where('id', $tomas_test_amount['id'])
                                ->update(["used_value" => $tomas_test_amount['used_value']+1]);
                $text_to_mess .= "Тест ТОМАСА успешно назначен!<br>";
            }else{
                $new_user->tomas = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста ТОМАСА</b></font><br>";
            }
        }
        $new_user->tomas_price = $request->input('tomas_test_price');
        if($request->has('solomin_test')){
             $solomin_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'solomin_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($solomin_test_amount['value']-($solomin_test_amount['used_value']+1) >= 0){
                $new_user->solomin = 1;
                SubadminSetting::where('id', $solomin_test_amount['id'])
                                ->update(["used_value" => $solomin_test_amount['used_value']+1]);
                $text_to_mess .= "Тест СОЛОМИНА успешно назначен!<br>";
            }else{
                $new_user->solomin = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста СОЛОМИНА</b></font><br>";
            }
        }
        $new_user->solomin_price = $request->input('solomin_test_price');

        $new_user->save();
            
        return back()->with('status', 'Успешно добавлен новый пользователь!<br>'.$text_to_mess);
    }

    public function ajax_all_users ($type){

        $users = User::select('login', 'name', 'surname', 'user_type', 'id')->where('is_active', $type)->where("parent_id", Auth::user()->id)->get()->toArray();
        $output = array(
            'data' => $users
        );
        return response()->json($output);
    }

    public function getEditUser ($user_id){

        $ar['title'] = "Редактировать настройки пользователя";
        $user = User::where('id', $user_id)->get();
        $ar['user'] = $user;
        $ar['action'] = action("Subadmin\SubadminController@saveEditedUser");

        $id = $user_id;

        $this->rec_check_user_parent($user_id, Auth::user()->id);

        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете редактировать чужих клиентов!']);
        }

        if($user['0']->user_type==3){
            return redirect("/subadmin/edit_subadmin/".$user_id);
        }

        $mapp_results = Results_Mapp::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $keirsi_results = Results_Keirsi::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $disc_results = Results_Disc::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $holl_results = Results_Holl::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $tomas_results = Results_Tomas::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $solomin_results = Results_Solomin1::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $mapp_results!=NULL ? $ar['mapp_passed'] = '1' : $ar['mapp_passed'] = '0';
        $keirsi_results!=NULL ? $ar['keirsi_passed'] = '1' : $ar['keirsi_passed'] = '0';
        $disc_results!=NULL ? $ar['disc_passed'] = '1' : $ar['disc_passed'] = '0';
        $holl_results!=NULL ? $ar['holl_passed'] = '1' : $ar['holl_passed'] = '0';
        $tomas_results!=NULL ? $ar['tomas_passed'] = '1' : $ar['tomas_passed'] = '0';
        $solomin_results!=NULL ? $ar['solomin_passed'] = '1' : $ar['solomin_passed'] = '0';

        return view("subadmin.users.edit", $ar);
    }

    public function saveEditedUser (Request $request){

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $surname = $request->input('surname');
        $user_type = $request->input('role');
        $password = Hash::make($request->input('password'));
        $mapp_login = $request->input('mapp_login');
        $mapp_pass = $request->input('mapp_pass');
        $is_active = $request->input('is_active');

        if(strlen($request->input('password'))>1){
            User::where('id', $user_id)->update(["password"=>$password]);
        }

        $user_info = User::where('id', $user_id)->get();

        $mapp = 0;
        $disc = 0;
        $keirsi = 0;
        $holl = 0;
        $tomas = 0;
        $solomin = 0;

        $text_to_mess = "";

        if($request->has('mapp_test')){
            $mapp_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'mapp_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($mapp_test_amount['value']-($mapp_test_amount['used_value']+1) >= 0 || $user_info['0']->mapp==1){
                $mapp = 1;
                if($user_info['0']->mapp!=1){
                    SubadminSetting::where('id', $mapp_test_amount['id'])
                                ->update(["used_value" => $mapp_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест MAPP успешно назначен!<br>";
            }else{
                $mapp = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения MAPP</b></font><br>";
            }
        }
        $mapp_price = $request->input('mapp_test_price');
        if($request->has('disc_test')){
            $disc_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'disc_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($disc_test_amount['value']-($disc_test_amount['used_value']+1) >= 0 || $user_info['0']->disc==1){
                $disc = 1;
                if($user_info['0']->disc!=1){
                    SubadminSetting::where('id', $disc_test_amount['id'])
                                ->update(["used_value" => $disc_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест DISC успешно назначен!<br>";
            }else{
                $disc = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения DISC</b></font><br>";
            }
        }
        $disc_price = $request->input('disc_test_price');
        if($request->has('keirsi_test')){
            $keirsi_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'keirsi_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($keirsi_test_amount['value']-($keirsi_test_amount['used_value']+1) >= 0 || $user_info['0']->keirsi==1){
                $keirsi = 1;
                if($user_info['0']->keirsi!=1){
                    SubadminSetting::where('id', $keirsi_test_amount['id'])
                                ->update(["used_value" => $keirsi_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест КЕЙРСИ успешно назначен!<br>";
            }else{
                $keirsi = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения КЕЙРСИ</b></font><br>";
            }
        }
        $keirsi_price = $request->input('keirsi_test_price');
        if($request->has('holl_test')){
             $holl_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'holl_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($holl_test_amount['value']-($holl_test_amount['used_value']+1) >= 0 || $user_info['0']->holl==1){
                $holl = 1;
                if($user_info['0']->holl!=1){
                    SubadminSetting::where('id', $holl_test_amount['id'])
                                ->update(["used_value" => $holl_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест ХОЛЛА успешно назначен!<br>";
            }else{
                $holl = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста ХОЛЛА</b></font><br>";
            }
        }
        $holl_price = $request->input('holl_test_price');
        if($request->has('tomas_test')){
             $tomas_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'tomas_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($tomas_test_amount['value']-($tomas_test_amount['used_value']+1) >= 0 || $user_info['0']->tomas==1){
                $tomas = 1;
                if($user_info['0']->tomas!=1){
                    SubadminSetting::where('id', $tomas_test_amount['id'])
                                ->update(["used_value" => $tomas_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест ТОМАСА успешно назначен!<br>";
            }else{
                $tomas = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста ТОМАСА</b></font><br>";
            }
        }
        $tomas_price = $request->input('tomas_test_price');
        if($request->has('solomin_test')){
             $solomin_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'solomin_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($solomin_test_amount['value']-($solomin_test_amount['used_value']+1) >= 0 || $user_info['0']->solomin==1){
                $solomin = 1;
                if($user_info['0']->solomin!=1){
                    SubadminSetting::where('id', $solomin_test_amount['id'])
                                ->update(["used_value" => $solomin_test_amount['used_value']+1]);
                }
                $text_to_mess .= "Тест СОЛОМИНА успешно назначен!<br>";
            }else{
                $solomin = 0;
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для назначения теста СОЛОМИНА</b></font><br>";
            }
        }
        $solomin_price = $request->input('solomin_test_price');

        User::where('id', $user_id)
                ->update([
                    "name" => $name,
                    "surname" => $surname,
                    "user_type" => $user_type,
                    "mapp_login" => $mapp_login,
                    "mapp_pass" => $mapp_pass,
                    "mapp" => $mapp,
                    "disc" => $disc,
                    "keirsi" => $keirsi,
                    "holl" => $holl,
                    "tomas" => $tomas,
                    "solomin" => $solomin,
                    "mapp_price" => $mapp_price,
                    "disc_price" => $disc_price,
                    "keirsi_price" => $keirsi_price,
                    "holl_price" => $holl_price,
                    "tomas_price" => $tomas_price,
                    "solomin_price" => $solomin_price,
                    "is_active" => $is_active
                ]);

        return back()->with('status', "Успешно обновлено!<br>".$text_to_mess);
    }

    function getAddSubadmin (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('Subadmin\SubadminController@savePostNewSubadmin');
        return view("subadmin.users.add_subadmin", $ar);

    }

    function savePostNewSubadmin (Request $request){
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->login = $request->input('login');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->parent_id = Auth::user()->id;
        $new_user->save();

        $settings_array = array();
        $text_to_mess = "";

        if($request->input("mapp_test") > 0){
            $mapp_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'mapp_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($mapp_test_amount['value']-($mapp_test_amount['used_value']+$request->input("mapp_test")) >= 0){
                $settings_array['mapp_test'] = $request->input('mapp_test');
                SubadminSetting::where('id', $mapp_test_amount['id'])
                                ->update(["used_value" => $mapp_test_amount['used_value']+$request->input("mapp_test")]);
                $text_to_mess .= "Тест MAPP успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи MAPP</b></font><br>";
            }
        }
        if($request->input("disc_test") > 0){
            $disc_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'disc_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($disc_test_amount['value']-($disc_test_amount['used_value']+$request->input("disc_test")) >= 0){
                $settings_array['disc_test'] = $request->input('disc_test');
                SubadminSetting::where('id', $disc_test_amount['id'])
                                ->update(["used_value" => $disc_test_amount['used_value']+$request->input("disc_test")]);
                $text_to_mess .= "Тест DISC успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи DISC</b></font><br>";
            }
        }
        if($request->input("keirsi_test") > 0){
            $keirsi_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'keirsi_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($keirsi_test_amount['value']-($keirsi_test_amount['used_value']+$request->input("keirsi_test")) >= 0){
                $settings_array['keirsi_test'] = $request->input('keirsi_test');
                SubadminSetting::where('id', $keirsi_test_amount['id'])
                                ->update(["used_value" => $keirsi_test_amount['used_value']+$request->input("keirsi_test")]);
                $text_to_mess .= "Тест КЕЙРСИ успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи КЕЙРСИ</b></font><br>";
            }
        }
        if($request->input("holl_test") > 0){
             $holl_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'holl_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($holl_test_amount['value']-($holl_test_amount['used_value']+$request->input("holl_test")) >= 0){
                $settings_array['holl_test'] = $request->input('holl_test');
                SubadminSetting::where('id', $holl_test_amount['id'])
                                ->update(["used_value" => $holl_test_amount['used_value']+$request->input("holl_test")]);
                $text_to_mess .= "Тест ХОЛЛА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста ХОЛЛА</b></font><br>";
            }
        }
        if($request->input("tomas_test") > 0){
             $tomas_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'tomas_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($tomas_test_amount['value']-($tomas_test_amount['used_value']+$request->input("tomas_test")) >= 0){
                $settings_array['tomas_test'] = $request->input('tomas_test');
                SubadminSetting::where('id', $tomas_test_amount['id'])
                                ->update(["used_value" => $tomas_test_amount['used_value']+$request->input("tomas_test")]);
                $text_to_mess .= "Тест ТОМАСА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста ТОМАСА</b></font><br>";
            }
        }
        if($request->input("solomin_test") > 0){
             $solomin_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'solomin_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($solomin_test_amount['value']-($solomin_test_amount['used_value']+$request->input("solomin_test")) >= 0){
                $settings_array['solomin_test'] = $request->input('solomin_test');
                SubadminSetting::where('id', $solomin_test_amount['id'])
                                ->update(["used_value" => $solomin_test_amount['used_value']+$request->input("solomin_test")]);
                $text_to_mess .= "Тест СОЛОМИНА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста СОЛОМИНА</b></font><br>";
            }
        }
    
        foreach ($settings_array as $key => $value) {
            $new_user_settings = new SubadminSetting;
            $new_user_settings->who_set_id = Auth::user()->id;
            $new_user_settings->user_id = $new_user->id;
            $new_user_settings->setting = $key;
            $new_user_settings->value = $value;
            $new_user_settings->save();

            $new_subadmin_test_price = new SubadminTestPrice;
            $new_subadmin_test_price->setting_id = $new_user_settings->id;
            $new_subadmin_test_price->price = $request->input($key.'_price');
            $new_subadmin_test_price->save();
        }

        return back()->with('status', 'Успешно добавлен новый subadmin с указанным количеством тестов!<br>'.$text_to_mess);
    }
    
    public function getEditSubadmin ($user_id){

        $this->rec_check_user_parent($user_id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужих subadmin аккаунтов!']);
        }

        $ar['title'] = "Редактировать настройки пользователя";
        $user = User::where('id', $user_id)->get();
        if($user['0']->user_type==2){
            return redirect("/subadmin/edit_user/".$user_id);
        }
        $ar['user'] = $user;
        $ar['action'] = action("Subadmin\SubadminController@saveEditedSubadmin");

        $id = $user_id;

        $ar['all_given_tests'] = SubadminSetting::where("user_id", $id)->orderBy('created_at')->get();

        return view("subadmin.users.edit_subadmin", $ar);
    }

    public function saveEditedSubadmin (Request $request){

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $surname = $request->input('surname');
        $password = Hash::make($request->input('password'));
        $is_active = $request->input('is_active');

        if(strlen($request->input('password'))>1){
            User::where('id', $user_id)->update(["password"=>$password]);
        }
        User::where('id', $user_id)
                ->update([
                    "name" => $name,
                    "surname" => $surname,
                    "is_active" => $is_active]);

        $settings_array = array();

        $text_to_mess = "";

        if($request->input("mapp_test") > 0){
            $mapp_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'mapp_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($mapp_test_amount['value']-($mapp_test_amount['used_value']+$request->input("mapp_test")) >= 0){
                $settings_array['mapp_test'] = $request->input('mapp_test');
                SubadminSetting::where('id', $mapp_test_amount['id'])
                                ->update(["used_value" => $mapp_test_amount['used_value']+$request->input("mapp_test")]);
                $text_to_mess .= "Тест MAPP успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи MAPP</b></font><br>";
            }
        }
        if($request->input("disc_test") > 0){
            $disc_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'disc_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($disc_test_amount['value']-($disc_test_amount['used_value']+$request->input("disc_test")) >= 0){
                $settings_array['disc_test'] = $request->input('disc_test');
                SubadminSetting::where('id', $disc_test_amount['id'])
                                ->update(["used_value" => $disc_test_amount['used_value']+$request->input("disc_test")]);
                $text_to_mess .= "Тест DISC успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи DISC</b></font><br>";
            }
        }
        if($request->input("keirsi_test") > 0){
            $keirsi_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'keirsi_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($keirsi_test_amount['value']-($keirsi_test_amount['used_value']+$request->input("keirsi_test")) >= 0){
                $settings_array['keirsi_test'] = $request->input('keirsi_test');
                SubadminSetting::where('id', $keirsi_test_amount['id'])
                                ->update(["used_value" => $keirsi_test_amount['used_value']+$request->input("keirsi_test")]);
                $text_to_mess .= "Тест КЕЙРСИ успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи КЕЙРСИ</b></font><br>";
            }
        }
        if($request->input("holl_test") > 0){
             $holl_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'holl_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($holl_test_amount['value']-($holl_test_amount['used_value']+$request->input("holl_test")) >= 0){
                $settings_array['holl_test'] = $request->input('holl_test');
                SubadminSetting::where('id', $holl_test_amount['id'])
                                ->update(["used_value" => $holl_test_amount['used_value']+$request->input("holl_test")]);
                $text_to_mess .= "Тест ХОЛЛА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста ХОЛЛА</b></font><br>";
            }
        }
        if($request->input("tomas_test") > 0){
             $tomas_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'tomas_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($tomas_test_amount['value']-($tomas_test_amount['used_value']+$request->input("tomas_test")) >= 0){
                $settings_array['tomas_test'] = $request->input('tomas_test');
                SubadminSetting::where('id', $tomas_test_amount['id'])
                                ->update(["used_value" => $tomas_test_amount['used_value']+$request->input("tomas_test")]);
                $text_to_mess .= "Тест ТОМАСА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста ТОМАСА</b></font><br>";
            }
        }
        if($request->input("solomin_test") > 0){
             $solomin_test_amount = SubadminSetting::where('user_id', Auth::user()->id)
                                                ->where('setting', 'solomin_test')
                                                ->whereRaw('value-used_value > 0')
                                                ->first();

            if($solomin_test_amount['value']-($solomin_test_amount['used_value']+$request->input("solomin_test")) >= 0){
                $settings_array['solomin_test'] = $request->input('solomin_test');
                SubadminSetting::where('id', $solomin_test_amount['id'])
                                ->update(["used_value" => $solomin_test_amount['used_value']+$request->input("solomin_test")]);
                $text_to_mess .= "Тест СОЛОМИНА успешно выдан!<br>";
            }else{
                $text_to_mess .= "<font color='red'><b>У Вас не хватает тестов для выдачи теста СОЛОМИНА</b></font><br>";
            }
        }
    
    
        foreach ($settings_array as $key => $value) {
            $new_user_settings = new SubadminSetting;
            $new_user_settings->who_set_id = Auth::user()->id;
            $new_user_settings->user_id = $user_id;
            $new_user_settings->setting = $key;
            $new_user_settings->value = $value;
            $new_user_settings->save();

            $new_subadmin_test_price = new SubadminTestPrice;
            $new_subadmin_test_price->setting_id = $new_user_settings->id;
            $new_subadmin_test_price->price = $request->input($key.'_price');
            $new_subadmin_test_price->save();
        }


        return back()->with('status', "Успешно обновлено!<br>".$text_to_mess);
    }

    public function getSubadminInfo ($subadmin_id){

        $this->rec_check_user_parent($subadmin_id, Auth::user()->id);
        
        if($this->IS_PARENT == "F"){
            return back()->withErrors(['Ошибка', 'Вы не можете просматривать чужие аккаунты!']);
        }

        $ar = array();
        $ar['title'] = "Информация о subadmin";
        $ar['parent_id'] = $subadmin_id;
        $ar['subadmins'] = User::where("user_type", 3)
                            ->where('parent_id', $subadmin_id)
                            ->get();
        $ar['now_user'] = User::where('id', $subadmin_id)->get();

         if($ar['now_user']['0']->user_type==2){
            return redirect("/subadmin/user/".$subadmin_id);
        }

        return view("subadmin.users.subadmin_info", $ar);
    }

    public function ajax_all_subadmin_users ($type, $parent_id){

        $users = User::select('login', 'name', 'surname', 'user_type', 'id')->where("parent_id", $parent_id)->where('user_type', 2)->where('is_active', $type)->get()->toArray();
        $output = array(
            'data' => $users
        );
        return response()->json($output);
    }

    public function rec_check_user_parent($user_id, $is_parent_id){

        $get_info = User::where('id', $user_id)
                        ->get();
        if($get_info->count()>0){
            if($get_info['0']->parent_id == $is_parent_id){
                $this->IS_PARENT = "T";
            }else{
                if($get_info['0']->parent_id == NULL){
                    $this->IS_PARENT = "F";
                }else{
                    $this->rec_check_user_parent($get_info['0']->parent_id, $is_parent_id);
                }
            }
        }else{
            $this->IS_PARENT = "F";
        }
    }

    public function testAmount() {

        $user_id = Auth::user()->id;
        $user_info = User::where('id', $user_id)
                            ->get()
                            ->toArray();

        $ar['user'] = $user_info;
        $ar['all_given_tests'] = SubadminSetting::where("user_id", Auth::user()->id)->orderBy('created_at')->get();
        $ar['title'] = "Количество выданных тестов для ".$user_info['0']['name']." ".$user_info['0']['surname'];

        return view("subadmin.users.test_amount", $ar);
    }

    public function getEditProfile(){

        $ar['title'] = "Редактировать профиль";
        $ar['user'] = User::where('id', Auth::user()->id)
                            ->get();

        $ar['action'] = action("Subadmin\SubadminController@saveProfile");

        return view("subadmin.users.edit_profile", $ar);
    }

    public function saveProfile (Request $r){

        $name = $r->input('name');
        $surname = $r->input('surname');

        $pass = $r->input('password');

        if(strlen($pass)>1 && strlen($pass)<4){
            return back()->withErrors(['Слишком короткий пароль. Не менее 4 символов']);
        }

        if(strlen($pass)>4){
            User::where("id", Auth::user()->id)->update(["password"=>Hash::make($pass)]);
        }

        User::where("id", Auth::user()->id)->update(["name"=>$name, "surname"=>$surname]);

        return back()->with('status', 'Данные успешно обновлены!');
    }

}
