<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use Hash;
use Auth;

class LoginController extends Controller{

    function getCloud (){
        $ar['title'] = "Платежи. Оплата банковской картой онлайн";
        return view("cloudpayments", $ar);
    }

    function getRegpage (){
    
        $ar = array();
        $ar['action'] = action('LoginController@postReg');

        return view('manage.regpage', $ar);
    }

    function check_login(Request $r){
        $is_exists = User::where("login", $r->input('login'))->count();
        if($is_exists >= 1){
            print(1);
        }else{
            print(0);
        }
    }

    function utf8_mail($to, $subject = '(No subject)', $message = '', $from = 'no-reply@careervision.kz') {
    $header = 'MIME-Version: 1.0' . "\n" . 'Content-type: text/html; charset=UTF-8'
        . "\n" . 'From: CAREERVISION.KZ <' . $from . ">\n";
    mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $message, $header);
    }


    function postReg (Request $r){
        $user = new User();
        $user->login = $r->input("login");
        $user->name = $r->input("name");
        $user->surname = $r->input("surname");
        $user->password = Hash::make($r->input("password"));
        $user->email = $r->input("email");
        $user->user_type = 2;
        $user->parent_id = 1;
        if($r->has('DISC')){
            $user->disc = 1;
            $user->disc_price = 0;
        }else{
            $user->disc = 0;
        }

        $user->save();
        $to = $r->input("email");
        $this->utf8_mail($to, 'Ваши данные при регистрации на CAREERVISION.KZ', "Вход по адресу: https://test.careervision.kz <br> Логин: ".$r->input('login')."<br>Пароль: ".$r->input('password'));
        Auth::attempt(['login' => $r->input('login'), 'password' => $r->input('password')]);

        return redirect('/user/main');
    }


    function getLogin (){
        
        /*
        $user = new User();
        $user->login = 'main';
        $user->password = Hash::make('goal100m');
        $user->name = "Career";
        $user->surname = "Vision";
        $user->user_type = 1;
        $user->save();
        */
        $ar = array();
        $ar['action'] = action('LoginController@postLogin');

        return view('manage.login', $ar);
    }

    function postLogin(Request $request){
        if (!Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')]))
            return back()->with('error', 'Неверный логин/пароль');
        if (Auth::user()->is_active != 1){
             return back()->with('error', 'Ваш аккаунт деактивирован администратором');
        }
        if (Auth::user()->user_type == 1)
            return redirect()->action('Manage\ManageController@getIndex');
        else if (Auth::user()->user_type == 2)
            return redirect()->action('User\UserController@getIndex');
        else if (Auth::user()->user_type == 3)
            return redirect()->action('Subadmin\SubadminController@getIndex');
        else abort(404);
    }

    function getLogout(){
        Auth::logout();

        return redirect()->to('/login');
    }

    function getApplication (){

        $ar['title'] = "Онлайн подача заявки";

        return view("manage.application", $ar);
    }

    function saveApplication (Request $r){

        $app = new Application();

        $app->name = $r->input('name');
        $app->surname = $r->input('surname');
        $app->sex = $r->input('sex');
        $app->born = $r->input('born');
        $app->address = $r->input('address');
        $app->school = $r->input('school');
        $app->sinif = $r->input('sinif');
        $app->outofdate = $r->input('outofdate');
        $app->tel = $r->input('tel');

        $app->prof1 = $r->input('prof1');
        $app->prof2 = $r->input('prof2');
        $app->prof3 = $r->input('prof3');

        $app->your_business = $r->input('your_business');

        $app->math = $r->input('math');
        $app->bio = $r->input('bio');
        $app->chem = $r->input('chem');
        $app->phys = $r->input('phys');
        $app->it_interes = $r->input('it_interes');

        $app->dance = $r->input('dance');
        $app->sing = $r->input('sing');
        $app->music_inses = $r->input('music_inses');
        $app->public_speak = $r->input('public_speak');
        $app->write_creat = $r->input('write_creat');
        $app->other_lang = $r->input('other_lang');

        $app->kazakh_language_aud = $r->input('kazakh_language_aud'); $app->russian_language_aud = $r->input('russian_language_aud');
        $app->kazakh_language_write = $r->input('kazakh_language_write'); $app->russian_language_write = $r->input('russian_language_write');
        $app->kazakh_language_read = $r->input('kazakh_language_read'); $app->russian_language_read = $r->input('russian_language_read');
        $app->kazakh_language_speak = $r->input('kazakh_language_speak'); $app->russian_language_speak = $r->input('russian_language_speak');

        $app->english_language_aud = $r->input('english_language_aud'); $app->other_language_aud = $r->input('other_language_aud');
        $app->english_language_write = $r->input('english_language_write'); $app->other_language_write = $r->input('other_language_write');
        $app->english_language_read = $r->input('english_language_read'); $app->other_language_read = $r->input('other_language_read');
        $app->english_language_speak = $r->input('english_language_speak'); $app->other_language_speak = $r->input('other_language_speak');

        if($r->has('expect1')){
            $app->expect1 = implode(',', $r->input('expect1'));
        }
        if($r->has('expect2')){
            $app->expect2 = implode(',', $r->input('expect2'));
        }
        if($r->has('expect3')){
            $app->expect3 = implode(',', $r->input('expect3'));
        }

        if($r->has('work1')){
            $work1 = $r->input('work1');
            $app->work .= $work1; 
        }
        if($r->has('work2')){
            $work2 = $r->input('work2');
            $app->work .= $work2;
        }
        if($r->has('work3')){
            $work3 = $r->input('work3');
            $app->work .= $work3;
        }
        if($r->has('work4')){
            $work4 = $r->input('work4');
            $app->work .= $work4;
        }
        if($r->has('work5')){
            $work5 = $r->input('work5');
            $app->work .= $work5;
        }
        if($r->has('work6')){
            $work6 = $r->input('work6');
            $app->work .= $work6;
        }
        if($r->has('work7')){
            $work7 = $r->input('work7');
            $app->work .= $work7;
        }

        $app->save();
        $ar['title'] = "Онлайн подача заявки";
        return redirect("/application")->with('status', "Ваша заявка успешно сохранена! Наш менеджер свяжется с Вами в ближайшее время.");
    }
}
