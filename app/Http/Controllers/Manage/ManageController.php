<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\ImageManager;
use Image;
use DateTimeZone;
use App\Http\Controllers\Manage\Graphs_Disc_DrawController;
use App\Graphs_Disc;
use App\Application;
use App\Questions_Keirsi;
use App\Questions_Keirsi_Kaz;
use App\Answers_Keirsi;
use App\Answers_Keirsi_Kaz;
use App\Questions_Mapp;
use App\Questions_Mapp_Kaz;
use App\User;
use App\Results_Mapp;
use App\Results_Keirsi;
use App\Results_Disc;
use App\Questions_Disc;
use App\Questions_Disc_Kaz;
use App\Questions_Disc_En;
use App\Results_Holl;
use App\Results_Tomas;
use App\Results_Solomin1;
use App\Results_Solomin2;
use App\Disk_Formula;
use App\Disc_Types;
use Auth;
use App\SubadminSetting;
use App\SubadminTestPrice;
use App\Http\Controllers\MappAuto;
use App\MappJob;
use App\MappJobDetails;
use App\MappTranses;
use App\MappHtmlRes;
use App\MappResPercent;

class ManageController extends Controller{
    function getIndex (Request $r){
        $ar = array();
        $ar['title'] = 'Центр управления тестами';
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        if($r->has('start_date') && $r->has('end_date')){
            $ar['firstDay'] = $r->input('start_date')." 00:00:00";
            $ar['lastDay'] = $r->input('end_date')." 23:59:59";
        }else{
            $ok = 1;
            $ar['firstDay'] = Carbon::now()->startOfMonth();
            $ar['lastDay'] = Carbon::now()->endOfMonth();
        }

        $ar['action'] = action("Manage\ManageController@getIndex");

        $ar['mapp_tests'] = $this->tests_math("mapp", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['mapp_total_sum'] = $this->tests_math("mapp", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['disc_tests'] = $this->tests_math("disc", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['disc_total_sum'] = $this->tests_math("disc", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['keirsi_tests'] = $this->tests_math("keirsi", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['keirsi_total_sum'] = $this->tests_math("keirsi", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['holl_tests'] = $this->tests_math("holl", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['holl_total_sum'] = $this->tests_math("holl", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['tomas_tests'] = $this->tests_math("tomas", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['tomas_total_sum'] = $this->tests_math("tomas", $ar['firstDay'], $ar['lastDay'], "sum");

        $ar['solomin_tests'] = $this->tests_math("solomin", $ar['firstDay'], $ar['lastDay'], "count");
        $ar['solomin_total_sum'] = $this->tests_math("solomin", $ar['firstDay'], $ar['lastDay'], "sum");
        if(isset($ok)){
            $ar['firstDay'] = $ar['firstDay']->toDateString();
            $ar['lastDay'] = $ar['lastDay']->toDateString();
        }else{
            $ar['firstDay'] = Carbon::createFromFormat('Y-m-d H:i:s', $ar['firstDay'])->format('Y-m-d');
            $ar['lastDay'] = Carbon::createFromFormat('Y-m-d H:i:s', $ar['lastDay'])->format('Y-m-d');
        }
        
        $ar['subadmins'] = User::where("user_type", 3)
                            ->where('parent_id', 1)
                            ->get();

        $mapp_pocket_math = $this->pockets_math("mapp", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['mapp_pockets_sum'] = $mapp_pocket_math['pockets_sum'];
        $ar['mapp_pockets_num'] = $mapp_pocket_math['pockets_num'];

        $disc_pocket_math = $this->pockets_math("disc", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['disc_pockets_sum'] = $disc_pocket_math['pockets_sum'];
        $ar['disc_pockets_num'] = $disc_pocket_math['pockets_num'];

        $keirsi_pocket_math = $this->pockets_math("keirsi", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['keirsi_pockets_sum'] = $keirsi_pocket_math['pockets_sum'];
        $ar['keirsi_pockets_num'] = $keirsi_pocket_math['pockets_num'];

        $holl_pocket_math = $this->pockets_math("holl", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['holl_pockets_sum'] = $holl_pocket_math['pockets_sum'];
        $ar['holl_pockets_num'] = $holl_pocket_math['pockets_num'];

        $tomas_pocket_math = $this->pockets_math("tomas", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['tomas_pockets_sum'] = $tomas_pocket_math['pockets_sum'];
        $ar['tomas_pockets_num'] = $tomas_pocket_math['pockets_num'];

        $solomin_pocket_math = $this->pockets_math("solomin", $ar['firstDay'], $ar['lastDay'], "sum_count");
        $ar['solomin_pockets_sum'] = $solomin_pocket_math['pockets_sum'];
        $ar['solomin_pockets_num'] = $solomin_pocket_math['pockets_num'];

        return view("manage.index", $ar);
    }

    function pockets_math($test_name, $start_date, $end_date, $action){

        if($action=="sum_count"){
            $all_pockets = SubadminSetting::where("setting", $test_name."_test")
                                            ->where("who_set_id", 1)
                                            ->where("created_at", ">=", $start_date)
                                            ->where("created_at", "<=", $end_date)
                                            ->get();
            $sum_pockets = 0;
            $num_pockets = 0;
            foreach ($all_pockets as $pocket) {
                $sum_pockets += ($pocket->test_price->price*$pocket->value);
                $num_pockets += $pocket->value;
            }
            $ret_ar = array("pockets_sum" => $sum_pockets, "pockets_num" => $num_pockets);
            return $ret_ar;
        }

        if($action == "get"){
            $all_pockets = SubadminSetting::where("setting", $test_name."_test")
                                            ->where("who_set_id", 1)
                                            ->where("created_at", ">=", $start_date)
                                            ->where("created_at", "<=", $end_date)
                                            ->get();
            return $all_pockets;
        }

    }

    public function getStatByPockets ($test_name, $from_date, $to_date){
        $ar['title'] = "Статистика по продаже пакетов по тесту ".$test_name;
        $ar['from_date'] = $from_date;
        $ar['to_date'] = $to_date;
        $ar['test_name'] = $test_name;

        $from_date = $from_date." 00:00:00";
        $to_date = $to_date." 23:59:59";

        $ar['all_pockets'] = $this->pockets_math($test_name, $from_date, $to_date, "get");

        return view("manage.stat_by_pockets", $ar);
    }

    function tests_math ($test_name, $start_date, $end_date, $action){

        if($action == "sum"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", 1)
                            ->sum($test_name."_price");
            return $data;
        }

        if($action == "count"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", 1)
                            ->count();
            return $data;
        }

        if($action == "get"){
            $data = User::where($test_name, '1')
                            ->where($test_name."_finished_date", "!=", NULL)
                            ->where($test_name."_finished_date", ">=", $start_date)
                            ->where($test_name."_finished_date", "<=", $end_date)
                            ->where("parent_id", 1)
                            ->get();
            return $data;
        }

    }
    function getStatByTest ($test_name, $from_date, $to_date){

        $ar['title'] = "Статистика по тесту ".$test_name;
        $ar['from_date'] = $from_date;
        $ar['to_date'] = $to_date;
        $ar['test_name'] = $test_name;

        $from_date = $from_date." 00:00:00";
        $to_date = $to_date." 23:59:59";

        $ar['users'] = $this->tests_math($test_name, $from_date, $to_date, "get");
        $ar['test_name_price'] = $test_name."_price";
        $ar['test_name_finished_date'] = $test_name."_finished_date";
        return view("manage.stat_by_test", $ar);
    }

    function getUserInfo (Request $r, $id){
        $ar = array();
        $ar['title'] = 'Информация о пользователе';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['user_id'] = $id;

        $mapp_results = Results_Mapp::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($mapp_results!=NULL){
            $mapp_auto = new MappAuto();
            if($mapp_auto->check_auth($user_info['mapp_login'], $user_info['mapp_pass'], $user_info['id'])){
                $ar['auth'] = "1";
            }else{
                $ar['auth'] = '0';
            }
        }
        $keirsi_results = Results_Keirsi::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $disc_results = Results_Disc::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $holl_results = Results_Holl::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $tomas_results = Results_Tomas::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $solomin_results = Results_Solomin1::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $ar['disc_results'] = $disc_results;
        $ar['keirsi_results'] = $keirsi_results;
        $ar['mapp_results'] = $mapp_results;
        $ar['holl_results'] = $holl_results;
        $ar['tomas_results'] = $tomas_results;
        $ar['solomin_results'] = $solomin_results;

        return view("manage.users.info", $ar);
    }

    //The KEIRSI TEST Methods
    function getKeirsiEdit (){
    	$ar = array();
        $ar['title'] = 'Редактор теста Кейрси';
        $ar['action'] = action('Manage\ManageController@savePostKeirsi');
        
        $questions = Questions_Keirsi::all();
        $ar['questions'] = $questions;

        return view("manage.keirsi.edit", $ar);
    }

    function getKeirsiEditKaz (){
        $ar = array();
        $ar['title'] = 'КЕЙРСИ тест өзгерту';
        $ar['action'] = action('Manage\ManageController@savePostKeirsiKaz');
        
        $questions = Questions_Keirsi_Kaz::all();
        $ar['questions'] = $questions;

        return view("manage.keirsi.editkaz", $ar);
    }

    function savePostKeirsi (Request $r){
        for($i = 1; $i <= 70; $i++){
            $answers_a = new Answers_Keirsi;
            Questions_Keirsi::where("id", "=", $i)->update(['text'=>$r->input('q'.$i)]);
            Answers_Keirsi::where("question_id", "=", $i)
                            ->where("type", "=", "a")
                            ->update(['text'=>$r->input('q'.$i.'a')]);
            Answers_Keirsi::where("question_id", "=", $i)
                            ->where("type", "=", "b")
                            ->update(['text'=>$r->input('q'.$i.'b')]);
        }
        return back();
    }

    function savePostKeirsiKaz (Request $r){
        for($i = 1; $i <= 70; $i++){
            $answers_a = new Answers_Keirsi_Kaz;
            Questions_Keirsi_Kaz::where("id", "=", $i)->update(['text'=>$r->input('q'.$i)]);
            Answers_Keirsi_Kaz::where("question_id", "=", $i)
                            ->where("type", "=", "a")
                            ->update(['text'=>$r->input('q'.$i.'a')]);
            Answers_Keirsi_Kaz::where("question_id", "=", $i)
                            ->where("type", "=", "b")
                            ->update(['text'=>$r->input('q'.$i.'b')]);
        }
        return back();
    }

    function getKeirsiResultsByDate ($id, $date){
        $ar['title'] = "Детализация результатов КЕЙРСИ";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Keirsi::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        $b = array();
        $k = 1;
        foreach($results_by_date as $key=>$value){
            if($value->answer == "a") $a[$k] = 1; else $a[$k] = 0;
            if($value->answer == "b") $b[$k] = 1; else $b[$k] = 0;
            $k++;
        }

        $sum_E = 0; $sum_I = 0;
        for($i=1; $i<=70; $i+=7){ $sum_E += $a[$i]; $sum_I += $b[$i]; }

        $sum_S = 0; $sum_N = 0;
        for($i=2; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }
        for($i=3; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }

        $sum_T = 0; $sum_F = 0;
        for($i=4; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }
        for($i=5; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }

        $sum_J = 0; $sum_P = 0;
        for($i=6; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }
        for($i=7; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }

        $ar['E'] = $sum_E; $ar['I'] = $sum_I; $ar['S'] = $sum_S; $ar['N'] = $sum_N;
        $ar['T'] = $sum_T; $ar['F'] = $sum_F; $ar['J'] = $sum_J; $ar['P'] = $sum_P;

        if($sum_E < $sum_I) $first = "I"; else $first = "E";
        if($sum_S < $sum_N) $second = "N"; else $second = "S";
        if($sum_T < $sum_F) $third = "F"; else $third = "T";
        if($sum_J < $sum_P) $fourth = "P"; else $fourth = "J";

        $ar["formula"] = $first.$second.$third.$fourth;

        return view("manage.keirsi.resultsbydate", $ar);
    }
    //END OF KEIRSI Controller Methods
    //The MAPP Test Controller Methods
    function getMappEdit (){
        $ar = array();
        $ar['title'] = 'Редактор теста MAPP';
        $ar['action'] = action('Manage\ManageController@savePostMapp');
        $ar['group_number'] = Questions_Mapp::all()
                                            ->groupBy('group_id')
                                            ->count();

        $ar['groups'] = Questions_Mapp::all()->groupBy('group_id')->toArray();
        return view("manage.mapp.edit", $ar);
    }

    function getMappEditKaz (){
        $ar = array();
        $ar['title'] = 'MAPP тест өзгерту';
        $ar['action'] = action('Manage\ManageController@savePostMappKaz');
        $ar['group_number'] = Questions_Mapp_Kaz::all()
                                            ->groupBy('group_id')
                                            ->count();

        $ar['groups'] = Questions_Mapp_Kaz::all()->groupBy('group_id')->toArray();
        return view("manage.mapp.editkaz", $ar);
    }

    function savePostMapp (Request $r){
        $group_number = $r->input('group_number');
        $now = Carbon::now(new DateTimeZone('Asia/Almaty'));
        $to_ins = array(
                    array('text'=>$r->input('predp_1'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now),
                    array('text'=>$r->input('predp_2'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now),
                    array('text'=>$r->input('predp_3'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now)
                );
        Questions_Mapp::insert($to_ins);
        return back();
    }

    function saveEditedMapp (Request $r){

        $total = $r->input("total_preds");

        for($i = 1; $i <= $total; $i++){
            $new_text = $r->input("text_of".$i);
            $id = $r->input("tid".$i);
            Questions_Mapp::where('id', $id)->update(["text"=>$new_text]);
        }

        return back();
    }

    function saveEditedMappKAZ (Request $r){

        $total = $r->input("total_preds");

        for($i = 1; $i <= $total; $i++){
            $new_text = $r->input("text_of".$i);
            $id = $r->input("tid".$i);
            Questions_Mapp_Kaz::where('id', $id)->update(["text"=>$new_text]);
        }

        return back();
    }

     function savePostMappKaz (Request $r){
        $group_number = $r->input('group_number');
        $now = Carbon::now(new DateTimeZone('Asia/Almaty'));
        $to_ins = array(
                    array('text'=>$r->input('predp_1'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now),
                    array('text'=>$r->input('predp_2'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now),
                    array('text'=>$r->input('predp_3'), 'group_id'=>$group_number, 'created_at'=> $now,'updated_at'=> $now)
                );
        Questions_Mapp_Kaz::insert($to_ins);
        return back();
    }

    function getMappAllTraslatePage (){

        $array['title'] = "Переводы MAPP результатов";
        $array['action'] = action('Manage\ManageController@saveMappAllTranslate');
        $array['transes'] = MappTranses::paginate(20);

        return view("manage.mapp.translate_all", $array);

    }

    function saveMappAllTranslate (Request $r){

        $total = $r->input("total_val");
        for($i=1; $i<=$total; $i++){
            $row_id = $r->input("trans_".$i);
            $en_val = $r->input("en_row_".$i);
            $ru_val = $r->input("ru_row_".$i);
            $kz_val = $r->input("kz_row_".$i);
            $kosymwa_val = $r->input("kosymwa_row_".$i);
            if($r->has('not_in_kz'.$i)){
                $not_in_kz = 1;
            }else{
                $not_in_kz = 0;
            }
            MappTranses::where("id", $row_id)->update([
                                                    "eng" => $en_val, 
                                                    "rus" => $ru_val,
                                                    "kaz" => $kz_val,
                                                    "kosymwa" => $kosymwa_val,
                                                    "not_in_kz" => $not_in_kz
                                                    ]);
        }

        return back();

    }

    function add_one_translate(){
        MappTranses::insert(["eng" => "", "rus" => "", "kaz" => ""]);
        return back();
    }

    function getMappTraslatePage (){

        $array['title'] = "Переводы MAPP результатов";
        $array['action'] = action('Manage\ManageController@saveMappTranslate');
        $array['jobs'] = MappJob::paginate(20);
        $array['is_desc'] = 0;

        return view("manage.mapp.translate", $array);

    }

    function getMappTraslateJobDesc (){

        $array['title'] = "Переводы MAPP результатов";
        $array['action'] = action('Manage\ManageController@saveMappTranslateJobDesc');
        $array['jobs'] = MappJob::paginate(20);
        $array['is_desc'] = 1;

        return view("manage.mapp.translate", $array);
    }

    function getMappTraslateJobDetails (){

        $array['title'] = "Переводы MAPP результатов";
        $array['action'] = action('Manage\ManageController@saveMappTranslateJobDetails');
        $array['jobs'] = MappJobDetails::paginate(20);

        return view("manage.mapp.translate_details", $array);
    }

    function saveMappTranslate (Request $r){

        $total = $r->input("total_val");
        for($i=1; $i<=$total; $i++){
            $row_id = $r->input("trans_".$i);
            $en_val = $r->input("en_row_".$i);
            $ru_val = $r->input("ru_row_".$i);
            $kz_val = $r->input("kz_row_".$i);
            $univers = $r->input("univer_row_".$i);
            if($r->has('not_in_kz'.$i)){
                $not_in_kz = 1;
            }else{
                $not_in_kz = 0;
            }
            MappJob::where("id", $row_id)->update([
                                                    "job_name" => $en_val, 
                                                    "job_name_rus" => $ru_val,
                                                    "job_name_kaz" => $kz_val,
                                                    "univers" => $univers,
                                                    "not_in_kz" => $not_in_kz
                                                    ]);
        }

        return back();
    }

    function saveMappTranslateJobDesc (Request $r){

        $total = $r->input("total_val");
        for($i=1; $i<=$total; $i++){
            $row_id = $r->input("trans_".$i);
            $en_val = $r->input("en_row_".$i);
            $ru_val = $r->input("ru_row_".$i);
            $kz_val = $r->input("kz_row_".$i);
            MappJob::where("id", $row_id)->update([
                                                    "job_desc" => $en_val, 
                                                    "job_desc_rus" => $ru_val,
                                                    "job_desc_kaz" => $kz_val,
                                                    ]);
        }

        return back();
    }

    function saveMappTranslateJobDetails (Request $r){

        $total = $r->input("total_val");
        for($i=1; $i<=$total; $i++){
            $row_id = $r->input("trans_".$i);
            $en_val = $r->input("en_row_".$i);
            $ru_val = $r->input("ru_row_".$i);
            $kz_val = $r->input("kz_row_".$i);
            MappJobDetails::where("id", $row_id)->update([
                                                    "detail_name" => $en_val, 
                                                    "detail_name_rus" => $ru_val,
                                                    "detail_name_kaz" => $kz_val,
                                                    ]);
        }

        return back();
    }

    function getMappResults ($id, $date, Request $r){
        $ar = array();
        $ar['title'] = 'Страница результатов';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['date'] = $date;
        $ar['user_id'] = $id;

        $ar['group_number'] = Questions_Mapp::all()
                                            ->groupBy('group_id')
                                            ->count();
        $ar['groups'] = Questions_Mapp::all()->groupBy('group_id');

        return view("manage.mapp.results", $ar);
    }

    public function download_mapp_results ($user_id, $lang){

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        download:
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();
        if($result_in_base->count() > 0){

            $response = $result_in_base[0]->html;

            if($lang == "kaz"){
                print("<center><h3>".$name." ".$surname." клиенттің MAPP нәтижесі</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }else{
                print("<center><h3>Результаты MAPP клиента ".$name." ".$surname."</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }

            print(' <link href="http://recruit.assessment.com/Content/bootstrap.css" rel="stylesheet"/> <link href="http://recruit.assessment.com/Content/site.css" rel="stylesheet"/>');
            
            print('<link rel="stylesheet" href="https://rawgit.com/tpreusse/radar-chart-d3/master/src/radar-chart.css">
            <style>
                .radar-chart .area {
                    fill-opacity: 0.7;
                }

                .radar-chart.focus .area {
                    fill-opacity: 0.3;
                }

                .radar-chart.focus .area.focused {
                    fill-opacity: 0.9;
                }

                .inner_content {
                    max-width: 900px;
                    margin: auto auto;
                }
            </style>');
            
            print("<style>
                        table{
                            width:50% !important;
                            margin-left:auto; 
                            margin-right:auto;
                        }
                    </style>
            ");
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            /*
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=Report.doc");*/
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kaz"){
                    $det2 = $detail->detail_name_kaz;   
                }

                $out[1][0] = preg_replace("~".$det1."~", $det2, $out[1][0]);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kaz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $out[2][0] = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $out[2][0]);
                }

                $out[1][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[1][0]);
                $out[1][0] = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $out[1][0]);

                $out[2][0] = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
            
            }

            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);
            $out[2][0] = preg_replace('/<a name="VOC_18"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>/', "", $out[2][0]);

            preg_match_all('/<td>(.*?)<\/td>[\t\n\r\s]+<td>(\d{1,3})<\/td>[\t\n\r\s]+<td>(\d)<\/td>/', $out[2][0], $all_jobs);

            array_multisort($all_jobs[2], SORT_DESC, $all_jobs[1], $all_jobs[3]);

            $parts_explanations = new MappHtmlRes();
            if($lang == "kaz"){
                print($parts_explanations->MAPP_1_PART_KZ);
            }else{
                print($parts_explanations->MAPP_1_PART);
            }
            print($out[1][0]);
            
            print("<hr>");

            if($lang == "kaz"){
                print($parts_explanations->MAPP_2_PART_KZ);
            }else{
                print($parts_explanations->MAPP_2_PART);
            }

            print($out[2][0]);

            print("<hr>");
                
            if($lang == "kaz"){
                print('<center><b>'.$parts_explanations->MAPP_4_PART_KZ.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Мамандықтардың түрi</b></td>
                        <td><b>Мотивациялық пайыз</b></td>
                        <td><b>Мотивациялық  деңгей</b></td>
                        <td><b>Потенциалды арттыру курстары</b></td>
                    </tr>');
            }else{
                print('<center><b>'.$parts_explanations->MAPP_4_PART.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Виды деятельностей</b></td>
                        <td><b>Процент мотивации</b></td>
                        <td><b>Уровень мотивации</b></td>
                        <td><b>Курсы для развития потенциала</b></td>
                    </tr>');
            }

            for($i = 0; $i < 20; $i++){
                if($lang == "ru"){
                    $kosymwa = MappTranses::where('rus', $all_jobs[1][$i])->get();
                }else{
                    $kosymwa = MappTranses::where('kaz', $all_jobs[1][$i])->get();
                }
                print("<tr>");
                    print("<td>".$all_jobs[1][$i]."</td>");
                    print("<td>".$all_jobs[2][$i]."</td>");
                    print("<td>".$all_jobs[3][$i]."</td>");
                    print("<td>");
                    $kos = explode(",", $kosymwa[0]->kosymwa);
                    foreach ($kos as $key => $value) {
                        print($value)."<br>";
                    }
                    print("</td>");
                print("</tr>");
            }
            print('</table>');


            
            print('<script src="http://recruit.assessment.com//Scripts/jquery-1.10.2.js"></script>
            <script src="http://recruit.assessment.com//Scripts/jquery-3.1.1.js"></script>

            <script src="http://recruit.assessment.com//Scripts/bootstrap.js"></script>
            <script src="http://recruit.assessment.com//Scripts/respond.js"></script>

    
            <script src="//d3js.org/d3.v3.min.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChart.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChartCreation.js"></script>
            <script>
                function CreateNewRadarGraph(graphData, graphName) {
                    var data = FormatGraphData(graphData);
                    RadarChart.draw(graphName, data, maxValue = 5, minValue = 0, levels = 5);
                }</script>');
                print($scripts[0]);
            
            get_job_auth:
            $result_in_base2 = MappResPercent::where("user_id", $user_id)
                                        ->where("type", "j")
                                        ->get();
            if($result_in_base2->count() > 0){/*
                print("<hr>");
                if($lang == "kaz"){
                    print($parts_explanations->MAPP_3_PART_KZ);
                }else{
                    print($parts_explanations->MAPP_3_PART);
                }
                print('<table class="table table-striped table-bordered table-responsive">');
                print("<tr>");
                if($lang == "kaz"){
                    print("<td><b>Мамандықтың атауы</b></td>");
                    print("<td><b>Пайыз</b></td>");
                    print("<td><b>Қазақстанның университеттері</b></td>");
                }else{
                    print("<td><b>Название профессии</b></td>");
                    print("<td><b>Процент</b></td>");
                    print("<td><b>Университеты Казахстана</b></td>");
                }
                print("</tr>");
                 foreach($result_in_base2 as $job){
                    if($job->job['not_in_kz']!=1){
                        print("<tr>");
                            if($lang == "ru")
                                print("<td><b>".$job->job['job_name_rus']."</b><br>".$job->job['job_desc_rus']."</td>");
                            if($lang == "kaz")
                                print("<td><b>".$job->job['job_name_kaz']."</b><br>".$job->job['job_desc_kaz']."</td>");
                            print("<td>".$job['percent']."%</td>");
                            //print("<td>".$job->job['univers']."</td>");
                            if($lang == "kaz"){
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Университеттердің тізімі</a></td>");
                            }else{
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Список университетов</a></td>");
                            }
                        print("</tr>");
                    }
                 }
                 print("</table>");*/
            }else if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
        
                $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/SortedMAPPMatchToONET';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)<\/a><\/h3>[\t\n\r\s]+<p>(.*?)<\/p>/', $response, $out);
                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)%<\/a><\/h3>/', $response, $out2);

                $to_ins = array();

                for($i=0; $i<20; $i++){
                    //$to_ins[] = array("sys_id" => $out[1][$i], "job_name" => $out[2][$i], "job_desc" => $out[3][$i]);
                    $sys_id = $out[1][$i];

                    $job_data = MappJob::where("sys_id", $sys_id)->get();

                    $job_percent = $out2[2][$i];
                    $job_rel_id = $job_data[0]->id;

                    $to_ins[] = array(
                        "type" => "j",
                        "rel_id" => $job_rel_id,
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $job_percent
                    );
                    $mapp = new MappAuto();
                    $mapp->getJob($sys_id, $user_id);
                }
                MappResPercent::insert($to_ins);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto get_job_auth;
            }
        }else{
            if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
            
                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                
                MappHtmlRes::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download;
            }
        }

    }

    //END OF MAPP Controller Methods

    //START OF DISC Controller Methods
     function getDiscEditEn (){
        $ar = array();
        $ar['title'] = "Edit DISC test"; 
        $ar['action'] = action('Manage\ManageController@savePostDiscEn');

        $ar['questions'] = Questions_Disc_En::all();

        return view("manage.disc.edit", $ar);
    }
    function getDiscEdit (){
        $ar = array();
        $ar['title'] = "Редактировать тест DISC"; 
        $ar['action'] = action('Manage\ManageController@savePostDisc');

        $ar['questions'] = Questions_Disc::all();

        return view("manage.disc.edit", $ar);
    }
    function getDiscEditKaz (){
        $ar = array();
        $ar['title'] = "DISC тест өзгерту"; 
        $ar['action'] = action('Manage\ManageController@savePostDiscKaz');

        $ar['questions'] = Questions_Disc_Kaz::all();

        return view("manage.disc.editkaz", $ar);
    }
    function savePostDiscEn (Request $r){
        $ar['title'] = "Edit DISC Test"; 
        $ar['action'] = action('Manage\ManageController@savePostDiscEn');
        $ar['questions'] = Questions_Disc_En::all();

        for($i = 1; $i<=96; $i++){
            Questions_Disc_En::where("id", "=", $i)->update(['question'=>$r->input('question'.$i), "most"=>$r->input('most'.$i), "least"=>$r->input("least".$i)]);
        }
        return back()->with('status', 'Успешно сохранено!');;
    }
    function savePostDisc (Request $r){
        $ar['title'] = "Редактировать тест DISC"; 
        $ar['action'] = action('Manage\ManageController@savePostDisc');
        $ar['questions'] = Questions_Disc::all();

        for($i = 1; $i<=96; $i++){
            Questions_Disc::where("id", "=", $i)->update(['question'=>$r->input('question'.$i), "most"=>$r->input('most'.$i), "least"=>$r->input("least".$i)]);
        }
        return back()->with('status', 'Успешно сохранено!');;
    }

    function savePostDiscKaz (Request $r){
        $ar['questions'] = Questions_Disc_Kaz::all();

        for($i = 1; $i<=96; $i++){
            Questions_Disc_Kaz::where("id", "=", $i)->update(['question'=>$r->input('question'.$i), "most"=>$r->input('most'.$i), "least"=>$r->input("least".$i)]);
        }
        return back()->with('status', 'Успешно сохранено!');
    }

    function getDiscResults ($id, $date, Request $r, $lang){
        $ar = array();
        $ar['title'] = 'Страница результатов';
        $user_info = User::findOrFail($id);
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['date'] = $date;
        $ar['user_id'] = $id;

        $results_by_date = Results_Disc::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $most = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $least = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $change = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        foreach ($results_by_date as $key => $value) {
            switch($value->get_most->most){
                case "D":$most["D"]++;break;
                case "I":$most["I"]++;break;
                case "S":$most["S"]++;break;
                case "C":$most["C"]++;break;
                case "*":$most["*"]++;break;
            }
            switch($value->get_least->least) {
                case "D":$least["D"]++;break;
                case "I":$least["I"]++;break;
                case "S":$least["S"]++;break;
                case "C":$least["C"]++;break;
                case "*":$least["*"]++;break;
            }
        }
        $most['total'] = $most['D']+$most['I']+$most['S']+$most['C']+$most['*'];
        $least['total'] = $least['D']+$least['I']+$least['S']+$least['C']+$least['*'];
        $change['D'] = $most['D']-$least['D'];
        $change['I'] = $most['I']-$least['I'];
        $change['S'] = $most['S']-$least['S'];
        $change['C'] = $most['C']-$least['C'];
        $ar['most'] = $most;
        $ar['least'] = $least;


        $D_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "D")->get()->toArray();
        $I_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "I")->get()->toArray();
        $S_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "S")->get()->toArray();
        $C_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "C")->get()->toArray();

        $D_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "D")->get()->toArray();
        $I_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "I")->get()->toArray();
        $S_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "S")->get()->toArray();
        $C_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "C")->get()->toArray();

        $D_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "D")->get()->toArray();
        $I_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "I")->get()->toArray();
        $S_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "S")->get()->toArray();
        $C_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "C")->get()->toArray();

        $img_most_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_most, $I_numbers_most, $S_numbers_most, $C_numbers_most, $most["D"], $most["I"], $most["S"], $most["C"]);
        $img_least_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_least, $I_numbers_least, $S_numbers_least, $C_numbers_least, $least["D"], $least["I"], $least["S"], $least["C"]);
        $img_change_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_change, $I_numbers_change, $S_numbers_change, $C_numbers_change, $change["D"], $change["I"], $change["S"], $change["C"]);

        $ar['img_most_graph'] = $img_most_graph['image']->encode('data-url');
        $ar['img_least_graph'] = $img_least_graph['image']->encode('data-url');
        $ar['img_change_graph'] = $img_change_graph['image']->encode('data-url');

        $ar['top_most_letters'] = $img_most_graph['top_letters'];
        $ar['top_least_letters'] = $img_least_graph['top_letters'];
        $ar['top_change_letters'] = $img_change_graph['top_letters'];

        //Зеркало формула и описание
        $formula = "";
        $pred = 1000;
        foreach($img_change_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula .= "=";
            }
            if($value > $pred){
                $formula .= ".";
            }
            $formula .= $letter;
            $pred = $value;
        }
        $ar['change_formula'] = $formula;
        $type_id_q = Disk_Formula::where("formula", $formula)->first();
        $type_id = $type_id_q['type_id'];
        $ar['description'] = Disc_Types::where('id',$type_id)->get();

        $top_prof = $formula[0];
        if(isset($formula[2])){
            $top_prof .= $formula[2];
        }
        $ar['top_prof'] = $top_prof;

        //Маска - формула и описание

        $formula_mask = "";
        $pred = 1000;
        foreach($img_most_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_mask .= "=";
            }
            if($value > $pred){
                $formula_mask .= ".";
            }
            $formula_mask .= $letter;
            $pred = $value;
        }
        $ar['most_formula'] = $formula_mask;
        $type_id_q_most = Disk_Formula::where("formula", $formula_mask)->first();
        $type_id_mask = $type_id_q_most['type_id'];
        $ar['description_mask'] = Disc_Types::where('id',$type_id_mask)->get();

        //Сердце

        $formula_heart = "";
        $pred = 1000;
        foreach($img_least_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_heart .= "=";
            }
            if($value > $pred){
                $formula_heart .= ".";
            }
            $formula_heart .= $letter;
            $pred = $value;
        }
        $ar['least_formula'] = $formula_heart;
        $type_id_q_least = Disk_Formula::where("formula", $formula_heart)->first();
        $type_id_least = $type_id_q_least['type_id'];
        $ar['description_least'] = Disc_Types::where('id',$type_id_least)->get();

        $ar['lang'] = $lang;
        return view("manage.disc.results", $ar);
    }
    function getDiscFormulasEdit (){

        $ar = array();
        $ar['title'] = 'Редактировать соответствие формул';
        $ar['action'] = action('Manage\ManageController@saveDiscFormulas');
        $ar['action_add'] = action('Manage\ManageController@discAddFormula');

        $ar['all_formulas'] = Disk_Formula::all();
        $ar['all_types'] = Disc_Types::all();

        return view("manage.disc.formulas", $ar);
    }
    function discAddFormula (){

        $formula = new Disk_Formula();
        $formula->formula = "";
        $formula->type_id = NULL;
        $formula->save();

        return back();
    }
    function saveDiscFormulas (Request $r){

        $all = Disk_Formula::all()->count();

        for($i=1; $i<=$all; $i++){
            $formula = $r->input("f".$i);
            $type_id = $r->input("t".$i);
            Disk_Formula::where("id", $i)->update(["formula"=>$formula, "type_id"=>$type_id]);
        }

        return back();
    }
    function discTextEdit ($text_id, $lang){
        $ar = array();
        $ar['title'] = 'Редактировать текст';
        $text_q = Disc_Types::where("id", $text_id)->get();
        if($lang=="ru"){
            $ar['text'] = $text_q[0]['desciption'];
        }else{
            $ar['text'] = $text_q[0]['kaz_description'];
        }
        $ar['action'] = action("Manage\ManageController@saveDiscText");
        $ar['id'] = $text_id;
        $ar['lang'] = $lang;
        return view("manage.disc.edit_text", $ar);
    }
    function saveDiscText (Request $r){
        $lang = $r->input("lang");
        $text = $r->input("disc_text");
        $id = $r->input("id");
        if($lang=="ru"){
            Disc_Types::where("id", $id)->update(["desciption" => $text]);
        }else{
             Disc_Types::where("id", $id)->update(["kaz_description" => $text]);
        }

        return back();
    }
    //END of DISC

    //Users Controller Methods
    function getAddUser (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('Manage\ManageController@savePostNewUser');
        

        return view("manage.users.add_user", $ar);

    }

    function getAddSubadmin (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('Manage\ManageController@savePostNewSubadmin');
        return view("manage.users.add_subadmin", $ar);

    }

    function getAllUsers (){
        $ar = array();
        $ar['title'] = 'Все пользователи';

        return view("manage.users.all_users", $ar);
    }

    function savePostNewUser (Request $request){
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->login = $request->input('login');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->mapp_login = $request->input('mapp_login');
        $new_user->mapp_pass = $request->input('mapp_pass');
        $new_user->parent_id = Auth::user()->id;

        if($request->has('mapp_test')){
            $new_user->mapp = 1;
        }
        $new_user->mapp_price = $request->input('mapp_test_price');
        if($request->has('disc_test')){
            $new_user->disc = $request->input("disc_test");
        }
        $new_user->disc_price = $request->input('disc_test_price');
        if($request->has('keirsi_test')){
            $new_user->keirsi = 1;
        }
        $new_user->keirsi_price = $request->input('keirsi_test_price');
        if($request->has('holl_test')){
            $new_user->holl = 1;
        }
        $new_user->holl_price = $request->input('holl_test_price');
        if($request->has('tomas_test')){
            $new_user->tomas = 1;
        }
        $new_user->tomas_price = $request->input('tomas_test_price');
        if($request->has('solomin_test')){
            $new_user->solomin = 1;
        }
        $new_user->solomin_price = $request->input('solomin_test_price');

        $new_user->save();
            
        return back()->with('status', 'Успешно добавлен новый пользователь!');
    }

    function savePostNewSubadmin (Request $request){
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->login = $request->input('login');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->parent_id = Auth::user()->id;
        $new_user->save();

        $settings_array = array();

        if($request->input("mapp_test") > 0){
            $settings_array['mapp_test'] = $request->input('mapp_test');
        }
        if($request->input("disc_test") > 0){
            $settings_array['disc_test'] = $request->input('disc_test');
        }
        if($request->input("keirsi_test") > 0){
            $settings_array['keirsi_test'] = $request->input('keirsi_test');
        }
        if($request->input("holl_test") > 0){
            $settings_array['holl_test'] = $request->input('holl_test');
        }
        if($request->input("tomas_test") > 0){
            $settings_array['tomas_test'] = $request->input('tomas_test');
        }
        if($request->input("solomin_test") > 0){
            $settings_array['solomin_test'] = $request->input('solomin_test');
        }
    
        foreach ($settings_array as $key => $value) {
            $new_user_settings = new SubadminSetting;
            $new_user_settings->who_set_id = Auth::user()->id;
            $new_user_settings->user_id = $new_user->id;
            $new_user_settings->setting = $key;
            $new_user_settings->value = $value;
            $new_user_settings->save();

            $new_subadmin_test_price = new SubadminTestPrice;
            $new_subadmin_test_price->setting_id = $new_user_settings->id;
            $new_subadmin_test_price->price = $request->input($key.'_price');
            $new_subadmin_test_price->save();
        }

        return back()->with('status', 'Успешно добавлен новый subadmin с указанным количеством тестов!');
    }

    function allApplications (){
        $ar['title'] = "Все заявки";
        $ar['apps'] = Application::orderBy('created_at', 'desc')->get();

        return view("manage.users.applications", $ar);
    }

    function applicationById ($id){
        $ar['title'] = "Просмотр информации о заявке";
        $ar['app'] = Application::findOrFail($id);

        $expect1 = array("1" => "Тест на тип личности", "2" => "Консультация", "3" => "Консультация с родителями", "4" => "Консультация с учителями", "5" => "Тренинги");
        $expect2 = array("1" => "Диагностический тест MAPP (30+ страниц личного отчета)", "2" => "Диагностический тест Keyer (5+ страниц личного отчета)", "3" => "Генетик-тест (5+ страниц личного отчета)", "4" => "Тренинги и семинары с психологом", "5" => "Консультации по всем тестам", "6" => "Лекции от представителей разных профессий");
        $expect3 = array("1" => "Личные консультации с представителями разных профессий", "2" => "Поездки в организации", "3" => "Стажировки (наблюдения, консультации)", "4" => "Бизнес-кейсы / практические занятия", "5" => "Ориентационная программа");

        $work = array("1" => "Полный пакет (консультации с родителями и преподавателями, диагностика с использованием 4 тестов, тренинги, бизнес-кейсы, ориентационные программы, стажировки)", "2" => "Консультация с родителями", "3" => "Консультация с учителями", "4" => "Диагностика с комплексным тестирование MAPP + консультация (более 30 страниц личного отчета)", "5" => "Диагностика с помощью Генетик-тест", "6" => "Диагностика с помощью теста Keyer + консультация", "7" => "Диагностика с помощью теста Личности + консультация");

        $ar['expect1'] = $expect1;
        $ar['expect2'] = $expect2;
        $ar['expect3'] = $expect3;
        $ar['work'] = $work;


        return view("manage.users.appbyid", $ar);

    }

    public function ajax_all_users ($type){

        $users = User::select('login', 'name', 'surname', 'user_type', 'id')
                        ->where('is_active', $type)
                        ->where('parent_id', 1)
                        ->orWhere('parent_id', NULL)
                        ->get()
                        ->toArray();
        $output = array(
            'data' => $users
        );
        return response()->json($output);
    }

    public function getEditUser ($user_id){

        $ar['title'] = "Редактировать настройки пользователя";
        $user = User::where('id', $user_id)->get();
        $ar['user'] = $user;
        $ar['action'] = action("Manage\ManageController@saveEditedUser");

        $id = $user_id;

        $mapp_results = Results_Mapp::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $keirsi_results = Results_Keirsi::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $disc_results = Results_Disc::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $holl_results = Results_Holl::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $tomas_results = Results_Tomas::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $solomin_results = Results_Solomin1::where("user_id", $id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $mapp_results!=NULL ? $ar['mapp_passed'] = '1' : $ar['mapp_passed'] = '0';
        $keirsi_results!=NULL ? $ar['keirsi_passed'] = '1' : $ar['keirsi_passed'] = '0';
        $disc_results!=NULL ? $ar['disc_passed'] = count($disc_results) : $ar['disc_passed'] = '0';
        $holl_results!=NULL ? $ar['holl_passed'] = '1' : $ar['holl_passed'] = '0';
        $tomas_results!=NULL ? $ar['tomas_passed'] = '1' : $ar['tomas_passed'] = '0';
        $solomin_results!=NULL ? $ar['solomin_passed'] = '1' : $ar['solomin_passed'] = '0';

        return view("manage.users.edit", $ar);
    }

    public function add_one_more_disc ($user_id){
        $user = User::where('id', $user_id)->get();
        $disc_c = $user[0]->disc;
        User::where('id', $user_id)->update(["disc" => $disc_c+1]);
        return back();
    }

    public function getEditSubadmin ($user_id){

        $ar['title'] = "Редактировать настройки пользователя";
        $user = User::where('id', $user_id)->get();
        $ar['user'] = $user;
        $ar['action'] = action("Manage\ManageController@saveEditedSubadmin");

        $id = $user_id;

        $ar['all_given_tests'] = SubadminSetting::where("user_id", $id)->orderBy('created_at')->get();

        return view("manage.users.edit_subadmin", $ar);
    }

    public function saveEditedSubadmin (Request $request){

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $surname = $request->input('surname');
        $password = Hash::make($request->input('password'));
        $is_active = $request->input('is_active');

        if(strlen($request->input('password'))>1){
            User::where('id', $user_id)->update(["password"=>$password]);
        }
        User::where('id', $user_id)
                ->update([
                    "name" => $name,
                    "surname" => $surname,
                    "is_active" => $is_active]);

        $settings_array = array();

        if($request->input("mapp_test") > 0){
            $settings_array['mapp_test'] = $request->input('mapp_test');
        }
        if($request->input("disc_test") > 0){
            $settings_array['disc_test'] = $request->input('disc_test');
        }
        if($request->input("keirsi_test") > 0){
            $settings_array['keirsi_test'] = $request->input('keirsi_test');
        }
        if($request->input("holl_test") > 0){
            $settings_array['holl_test'] = $request->input('holl_test');
        }
        if($request->input("tomas_test") > 0){
            $settings_array['tomas_test'] = $request->input('tomas_test');
        }
        if($request->input("solomin_test") > 0){
            $settings_array['solomin_test'] = $request->input('solomin_test');
        }
    
        foreach ($settings_array as $key => $value) {
            $new_user_settings = new SubadminSetting;
            $new_user_settings->who_set_id = Auth::user()->id;
            $new_user_settings->user_id = $user_id;
            $new_user_settings->setting = $key;
            $new_user_settings->value = $value;
            $new_user_settings->save();

            $new_subadmin_test_price = new SubadminTestPrice;
            $new_subadmin_test_price->setting_id = $new_user_settings->id;
            $new_subadmin_test_price->price = $request->input($key.'_price');
            $new_subadmin_test_price->save();
        }


        return back()->with('status', "Успешно обновлено!");;
    }

    public function saveEditedUser (Request $request){

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $surname = $request->input('surname');
        $user_type = $request->input('role');
        $password = Hash::make($request->input('password'));
        $mapp_login = $request->input('mapp_login');
        $mapp_pass = $request->input('mapp_pass');
        $is_active = $request->input('is_active');


        if(strlen($request->input('password'))>1){
            User::where('id', $user_id)->update(["password"=>$password]);
        }

        if($request->has('mapp_test')){
            $mapp = 1;
        }else{
            $mapp = 0;
        }
        $mapp_price = $request->input('mapp_test_price');
        if($request->has('disc_test')){
            $disc = 1;
        }else{
            $disc = 0;
        }
        $disc_price = $request->input('disc_test_price');
        if($request->has('keirsi_test')){
            $keirsi = 1;
        }else{
            $keirsi = 0;
        }
        $keirsi_price = $request->input('keirsi_test_price');
        if($request->has('holl_test')){
            $holl = 1;
        }else{
            $holl = 0;
        }
        $holl_price = $request->input('holl_test_price');
        if($request->has('tomas_test')){
            $tomas = 1;
        }else{
            $tomas = 0;
        }
        $tomas_price = $request->input('tomas_test_price');
        if($request->has('solomin_test')){
            $solomin = 1;
        }else{
            $solomin = 0;
        }
        $solomin_price = $request->input('solomin_test_price');

        User::where('id', $user_id)
                ->update([
                    "name" => $name,
                    "surname" => $surname,
                    "user_type" => $user_type,
                    "mapp_login" => $mapp_login,
                    "mapp_pass" => $mapp_pass,
                    "mapp" => $mapp,
                    "disc" => $disc,
                    "keirsi" => $keirsi,
                    "holl" => $holl,
                    "tomas" => $tomas,
                    "solomin" => $solomin,
                    "mapp_price" => $mapp_price,
                    "disc_price" => $disc_price,
                    "keirsi_price" => $keirsi_price,
                    "holl_price" => $holl_price,
                    "tomas_price" => $tomas_price,
                    "solomin_price" => $solomin_price,
                    "is_active" => $is_active
                ]);

        return back()->with('status', "Успешно обновлено!");
    }

    public function getSubadminInfo ($subadmin_id){
        $ar = array();
        $ar['title'] = "Информация о subadmin";
        $ar['parent_id'] = $subadmin_id;
        $ar['subadmins'] = User::where("user_type", 3)
                            ->where('parent_id', $subadmin_id)
                            ->get();
        $ar['now_user'] = User::where('id', $subadmin_id)->get();
        return view("manage.users.subadmin_info", $ar);
    }

    public function ajax_all_subadmin_users ($type, $parent_id){

        $users = User::select('login', 'name', 'surname', 'user_type', 'id')->where("parent_id", $parent_id)->where('user_type', 2)->where('is_active', $type)->get()->toArray();
        $output = array(
            'data' => $users
        );
        return response()->json($output);
    }

}
