<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MappAuto;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DateTimeZone;
use Intervention\Image\ImageManager;
use Image;
use App\Http\Controllers\Manage\Graphs_Disc_DrawController;
use App\Graphs_Disc;
use App\Questions_Keirsi;
use App\Questions_Keirsi_Kaz;
use App\Answers_Keirsi;
use App\Answers_Keirsi_Kaz;
use App\Questions_Mapp;
use App\Questions_Mapp_Kaz;
use App\Results_Mapp;
use App\Results_Keirsi;
use App\Questions_Disc;
use App\Questions_Disc_Kaz;
use App\Questions_Disc_En;
use App\User;
use App\Disk_Formula;
use App\Disc_Types;
use App\Results_Disc;
use App\MappJob;
use App\MappJobDetails;
use App\MappTranses;
use App\MappHtmlRes;
use App\MappResPercent;
use Auth;

class UserController extends Controller{
    function getIndex (){
        $ar = array();
        $ar['title'] = 'Центр тестирования';
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        return view("user.index", $ar);
    }
    //START MAPP TEST
    function getMapp (){
        $ar = array();
        $ar['title'] = 'Тестирование MAPP';
        $ar['action'] = action("User\UserController@saveResultMapp");
        $ar['group_number'] = Questions_Mapp::all()
                                            ->groupBy('group_id')
                                            ->count();
        $ar['groups'] = Questions_Mapp::all()->groupBy('group_id')->toArray();

        $mapp_results = Results_Mapp::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if(Auth::user()->mapp == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        if($mapp_results != NULL){
            $already_passed = "1";
        }else{
            $already_passed = "0";
        }
        $mapp_auto = new MappAuto();
        if($mapp_auto->check_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id)){
            $ar['auth'] = "1";
        }else{
            $ar['auth'] = '0';
        }

        $col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A", "4" => "#EE948A", "5" => "#5BD375", "6" => "#57B2C4", "7" => "#EEA966", "8" => "#7BD38E");
        $ar['col'] = $col;
        $ar['passed'] = $already_passed;

        return view("user.mapp.pass", $ar);
    }

    function getMappKaz (){
        $ar = array();
        $ar['title'] = 'MAPP тестті өту';
        $ar['action'] = action("User\UserController@saveResultMapp");
        $ar['group_number'] = Questions_Mapp_Kaz::all()
                                            ->groupBy('group_id')
                                            ->count();
        $ar['groups'] = Questions_Mapp_Kaz::all()->groupBy('group_id')->toArray();
        
        $mapp_results = Results_Mapp::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if(Auth::user()->mapp == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        if($mapp_results != NULL){
            $already_passed = "1";
        }else{
            $already_passed = "0";
        }

        $mapp_auto = new MappAuto();
        if($mapp_auto->check_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id)){
            $ar['auth'] = "1";
        }else{
            $ar['auth'] = '0';
        }

        $col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A", "4" => "#EE948A", "5" => "#5BD375", "6" => "#57B2C4", "7" => "#EEA966", "8" => "#7BD38E");
        $ar['col'] = $col;
        $ar['passed'] = $already_passed;
        
        $col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A", "4" => "#EE948A", "5" => "#5BD375", "6" => "#57B2C4", "7" => "#EEA966", "8" => "#7BD38E");
        $ar['col'] = $col;


        return view("user.mapp.passkaz", $ar);
    }

    function saveResultMapp (Request $r){
        $questions_mapp = Questions_Mapp::all()->toArray();
        $to_ins = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));
        $k = 0;
        $q = 0;
        foreach ($questions_mapp as $key => $value) {
            if($r->exists('pr'.$value['id'])){
                $ans = $r->input('pr'.$value['id']);
                $k++;
            }else{
                $ans = 3;
            }
            $q++;
            $to_ins[] = array("question_id"=>$value['id'], "answer" => $ans, "user_id" => Auth::user()->id, "created_at" => $current);
        }
        if( (($q/3)*2) != $k){
            return redirect('/user/mapp')->with('status', 'Ответьте на все вопросы!');
        } 
        Results_Mapp::insert($to_ins);
        User::where('id', Auth::user()->id)->update(["mapp_finished_date"=>$current]);
        return redirect('user/mapp/results')->with('status', 'Вы успешно завершили тестирование MAPP!');
    }

    function resultsMapp (){
        $ar = array();
        $ar['title'] = 'Результаты тестирования MAPP';
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        $mapp_results = Results_Mapp::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $ar['mapp_results'] = $mapp_results;

        $mapp_auto = new MappAuto();
        if($mapp_auto->check_auth(Auth::user()->mapp_login, Auth::user()->mapp_pass, Auth::user()->id)){
            $ar['auth'] = "1";
        }else{
            $ar['auth'] = '0';
        }

        return view("user.mapp.results", $ar);
    }


    function show_mapp_results_online ($lang){
        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        download:
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();
        if($result_in_base->count() > 0){

            $response = $result_in_base[0]->html;

            if($lang == "kaz"){
                print("<center><h3>".$name." ".$surname." клиенттің MAPP нәтижесі</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }else{
                print("<center><h3>Результаты MAPP клиента ".$name." ".$surname."</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }

            print(' <link href="http://recruit.assessment.com/Content/bootstrap.css" rel="stylesheet"/> <link href="http://recruit.assessment.com/Content/site.css" rel="stylesheet"/>');
            
            print('<link rel="stylesheet" href="https://rawgit.com/tpreusse/radar-chart-d3/master/src/radar-chart.css">
            <style>
                .radar-chart .area {
                    fill-opacity: 0.7;
                }

                .radar-chart.focus .area {
                    fill-opacity: 0.3;
                }

                .radar-chart.focus .area.focused {
                    fill-opacity: 0.9;
                }

                .inner_content {
                    max-width: 900px;
                    margin: auto auto;
                }
            </style>');
            
            print("<style>
                        table{
                            width:50% !important;
                            margin-left:auto; 
                            margin-right:auto;
                        }
                    </style>
            ");
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            /*
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=Report.doc");*/
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kaz"){
                    $det2 = $detail->detail_name_kaz;   
                }

                $out[1][0] = preg_replace("~".$det1."~", $det2, $out[1][0]);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kaz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $out[2][0] = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $out[2][0]);
                }

                $out[1][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[1][0]);
                $out[1][0] = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $out[1][0]);

                $out[2][0] = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
            
            }

            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);
            $out[2][0] = preg_replace('/<a name="VOC_18"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>/', "", $out[2][0]);

            //Вставка видео-инструкций если язык русский
            if($lang == "ru"){
            $out[1][0] = preg_replace('/<a name="WTC_1"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/dxDwNDMiUmU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_4"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/sDFHpVHSGS0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_5"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/NbKwwLtjVGc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_8"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/uFIJv8_1dbQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>СОЦИАЛЬНЫЕ РОЛИ, РОЛЬ В КОМАНДЕ<\/h3><\/td>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/9X1DI-ZBgLI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br><div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="90%"><h3>СОЦИАЛЬНЫЕ РОЛИ, РОЛЬ В КОМАНДЕ</h3></td>', $out[1][0]);

            $out[1][0] = preg_replace('/<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>МАТЕМАТИЧЕСКИЙ ПОТЕНЦИАЛ<\/h3><\/td>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/8YNF33v31OI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br><div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="90%"><h3>МАТЕМАТИЧЕСКИЙ ПОТЕНЦИАЛ</h3></td>', $out[1][0]);
            }

            //Вставка видео-инструкций если язык казахский

            if($lang == "kaz"){
            $out[1][0] = preg_replace('/<a name="WTC_1"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/R8ugfsgg7cI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_4"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/FGCEnZLpLHM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_5"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/RD6v26OhItU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<a name="WTC_8"><\/a>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/HzETcc0ftW0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>', $out[1][0]);

            $out[1][0] = preg_replace('/<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>ӘЛЕУМЕТТІК ЖӘНЕ КОМАНДАЛЫҚ РӨЛДЕР<\/h3><\/td>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/syh9ifEHeUU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br><div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="90%"><h3>ӘЛЕУМЕТТІК ЖӘНЕ КОМАНДАЛЫҚ РӨЛДЕР</h3></td>', $out[1][0]);

            $out[1][0] = preg_replace('/<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>МАТЕМАТИКАЛЫҚ ҚАБІЛЕТ<\/h3><\/td>/', '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/CxsRZH6OLbY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br><div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="90%"><h3>МАТЕМАТИКАЛЫҚ ҚАБІЛЕТ</h3></td>', $out[1][0]);
            }

            preg_match_all('/<td>(.*?)<\/td>[\t\n\r\s]+<td>(\d{1,3})<\/td>[\t\n\r\s]+<td>(\d)<\/td>/', $out[2][0], $all_jobs);

            array_multisort($all_jobs[2], SORT_DESC, $all_jobs[1], $all_jobs[3]);

            $parts_explanations = new MappHtmlRes();
            if($lang == "kaz"){
                print($parts_explanations->MAPP_1_PART_KZ);
            }else{
                print($parts_explanations->MAPP_1_PART);
            }
            print($out[1][0]);
            
            print("<hr>");

            if($lang == "kaz"){
                print($parts_explanations->MAPP_2_PART_KZ);
            }else{
                print($parts_explanations->MAPP_2_PART);
            }
            print($out[2][0]);
 
            print("<hr>");
                
            if($lang == "kaz"){
                print('<center><b>'.$parts_explanations->MAPP_4_PART_KZ.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Мамандықтардың түрi</b></td>
                        <td><b>Мотивациялық пайыз</b></td>
                        <td><b>Мотивациялық  деңгей</b></td>
                        <td><b>Потенциалды арттыру курстары</b></td>
                    </tr>');
            }else{
                print('<center><b>'.$parts_explanations->MAPP_4_PART.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Виды деятельностей</b></td>
                        <td><b>Процент мотивации</b></td>
                        <td><b>Уровень мотивации</b></td>
                        <td><b>Курсы для развития потенциала</b></td>
                    </tr>');
            }

            
            for($i = 0; $i < 20; $i++){
                if($lang == "ru"){
                    $kosymwa = MappTranses::where('rus', $all_jobs[1][$i])->get();
                }else{
                    $kosymwa = MappTranses::where('kaz', $all_jobs[1][$i])->get();
                }
                print("<tr>");
                    print("<td>".$all_jobs[1][$i]."</td>");
                    print("<td>".$all_jobs[2][$i]."</td>");
                    print("<td>".$all_jobs[3][$i]."</td>");
                    print("<td>");
                    $kos = explode(",", $kosymwa[0]->kosymwa);
                    foreach ($kos as $key => $value) {
                        print($value)."<br>";
                    }
                    print("</td>");
                print("</tr>");
            }
            print('</table>');
            
            print('<script src="http://recruit.assessment.com//Scripts/jquery-1.10.2.js"></script>
            <script src="http://recruit.assessment.com//Scripts/jquery-3.1.1.js"></script>

            <script src="http://recruit.assessment.com//Scripts/bootstrap.js"></script>
            <script src="http://recruit.assessment.com//Scripts/respond.js"></script>

    
            <script src="//d3js.org/d3.v3.min.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChart.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChartCreation.js"></script>
            <script>
                function CreateNewRadarGraph(graphData, graphName) {
                    var data = FormatGraphData(graphData);
                    RadarChart.draw(graphName, data, maxValue = 5, minValue = 0, levels = 5);
                }</script>');
                print($scripts[0]);
            
            get_job_auth:
            $result_in_base2 = MappResPercent::where("user_id", $user_id)
                                        ->where("type", "j")
                                        ->get();
            if($result_in_base2->count() > 0){/*
                print("<hr>");
                if($lang == "kaz"){
                    print($parts_explanations->MAPP_3_PART_KZ);
                }else{
                    print($parts_explanations->MAPP_3_PART);
                }
                print('<table class="table table-striped table-bordered table-responsive">');
                print("<tr>");
                if($lang == "kaz"){
                    print("<td><b>Мамандықтың атауы</b></td>");
                    print("<td><b>Пайыз</b></td>");
                    print("<td><b>Қазақстанның университеттері</b></td>");
                }else{
                    print("<td><b>Название профессии</b></td>");
                    print("<td><b>Процент</b></td>");
                    print("<td><b>Университеты Казахстана</b></td>");
                }
                print("</tr>");
                 foreach($result_in_base2 as $job){
                    if($job->job['not_in_kz']!=1){
                        print("<tr>");
                            if($lang == "ru")
                                print("<td><b>".$job->job['job_name_rus']."</b><br>".$job->job['job_desc_rus']."</td>");
                            if($lang == "kaz")
                                print("<td><b>".$job->job['job_name_kaz']."</b><br>".$job->job['job_desc_kaz']."</td>");
                            print("<td>".$job['percent']."%</td>");
                            //print("<td>".$job->job['univers']."</td>");
                            if($lang == "kaz"){
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Университеттердің тізімі</a></td>");
                            }else{
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Список университетов</a></td>");
                            }
                        print("</tr>");
                    }
                 }
                 print("</table>");
                 */
            }else if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
        
                $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/SortedMAPPMatchToONET';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)<\/a><\/h3>[\t\n\r\s]+<p>(.*?)<\/p>/', $response, $out);
                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)%<\/a><\/h3>/', $response, $out2);

                $to_ins = array();

                for($i=0; $i<20; $i++){
                    //$to_ins[] = array("sys_id" => $out[1][$i], "job_name" => $out[2][$i], "job_desc" => $out[3][$i]);
                    $sys_id = $out[1][$i];

                    $job_data = MappJob::where("sys_id", $sys_id)->get();

                    $job_percent = $out2[2][$i];
                    $job_rel_id = $job_data[0]->id;

                    $to_ins[] = array(
                        "type" => "j",
                        "rel_id" => $job_rel_id,
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $job_percent
                    );
                    $mapp = new MappAuto();
                    $mapp->getJob($sys_id, $user_id);
                }
                MappResPercent::insert($to_ins);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto get_job_auth;
            }
        }else{
            if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
            
                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                
                MappHtmlRes::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download;
            }
        }

    }
    //END MAPP TEST

    //START KEIRSI TEST
    function getKeirsi (){
        $ar = array();
        $ar['title'] = 'Тестирование КЕЙРСИ';
        $ar['action'] = action("User\UserController@saveResultKeirsi");
        $ar['questions'] = Questions_Keirsi::all();

        if(Auth::user()->keirsi == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $keirsi_results = Results_Keirsi::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if($keirsi_results != NULL){
            $ar['passed'] = "1";
        }else{
            $ar['passed'] = "0";
        }

        $col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A", "4" => "#EE948A", "5" => "#5BD375", "6" => "#57B2C4", "7" => "#EEA966", "8" => "#7BD38E");
        $ar['col'] = $col;

        return view("user.keirsi.pass", $ar);
    }
    function getKeirsiKaz (){
        $ar = array();
        $ar['title'] = 'КЕЙРСИ тесті';
        $ar['action'] = action("User\UserController@saveResultKeirsi");
        $ar['questions'] = Questions_Keirsi_Kaz::all();

        if(Auth::user()->keirsi == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $keirsi_results = Results_Keirsi::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if($keirsi_results != NULL){
            $ar['passed'] = "1";
        }else{
            $ar['passed'] = "0";
        }

        $col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A", "4" => "#EE948A", "5" => "#5BD375", "6" => "#57B2C4", "7" => "#EEA966", "8" => "#7BD38E");
        $ar['col'] = $col;
        
        return view("user.keirsi.passkaz", $ar);
    }

    function saveResultKeirsi (Request $r){

        $to_ins = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));

        for($i=1; $i<=70; $i++){
             $to_ins[] = array("quest_id"=>$i, "answer" => $r->input('ans_'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
        }

        Results_Keirsi::insert($to_ins);
        User::where('id', Auth::user()->id)->update(["keirsi_finished_date"=>$current]);
        return redirect('user/keirsi/results')->with('status', 'Вы успешно завершили тестирование КЕЙРСИ!');
    }

    function resultsKeirsi (){
        $ar['title'] = "Результаты тестирования КЕЙРСИ";

        $keirsi_results = Results_Keirsi::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        $ar['keirsi_results'] = $keirsi_results;

        return view("user.keirsi.results", $ar);
    }

    function resultsKeirsiByDate ($date){
        $ar['title'] = "Детализация результатов КЕЙРСИ";
        $ar['name'] = Auth::user()->name;
        $ar['surname'] = Auth::user()->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Keirsi::where("user_id", Auth::user()->id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        $b = array();
        $k = 1;
        foreach($results_by_date as $key=>$value){
            if($value->answer == "a") $a[$k] = 1; else $a[$k] = 0;
            if($value->answer == "b") $b[$k] = 1; else $b[$k] = 0;
            $k++;
        }

        $sum_E = 0; $sum_I = 0;
        for($i=1; $i<=70; $i+=7){ $sum_E += $a[$i]; $sum_I += $b[$i]; }

        $sum_S = 0; $sum_N = 0;
        for($i=2; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }
        for($i=3; $i<=70; $i+=7){ $sum_S += $a[$i]; $sum_N += $b[$i]; }

        $sum_T = 0; $sum_F = 0;
        for($i=4; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }
        for($i=5; $i<=70; $i+=7){ $sum_T += $a[$i]; $sum_F += $b[$i]; }

        $sum_J = 0; $sum_P = 0;
        for($i=6; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }
        for($i=7; $i<=70; $i+=7){ $sum_J += $a[$i]; $sum_P += $b[$i]; }

        $ar['E'] = $sum_E; $ar['I'] = $sum_I; $ar['S'] = $sum_S; $ar['N'] = $sum_N;
        $ar['T'] = $sum_T; $ar['F'] = $sum_F; $ar['J'] = $sum_J; $ar['P'] = $sum_P;

        if($sum_E < $sum_I) $first = "I"; else $first = "E";
        if($sum_S < $sum_N) $second = "N"; else $second = "S";
        if($sum_T < $sum_F) $third = "F"; else $third = "T";
        if($sum_J < $sum_P) $fourth = "P"; else $fourth = "J";

        $ar["formula"] = $first.$second.$third.$fourth;

        return view("user.keirsi.resultsbydate", $ar);
    }

    //END OF KEIRSI TEST

    //START DISC TEST
    function getDisc (){
        $ar = array();
        $ar['title'] = 'Тестирование DISC';
        $ar['action'] = action("User\UserController@saveResultDisc");
        $ar['questions'] = Questions_Disc::all();
        $ar['user'] = User::where('id', Auth::user()->id)->get();

        if(Auth::user()->disc >= 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        $group = 1;
        for($i = 1; $i <= 96; $i++){
            $group_ar[$i] = $group;
            if($i % 4 == 0){
                $group++;
            }
        }

        $ar['group_ar'] = $group_ar;

        return view("user.disc.pass", $ar);
    }

    function getDiscEn (){
        $ar = array();
        $ar['title'] = 'DISC TEST';
        $ar['action'] = action("User\UserController@saveResultDisc");
        $ar['questions'] = Questions_Disc_En::all();
        $ar['user'] = User::where('id', Auth::user()->id)->get();

        if(Auth::user()->disc >= 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
         if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        $group = 1;
        for($i = 1; $i <= 96; $i++){
            $group_ar[$i] = $group;
            if($i % 4 == 0){
                $group++;
            }
        }

        $ar['group_ar'] = $group_ar;

        return view("user.disc.pass", $ar);
    }

    function getDiscKaz (){
        $ar = array();
        $ar['title'] = 'DISC тесті';
        $ar['action'] = action("User\UserController@saveResultDisc");
        $ar['questions'] = Questions_Disc_Kaz::all();
        $ar['user'] = User::where('id', Auth::user()->id)->get();

        if(Auth::user()->disc >= 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

                $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        $group = 1;
        for($i = 1; $i <= 96; $i++){
            $group_ar[$i] = $group;
            if($i % 4 == 0){
                $group++;
            }
        }

        $ar['group_ar'] = $group_ar;

        return view("user.disc.passkaz", $ar);
    }

    function saveResultDisc (Request $r){

        $to_ins = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));
        $messages = [
            'required' => 'Поле :attribute необходимо заполнить',
        ];

        for($i = 1; $i <= 24; $i++){

            $validator = Validator::make($r->all(), [
                'most'.$i => 'required',
                'least'.$i => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

            $to_ins[] = array("most"=>$r->input('most'.$i), "least" => $r->input('least'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
        }

        Results_Disc::insert($to_ins);
        User::where('id', Auth::user()->id)->update(["disc_finished_date"=>$current]);
        return redirect('user/disc/results')->with('status', 'Ваши результаты по тесту DISC успешно сохранены!');
    }

    function resultsDisc (){
        $ar['title'] = "Результаты тестирования DISC";

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $ar['disc_results'] = $disc_results;

        return view("user.disc.results", $ar);
    }

    function resultsDiscByDate ($date, Request $r, $lang){
        $id = Auth::id();
        $ar = array();
        $ar['title'] = 'Страница результатов';
        $user_info = User::findOrFail(Auth::id());
        $ar['username'] = $user_info['name']." ".$user_info['surname'];
        $ar['date'] = $date;
        $ar['user_id'] = $id;

        $results_by_date = Results_Disc::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $most = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $least = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        $change = array("D" => 0, "I" => 0, "S" => 0, "C" => 0, "*" => 0);
        foreach ($results_by_date as $key => $value) {
            switch($value->get_most->most){
                case "D":$most["D"]++;break;
                case "I":$most["I"]++;break;
                case "S":$most["S"]++;break;
                case "C":$most["C"]++;break;
                case "*":$most["*"]++;break;
            }
            switch($value->get_least->least) {
                case "D":$least["D"]++;break;
                case "I":$least["I"]++;break;
                case "S":$least["S"]++;break;
                case "C":$least["C"]++;break;
                case "*":$least["*"]++;break;
            }
        }
        $most['total'] = $most['D']+$most['I']+$most['S']+$most['C']+$most['*'];
        $least['total'] = $least['D']+$least['I']+$least['S']+$least['C']+$least['*'];
        $change['D'] = $most['D']-$least['D'];
        $change['I'] = $most['I']-$least['I'];
        $change['S'] = $most['S']-$least['S'];
        $change['C'] = $most['C']-$least['C'];
        $ar['most'] = $most;
        $ar['least'] = $least;


        $D_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "D")->get()->toArray();
        $I_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "I")->get()->toArray();
        $S_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "S")->get()->toArray();
        $C_numbers_most = Graphs_Disc::where("type", "most")->where("letter", "C")->get()->toArray();

        $D_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "D")->get()->toArray();
        $I_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "I")->get()->toArray();
        $S_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "S")->get()->toArray();
        $C_numbers_least = Graphs_Disc::where("type", "least")->where("letter", "C")->get()->toArray();

        $D_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "D")->get()->toArray();
        $I_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "I")->get()->toArray();
        $S_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "S")->get()->toArray();
        $C_numbers_change = Graphs_Disc::where("type", "change")->where("letter", "C")->get()->toArray();

        $img_most_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_most, $I_numbers_most, $S_numbers_most, $C_numbers_most, $most["D"], $most["I"], $most["S"], $most["C"]);
        $img_least_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_least, $I_numbers_least, $S_numbers_least, $C_numbers_least, $least["D"], $least["I"], $least["S"], $least["C"]);
        $img_change_graph = Graphs_Disc_DrawController::draw_graph($D_numbers_change, $I_numbers_change, $S_numbers_change, $C_numbers_change, $change["D"], $change["I"], $change["S"], $change["C"]);

        $ar['img_most_graph'] = $img_most_graph['image']->encode('data-url');
        $ar['img_least_graph'] = $img_least_graph['image']->encode('data-url');
        $ar['img_change_graph'] = $img_change_graph['image']->encode('data-url');

        $ar['top_most_letters'] = $img_most_graph['top_letters'];
        $ar['top_least_letters'] = $img_least_graph['top_letters'];
        $ar['top_change_letters'] = $img_change_graph['top_letters'];

        //Зеркало формула и описание
        $formula = "";
        $pred = 1000;
        foreach($img_change_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula .= "=";
            }
            if($value > $pred){
                $formula .= ".";
            }
            $formula .= $letter;
            $pred = $value;
        }
        $ar['change_formula'] = $formula;
        $type_id_q = Disk_Formula::where("formula", $formula)->first();
        $type_id = $type_id_q['type_id'];
        $ar['description'] = Disc_Types::where('id',$type_id)->get();

        $top_prof = $formula[0];
        if(isset($formula[2])){
            $top_prof .= $formula[2];
        }
        $ar['top_prof'] = $top_prof;

        //Маска - формула и описание

        $formula_mask = "";
        $pred = 1000;
        foreach($img_most_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_mask .= "=";
            }
            if($value > $pred){
                $formula_mask .= ".";
            }
            $formula_mask .= $letter;
            $pred = $value;
        }
        $ar['most_formula'] = $formula_mask;
        $type_id_q_most = Disk_Formula::where("formula", $formula_mask)->first();
        $type_id_mask = $type_id_q_most['type_id'];
        $ar['description_mask'] = Disc_Types::where('id',$type_id_mask)->get();

        //Сердце

        $formula_heart = "";
        $pred = 1000;
        foreach($img_least_graph['top_letters'] as $letter=>$value){
            if($value==$pred){
                $formula_heart .= "=";
            }
            if($value > $pred){
                $formula_heart .= ".";
            }
            $formula_heart .= $letter;
            $pred = $value;
        }
        $ar['least_formula'] = $formula_heart;
        $type_id_q_least = Disk_Formula::where("formula", $formula_heart)->first();
        $type_id_least = $type_id_q_least['type_id'];
        $ar['description_least'] = Disc_Types::where('id',$type_id_least)->get();

        $ar['lang'] = $lang;
        return view("user.disc.resultsbydate", $ar);
    }
    //END OF DISC TEST
}
