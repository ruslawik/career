<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MappAuto;
use Illuminate\Http\Response;
use App\MappJob;
use App\MappJobDetails;
use App\MappTranses;
use App\MappHtmlRes;
use App\MappResPercent;
use App\User;
use Auth;

class MappResults extends Controller
{
    function show_mapp_results_small ($lang){
        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        download:
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();
        if($result_in_base->count() > 0){

            $response = $result_in_base[0]->html;

            if($lang == "kaz"){
                print("<center><h3>".$name." ".$surname." клиенттің MAPP нәтижесі</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }else{
                print("<center><h3>Результаты MAPP клиента ".$name." ".$surname."</h3><a href='kaz'>КАЗ</a> | <a href='ru'>РУС</a></center>");
            }

            print(' <link href="http://recruit.assessment.com/Content/bootstrap.css" rel="stylesheet"/> <link href="http://recruit.assessment.com/Content/site.css" rel="stylesheet"/>');
            
            print('<link rel="stylesheet" href="https://rawgit.com/tpreusse/radar-chart-d3/master/src/radar-chart.css">
            <style>
                .radar-chart .area {
                    fill-opacity: 0.7;
                }

                .radar-chart.focus .area {
                    fill-opacity: 0.3;
                }

                .radar-chart.focus .area.focused {
                    fill-opacity: 0.9;
                }

                .inner_content {
                    max-width: 900px;
                    margin: auto auto;
                }
            </style>');
            
            print("<style>
                        table{
                            width:50% !important;
                            margin-left:auto; 
                            margin-right:auto;
                        }
                    </style>
            ");
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            /*
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=Report.doc");*/
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kaz"){
                    $det2 = $detail->detail_name_kaz;   
                }

                $out[1][0] = preg_replace("~".$det1."~", $det2, $out[1][0]);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kaz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $out[2][0] = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $out[2][0]);
                }

                $out[1][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[1][0]);
                $out[1][0] = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $out[1][0]);

                $out[2][0] = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
            
            }

            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);

            preg_match_all('/<td>(.*?)<\/td>[\t\n\r\s]+<td>(\d{1,3})<\/td>[\t\n\r\s]+<td>(\d)<\/td>/', $out[2][0], $all_jobs);

            $out[2][0] = preg_replace('/<a name="VOC_1"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>[\t\n\r\s]+<div class="row">[\t\n\r\s\w\W\S]+<\/div>[\t\n\r\s]+<\/div>/', "", $out[2][0]);

            array_multisort($all_jobs[2], SORT_DESC, $all_jobs[1], $all_jobs[3]);

            $parts_explanations = new MappHtmlRes();
            /*
            if($lang == "kaz"){
                print($parts_explanations->MAPP_1_PART_KZ);
            }else{
                print($parts_explanations->MAPP_1_PART);
            }
            print($out[1][0]);
            
            print("<hr>");
            */
            if($lang == "kaz"){
                print($parts_explanations->MAPP_2_PART_KZ);
            }else{
                print($parts_explanations->MAPP_2_PART);
            }
            print($out[2][0]);
 
            print("<hr>");
                
            if($lang == "kaz"){
                print('<center><b>'.$parts_explanations->MAPP_4_PART_KZ.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Мамандықтардың түрi</b></td>
                        <td><b>Мотивациялық пайыз</b></td>
                        <td><b>Мотивациялық  деңгей</b></td>
                        <td><b>Потенциалды арттыру курстары</b></td>
                    </tr>');
            }else{
                print('<center><b>'.$parts_explanations->MAPP_4_PART.'</b></center><br><table class="table table-striped table-bordered table-responsive">
                    <tr>
                        <td><b>Виды деятельностей</b></td>
                        <td><b>Процент мотивации</b></td>
                        <td><b>Уровень мотивации</b></td>
                        <td><b>Курсы для развития потенциала</b></td>
                    </tr>');
            }

            
            for($i = 0; $i < 20; $i++){
                if($lang == "ru"){
                    $kosymwa = MappTranses::where('rus', $all_jobs[1][$i])->get();
                }else{
                    $kosymwa = MappTranses::where('kaz', $all_jobs[1][$i])->get();
                }
                print("<tr>");
                    print("<td>".$all_jobs[1][$i]."</td>");
                    print("<td>".$all_jobs[2][$i]."</td>");
                    print("<td>".$all_jobs[3][$i]."</td>");
                    print("<td>");
                    $kos = explode(",", $kosymwa[0]->kosymwa);
                    foreach ($kos as $key => $value) {
                        print($value)."<br>";
                    }
                    print("</td>");
                print("</tr>");
            }
            print('</table>');


            
            print('<script src="http://recruit.assessment.com//Scripts/jquery-1.10.2.js"></script>
            <script src="http://recruit.assessment.com//Scripts/jquery-3.1.1.js"></script>

            <script src="http://recruit.assessment.com//Scripts/bootstrap.js"></script>
            <script src="http://recruit.assessment.com//Scripts/respond.js"></script>

    
            <script src="//d3js.org/d3.v3.min.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChart.js"></script>
            <script src="http://recruit.assessment.com//Scripts/RadarChartCreation.js"></script>
            <script>
                function CreateNewRadarGraph(graphData, graphName) {
                    var data = FormatGraphData(graphData);
                    RadarChart.draw(graphName, data, maxValue = 5, minValue = 0, levels = 5);
                }</script>');
                print($scripts[0]);
            
            get_job_auth:
            $result_in_base2 = MappResPercent::where("user_id", $user_id)
                                        ->where("type", "j")
                                        ->get();
            if($result_in_base2->count() > 0){/*
                print("<hr>");
                if($lang == "kaz"){
                    print($parts_explanations->MAPP_3_PART_KZ);
                }else{
                    print($parts_explanations->MAPP_3_PART);
                }
                print('<table class="table table-striped table-bordered table-responsive">');
                print("<tr>");
                if($lang == "kaz"){
                    print("<td><b>Мамандықтың атауы</b></td>");
                    print("<td><b>Пайыз</b></td>");
                    print("<td><b>Қазақстанның университеттері</b></td>");
                }else{
                    print("<td><b>Название профессии</b></td>");
                    print("<td><b>Процент</b></td>");
                    print("<td><b>Университеты Казахстана</b></td>");
                }
                print("</tr>");
                 foreach($result_in_base2 as $job){
                    if($job->job['not_in_kz']!=1){
                        print("<tr>");
                            if($lang == "ru")
                                print("<td><b>".$job->job['job_name_rus']."</b><br>".$job->job['job_desc_rus']."</td>");
                            if($lang == "kaz")
                                print("<td><b>".$job->job['job_name_kaz']."</b><br>".$job->job['job_desc_kaz']."</td>");
                            print("<td>".$job['percent']."%</td>");
                            //print("<td>".$job->job['univers']."</td>");
                            if($lang == "kaz"){
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Университеттердің тізімі</a></td>");
                            }else{
                                print("<td><a target='_blank' href='/job/".$job->job['id']."'>Список университетов</a></td>");
                            }
                        print("</tr>");
                    }
                 }
                 print("</table>");
                 */
            }else if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
        
                $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/SortedMAPPMatchToONET';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)<\/a><\/h3>[\t\n\r\s]+<p>(.*?)<\/p>/', $response, $out);
                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)%<\/a><\/h3>/', $response, $out2);

                $to_ins = array();

                for($i=0; $i<20; $i++){
                    //$to_ins[] = array("sys_id" => $out[1][$i], "job_name" => $out[2][$i], "job_desc" => $out[3][$i]);
                    $sys_id = $out[1][$i];

                    $job_data = MappJob::where("sys_id", $sys_id)->get();

                    $job_percent = $out2[2][$i];
                    $job_rel_id = $job_data[0]->id;

                    $to_ins[] = array(
                        "type" => "j",
                        "rel_id" => $job_rel_id,
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $job_percent
                    );
                    $mapp = new MappAuto();
                    $mapp->getJob($sys_id, $user_id);
                }
                MappResPercent::insert($to_ins);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto get_job_auth;
            }
        }else{
            if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
            
                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                
                MappHtmlRes::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download;
            }
        }

    }
}
