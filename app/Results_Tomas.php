<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results_Tomas extends Model
{
    protected $table = "results_tomas";

     function quest_text (){
    	return $this->hasOne("App\Questions_Tomas", 'id', 'quest_id');
    }

}
