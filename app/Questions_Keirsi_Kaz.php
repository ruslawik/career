<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answers_Keirsi_Kaz;

class Questions_Keirsi_Kaz extends Model
{
    protected $table = 'questions_keirsi_kaz';

    public function answer_a()
    {
        return $this->hasOne('App\Answers_Keirsi_Kaz', 'question_id')->where('type', 'a');
    }

    public function answer_b()
    {
        return $this->hasOne('App\Answers_Keirsi_Kaz', 'question_id')->where('type', 'b');
    }
}