<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Questions_Disc;

class Results_Disc extends Model
{
    protected $table = 'results_disc';
    
    function get_most (){
        return $this->hasOne('App\Questions_Disc', 'id', 'most');
    }

    function get_least (){
        return $this->hasOne('App\Questions_Disc', 'id', 'least');
    }
}