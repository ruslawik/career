<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappHtmlRes extends Model
{
    protected $table = 'mapp_res_html';

    public $MAPP_1_PART = "<table style='text-align:justify;'><tr><td><center>Отчет теста МАРР состоит из 4 частей</center><br>

<center><b>1 часть - Характеристика работника</b></center><br>
<center>
<iframe width='560' height='315' src='https://www.youtube.com/embed/kG7umeAvpQM' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>
Здесь вы увидите свой уровень мотивации к работе по 7 факторам.<br> В каждом из этих факторов есть подфакторы или черты (критерии работы), определяющие наиболее подходящие для вас. 
						   <br>1 означает - самая высокая мотивация, самый подходящий вариант
						   	<br>2 означает - выше среднего
							<br>3 означает - средний уровень мотивации
							<br>4 означает - ниже среднего
							<br>5 означает - самый низкий уровень мотивации
							<br>Вы можете сосредоточиться на 1 и 2 и указать, что 4 и 5 - это задачи, которые вы предпочитаете избегать, и часто даже являются прямыми противоположностями ваших главных черт. Если у вас нет 1 или 2, вы можете сосредоточиться на 3.
							<br>Люди с большим количеством единиц обычно имеют много пятерок. Это может облегчить поиск работы для человека, потому что они могут точно знать, что они хотят. Тем не менее, это также может быть сложной задачей, потому что они могут быть не такими гибкими, если им приходится выполнять задачи, которые им не нравятся. Люди с большим количеством 2 и 3 немного более гибки в том, что они предпочитают делать. Это может быть сложной задачей, потому что может быть трудно точно определить подходящую работу. Это также может быть полезно, когда индивидуума просят выполнить несколько различных задач, и он не имеет сильных предпочтений в пользу или против задачи.
							</td></tr></table><br><center><iframe width='560' height='315' src='https://www.youtube.com/embed/Eaawdfio0H8' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>";

	public $MAPP_2_PART = "<table style='text-align:justify;'><tr><td><center><b>2 часть - Основные профессиональные сферы</b></center><br>
	<center><iframe width='560' height='315' src='https://www.youtube.com/embed/IHwVqPtrdjY' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>

После того, как вы ознакомились с другими разделами, и у вас есть общее представление о чертах, которые для вас наиболее важны в вашей работе, вы можете посмотреть на предложенные основные области работы. Этот список основан на главных ваших чертах и рабочих местах, которые включают работу с этими чертами.
Также как в 1 части вы увидите напротив каждой сферы деятельности нумерацию от 1 до 5. Во втором столбце вы увидите двузначные числа до 100 или 100 (самый высокий уровень). Числовые данные 2 столбца означают процентное соотношение насколько вы сами подходите к той или иной работе.
</td></tr></table><br>";

	public $MAPP_3_PART = "<table style='text-align:justify;'><tr><td><center><b>4 часть - Список ONE-net профессий</b></center><br>

В этом разделе представлен список подходящих профессий и, соответственно, университеты Казахстана, где можно обучиться этим профессиям.
<br>
Данный список может отличаться от 3 части отчета, так как 4 часть показывает ваш потенциал, а 3 часть показывает наивысший уровень мотивации в видах деятельности.
</td></tr></table><br>";

public $MAPP_4_PART = "3 часть – Топ-20 сфер деятельности".'<br><br><center><iframe width="560" height="315" src="https://www.youtube.com/embed/DVmiw9itUyg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center><br>';

public $MAPP_1_PART_KZ = "<table style='text-align:justify;'><tr><td><center>МАРР тесттің нәтижесі 4 бөлімнен тұрады</center><br>

<center><b>1 бөлім – Жұмыскердің сипаттамасы</b></center><br>
<center>
<iframe width='560' height='315' src='https://www.youtube.com/embed/Q9RTnBkcqno' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>
Бұл жерде сіз жұмысқа деген ынта көрсеткішін 7 факторға байланысты көре аласыз. <br>Бұл жетеуі өз ішінде бірнеше кішігірім факторларды қамтиды. Олар ең әсерлі факторларды анықтауға септігін тигізеді. 
						   <br>1 - ең қатты әсер ететін мотивация, ең сәйкес нұсқа
						   	<br>2 - орташадан жоғары
							<br>3 - орташа ынта
							<br>4 - орташадан төмен
							<br>5 - ынтаның ең төменгі дәрежесі дегенді білдіреді
							<br>Сіз 1 мен 2-ні таңдап, 4 пен 5-тің қабілеттеріңізге қайшы келетінін айтуыңыз мүмкін. Тіпті олармен бетпе-бет келмес үшін айналып өтесіз. Егер, 1 мен 2 болмаған жағдайда, 3-ке көңіл бөлсеңіз болады.  
 Әдетте, адамдарда 1 мен 5 қатар жүреді. Яғни, олар не қалайтынын жақсы білгендіктен жұмыс іздеу барысы оңайға соғады, бірақ кей жұмыстар оларға ұнамай қалса, олардың ұнамайтын жұмысқа икемделуі қиындық тудырады. Ал, 2 мен 3-тер қай жұмысты көбірек ұнататынын білмесе де, жұмысты бітіру мәселесінде икемдірек келеді. Яғни, бірнеше тапсырма берілгенде, олар тапсырманың ұнап- ұнамағандығынан гөрі тапсырманы бітіруге көбірек көңіл бөледі. 
							</td></tr></table><br>";

	public $MAPP_2_PART_KZ = "<table style='text-align:justify;'><tr><td><center><b>2 бөлім -  Негізгі кәсіби сфералар</b></center><br>
	<center><iframe width='560' height='315' src='https://www.youtube.com/embed/Wr2DMXMDLVs' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>
Басқа бөлімдермен танысып, әр жұмысқа қажетті қабілеттер жайлы мәлімет алғаннан кейін, сізге керек болатын негізгі жұмыс алаңдары ұсынылады. Бұл тізім мамандық түрі мен жұмыскердің қабілеттерінін қаншалықты сәйкес келетінін көрсетеді.<br>
Сонымен қатар 1-ші бағанда 100-ге дейінгі сандар тізілген. Бұл сандар пайыздық өлшемде әрбір жұмысқа қатысты сіздің икемділігіңізді көрсетеді. Одан кейінгі бағанда жұмыс сфераларына қарама-қарсы 1-ден 5-ке дейін нөмірлерді көресіз. 
</td></tr></table><br>";
	
	public $MAPP_3_PART_KZ = "<table style='text-align:justify;'><tr><td><center><b>4 бөлім - ONE-net мамандықтар тізімі</b></center><br>
Бұл бөлімде Қазақстандағы мамандықтар мен соған сәйкес ЖОО-ның тізімі көрсетілген.
</td></tr></table><br>";

	public $MAPP_4_PART_KZ = "3 бөлім – Топ-20 кәсіби мамандық түрлері".'<br><br><center><iframe width="560" height="315" src="https://www.youtube.com/embed/_iNZHVbc1Nk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>';

}
