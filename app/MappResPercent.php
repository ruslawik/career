<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MappJob;
use App\MappJobDetails;

class MappResPercent extends Model
{
    protected $table = "mapp_res_percent";


    public function job (){
    	return $this->hasOne('App\MappJob', 'id', 'rel_id');
    }

    public function detail (){
    	return $this->hasOne('App\MappJobDetails', 'id', 'rel_id');
    }
    
}
