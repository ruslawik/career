<?php

Route::get('/', "LoginController@getLogin");
Route::get('/cloudpayments', "LoginController@getCloud");
Route::get('/is_exists', "LoginController@check_login");


Route::get('login', 'LoginController@getLogin');
Route::post('login', 'LoginController@postLogin');

Route::get('regpage', 'LoginController@getRegpage');
Route::post('reg', 'LoginController@postReg');

Route::any('logout', 'LoginController@getLogout');
Route::any('job/{id}', 'MappAuto@getJobUnivers');

Route::get('application', 'LoginController@getApplication');
Route::post('/application_save', 'LoginController@saveApplication');

//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
Route::group(['middleware' => ['auth.admin'], 'prefix' => 'manage'], function () {
	// Main Route
	Route::any("/admin", "Manage\ManageController@getIndex");
	Route::any("show_stat/{test_name}/{from_date}/{to_date}", "Manage\ManageController@getStatByTest");
	Route::any("show_stat_by_pockets/{test_name}/{from_date}/{to_date}", "Manage\ManageController@getStatByPockets");
	Route::any("/subadmin_info/{subadmin_id}", "Manage\ManageController@getSubadminInfo");

	//Tests routes
	//KEIRSI
	Route::any("keirsi_edit", "Manage\ManageController@getKeirsiEdit");
	Route::any("keirsi_edit_kaz", "Manage\ManageController@getKeirsiEditKaz");
	Route::post("save_keirsi", "Manage\ManageController@savePostKeirsi");
	Route::post("save_keirsi_kaz", "Manage\ManageController@savePostKeirsiKaz");
	Route::get("user/{id}/keirsi/{date}", "Manage\ManageController@getKeirsiResultsByDate");
	//MAPP
	Route::any("mapp_edit", "Manage\ManageController@getMappEdit");
	Route::any("mapp_edit_kaz", "Manage\ManageController@getMappEditKaz");
	Route::post("save_mapp", "Manage\ManageController@savePostMapp");
	Route::post("save_mapp_kaz", "Manage\ManageController@savePostMappKaz");
	Route::any("user/{id}/mapp/{date}", "Manage\ManageController@getMappResults");
	Route::post("mapp_edit/save_edited", "Manage\ManageController@saveEditedMapp");	
	Route::post("mapp_edit/save_editedkaz", "Manage\ManageController@saveEditedMappKAZ");
	Route::any("mapp_tercume", "Manage\ManageController@getMappTraslatePage");	
	Route::any("mapp_tercume_all", "Manage\ManageController@getMappAllTraslatePage");
	Route::any("mapp_tercume_jobdesc", "Manage\ManageController@getMappTraslateJobDesc");
	Route::any("mapp_tercume_details", "Manage\ManageController@getMappTraslateJobDetails");
	Route::any("save_mapp_translate", "Manage\ManageController@saveMappTranslate");
	Route::any("save_mapp_translate_all", "Manage\ManageController@saveMappAllTranslate");
	Route::any("save_mapp_translate_jobdesc", "Manage\ManageController@saveMappTranslateJobDesc");
	Route::any("save_mapp_translate_jobdetail", "Manage\ManageController@saveMappTranslateJobDetails");

	Route::any("add_one_translate", "Manage\ManageController@add_one_translate");
	
	//DISC
	Route::any("disc_edit", "Manage\ManageController@getDiscEdit");
	Route::any("disc_edit_kaz", "Manage\ManageController@getDiscEditKaz");
	Route::post("save_disc", "Manage\ManageController@savePostDisc");
	Route::post("save_disc_kaz", "Manage\ManageController@savePostDiscKaz");
	Route::any("disc_edit_en", "Manage\ManageController@getDiscEditEn");
	Route::post("save_disc_en", "Manage\ManageController@savePostDiscEn");
	Route::any("user/{id}/disc/{date}/{lang}", "Manage\ManageController@getDiscResults");
	Route::any("disc_formulas", "Manage\ManageController@getDiscFormulasEdit");
	Route::any("disc/add_formula", 'Manage\ManageController@discAddFormula');
	Route::post("disc/save_formulas", 'Manage\ManageController@saveDiscFormulas');
	Route::get("disc_text_edit/{text_id}/{lang}", "Manage\ManageController@discTextEdit");
	Route::post("save_disc_text", "Manage\ManageController@saveDiscText");

	//HOLL
	Route::any("holl_edit", "HollController@getHollEdit");
	Route::any("holl_edit_kaz", "HollController@getHollKazEdit");
	Route::any("save_holl", "HollController@Save");
	Route::any("save_holl_kaz", "HollController@SaveKaz");
	Route::get("user/{id}/holl/{date}", "HollController@getHollResultsByDate");

	//TOMAS
	Route::any("tomas_edit", "TomasController@getTomasEdit");
	Route::any("tomas_edit_kaz", "TomasController@getTomasKazEdit");
	Route::any("save_tomas", "TomasController@Save");
	Route::any("save_tomas_kaz", "TomasController@SaveKaz");
	Route::get("user/{id}/tomas/{date}", "TomasController@getTomasResultsByDate");

	//Solomin
	Route::any("solomin_edit", "SolominController@getSolominEdit");
	Route::any("solomin_editkaz", "SolominController@getSolominKazEdit");
	Route::any("solomin2_edit", "SolominController@getSolomin2Edit");
	Route::any("solomin2_editkaz", "SolominController@getSolomin2KazEdit");
	Route::any("save_solomin", "SolominController@Save");
	Route::any("save_solominkaz", "SolominController@SaveKaz");
	Route::any("save_solomin2", "SolominController@Save2");
	Route::any("save_solomin2kaz", "SolominController@Save2Kaz");
	Route::get("user/{id}/solomin/{date}", "SolominController@getSolominResultsByDate");

	//Users Routes
	Route::any("add_user", "Manage\ManageController@getAddUser");
	Route::any("add_subadmin", "Manage\ManageController@getAddSubadmin");
	Route::any("all_users", "Manage\ManageController@getAllUsers");
	Route::post("save_new_user", "Manage\ManageController@savePostNewUser");
	Route::post('save_new_subadmin', "Manage\ManageController@savePostNewSubadmin");
	Route::get("user/{id}", "Manage\ManageController@getUserInfo");
	Route::get("ajax_all_users/{type}", "Manage\ManageController@ajax_all_users");
	Route::get("ajax_all_users_subadmin/{type}/{parent_id}", "Manage\ManageController@ajax_all_subadmin_users");
	Route::get("edit_user/{user_id}", "Manage\ManageController@getEditUser");
	Route::get("add_one_more_disc/{user_id}", "Manage\ManageController@add_one_more_disc");
	Route::get("edit_subadmin/{user_id}", "Manage\ManageController@getEditSubadmin");
	Route::post('save_edited_user', "Manage\ManageController@saveEditedUser");
	Route::post('save_edited_subadmin', "Manage\ManageController@saveEditedSubadmin");
	Route::any('download_mapp_results/{user_id}/{lang}', "Manage\ManageController@download_mapp_results");

	//All applications
	Route::any("applications", "Manage\ManageController@allApplications");
	Route::any("app/{id}", "Manage\ManageController@applicationById");
});

Route::group(['middleware' => ['auth.subadmin'], 'prefix' => 'subadmin'], function () {
	Route::any("/main", "Subadmin\SubadminController@getIndex");
	Route::any("show_stat/{test_name}/{from_date}/{to_date}", "Subadmin\SubadminController@getStatByTest");
	Route::any("/subadmin_info/{subadmin_id}", "Subadmin\SubadminController@getSubadminInfo");

	Route::any("/test_amount", "Subadmin\SubadminController@testAmount");
	Route::any("/profile", "Subadmin\SubadminController@getEditProfile");
	Route::post("save_profile", "Subadmin\SubadminController@saveProfile");

	Route::any("add_user", "Subadmin\SubadminController@getAddUser");
	Route::any("add_subadmin", "Subadmin\SubadminController@getAddSubadmin");
	Route::post("save_new_user", "Subadmin\SubadminController@savePostNewUser");
	Route::post('save_new_subadmin', "Subadmin\SubadminController@savePostNewSubadmin");
	Route::get("edit_subadmin/{user_id}", "Subadmin\SubadminController@getEditSubadmin");
	Route::post('save_edited_subadmin', "Subadmin\SubadminController@saveEditedSubadmin");
	Route::any("all_users", "Subadmin\SubadminController@getAllUsers");
	Route::get("ajax_all_users/{type}", "Subadmin\SubadminController@ajax_all_users");
	Route::get("edit_user/{user_id}", "Subadmin\SubadminController@getEditUser");
	Route::post('save_edited_user', "Subadmin\SubadminController@saveEditedUser");
	Route::get("user/{id}", "Subadmin\SubadminController@getUserInfo");
	Route::get("ajax_all_users_subadmin/{type}/{parent_id}", "Subadmin\SubadminController@ajax_all_subadmin_users");

	Route::get("user/{id}/keirsi/{date}", "Subadmin\SubadminController@getKeirsiResultsByDate");
	Route::any("user/{id}/disc/{date}/{lang}", "Subadmin\SubadminController@getDiscResults");
	Route::any("user/{id}/mapp/{date}", "Subadmin\SubadminController@getMappResults");
	Route::get("user/{id}/holl/{date}", "HollController@getHollResultsByDateForSubadmin");
	Route::get("user/{id}/tomas/{date}", "TomasController@getTomasResultsByDateForSubadmin");
	Route::get("user/{id}/solomin/{date}", "SolominController@getSolominResultsByDateForSubadmin");
});

//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {

	//AUTO MAPP
	Route::any("/send_to_mapp_server", "MappAuto@mapp_send");
	Route::any("/download_mapp_results", "MappAuto@getResults");
	Route::any("/show_mapp_results_online/{lang}", "User\UserController@show_mapp_results_online");
	Route::any("/show_mapp_results_small/{lang}", "User\MappResults@show_mapp_results_small");
	Route::any('/mapp/joblist', "MappAuto@getJobList");
	Route::any('/mapp_job/{sys_id}', "MappAuto@showJob");

	//Main Route
	Route::get("/main", "User\UserController@getIndex");
	//Mapp pass page
	Route::get("/mapp", "User\UserController@getMapp");
	Route::get("/mappkaz", "User\UserController@getMappKaz");
	Route::post("/mapp/save", "User\UserController@saveResultMapp");
	Route::get("mapp/results", "User\UserController@resultsMapp");
	//KEIRSI pass page
	Route::get("/keirsi", "User\UserController@getKeirsi");
	Route::get("/keirsikaz", "User\UserController@getKeirsiKaz");
	Route::post("/keirsi/save", "User\UserController@saveResultKeirsi");
	Route::get("/keirsi/results", "User\UserController@resultsKeirsi");
	Route::get("/keirsi/{date}", "User\UserController@resultsKeirsiByDate");
	//DISC
	Route::get("/disc", "User\UserController@getDisc");
	Route::get("/disckaz", "User\UserController@getDiscKaz");
	Route::get("/discen", "User\UserController@getDiscEn");
	Route::post("/disc/save", "User\UserController@saveResultDisc");
	Route::any("/disc/results", "User\UserController@resultsDisc");
	Route::get("/disc/{date}/{lang}", "User\UserController@resultsDiscByDate");
	//HOLL
	Route::any("/holl/{lang}", "HollController@getHoll");
	Route::post("/send_holl", "HollController@Send");
	Route::any("/holl/finish", "HollController@finish");
	Route::any("/holl_res/results", "HollController@results");
	Route::get("/holl_indres/{date}/{lang}", "HollController@getHollResultsByDateForUser");
	//TOMAS
	Route::any("/tomas/{lang}", "TomasController@getTomas");
	Route::any("/send_tomas", "TomasController@Send");
	Route::any("/tomas/finish", "TomasController@finish");
	Route::any("/tomas_res/results", "TomasController@results");
	Route::get("/tomas_indres/{date}/{lang}", "TomasController@getTomasResultsByDateForUser");
	//SOLOMIN
	Route::any("/solomin/{lang}", "SolominController@getSolomin");
	Route::any("/send_solomin", "SolominController@Send");
	Route::any("/solomin/finish", "SolominController@finish");
	Route::any("/solomin_res/results", "SolominController@results");
	Route::get("/solomin_indres/{date}/{lang}", "SolominController@getSolominResultsByDateForUser");

});